#include "videothumbnailgenerator.h"




QList<QVideoFrame::PixelFormat> SingleFrameGrabber::supportedPixelFormats(QAbstractVideoBuffer::HandleType handleType) const
{
    Q_UNUSED(handleType);
    return QList<QVideoFrame::PixelFormat>() << QVideoFrame::Format_RGB24;
}
bool SingleFrameGrabber::present(const QVideoFrame &frame)
{
    if(_grabbed) {
        return true;
    }
    
    receivedFrame = frame;
    emit gotFrame();
    
    _grabbed = true;
    return true;
}

#include <QMutex>
#include <QMutexLocker>
#include <QMap>
QMutex g_thumbnailCacheMutex;
QMap<QString, QImage> g_thumbnailCache;

#include <QFile>
#include <opencv2/imgproc.hpp>
QImage getThumbnail(QString path)
{    
    if(g_thumbnailCache.contains(path)) {
        QMutexLocker lock(&g_thumbnailCacheMutex);
        return g_thumbnailCache.value(path);
    }
    
    if(!QFile(path).exists()) {
        return QImage();
    }
    
    QMediaPlayer mp;
    SingleFrameGrabber sfg;
    QEventLoop waitLoop;
    mp.setVideoOutput(&sfg);
    mp.setMedia(QUrl::fromLocalFile(path));
    mp.play();
    
    QObject::connect(&sfg, &SingleFrameGrabber::gotFrame, &waitLoop, &QEventLoop::quit);
    waitLoop.exec();
    mp.stop();
    
    // Mat has to be allocated outside of if statement 
    // because QImage stores a reference to the data 
    // that it is initialized with (rgb.data)
    cv::Mat rgb;
    QImage img;
#define FRAME sfg.receivedFrame 
    FRAME.map(QAbstractVideoBuffer::ReadOnly);
    if(FRAME.pixelFormat() == QVideoFrame::Format_YUV420P) {
        cv::Mat yuv(FRAME.size().height() + FRAME.size().height()/2, 
                    FRAME.size().width(), 
                    CV_8UC1,
                    FRAME.bits());
        cv::cvtColor(yuv, rgb, CV_YUV2RGB_I420);
        img = QImage(rgb.data, rgb.cols, rgb.rows, QImage::Format_RGB888);
    }
    else {
        img = QImage(FRAME.bits(), FRAME.width(), FRAME.height(), 
                     FRAME.bytesPerLine(), 
                     QVideoFrame::imageFormatFromPixelFormat(FRAME.pixelFormat()));  
    }
    FRAME.unmap();
#undef FRAME
    
    QMutexLocker lock(&g_thumbnailCacheMutex);
    g_thumbnailCache.insert(path, img.copy());
    return g_thumbnailCache.value(path);
}
