#include <QRegExp>
#include <QHash>
#include <QStringList>
#include "youtubelinkhandler.h"

YouTubeLinkHandler::YouTubeLinkHandler(QObject *parent) :
    QObject(parent),
    _manager(this)
{

}

void YouTubeLinkHandler::get(const QUrl& url)
{
    _reply = _manager.get(QNetworkRequest(url));
    connect(_reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(debugError(QNetworkReply::NetworkError)), Qt::QueuedConnection);
    connect(_reply, &QNetworkReply::finished, this, &YouTubeLinkHandler::parseResult, Qt::QueuedConnection);
}

void YouTubeLinkHandler::parseResult()
{
    QString result;

    auto statusCode = _reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if (statusCode >= 200 && statusCode < 300){
        auto data = QUrl::fromPercentEncoding(_reply->readAll());
        QString urlStart = data.mid(data.indexOf("url_encoded_fmt_stream_map"), 1000);
        QRegExp re("(url=https:\\/\\/[^,|\\\\]*)");
        re.indexIn(urlStart);
        auto urlList = re.capturedTexts();
        if(!urlList.isEmpty()) result = urlList[0].mid(4);
    }

    emit finished(result);
}

void YouTubeLinkHandler::debugError(QNetworkReply::NetworkError error)
{
    qDebug() << error;
}
