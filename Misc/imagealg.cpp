#include "imagealg.h"
#include <QPainter>
#include <QTime>

#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

namespace ImageAlg {

cv::Rect getPerspRegion(const cv::Mat& lambda, cv::Size size)
{
    cv::Rect rect;
    std::vector<cv::Point2f> cornersOrig = {cv::Point2f(0,0),
                                        cv::Point2f(size.width,size.height),
                                        cv::Point2f(0,size.height),
                                        cv::Point2f(size.width,0)
                                       };
    std::vector<cv::Point2f> corners;
    cv::perspectiveTransform(cornersOrig, corners, lambda);

    float pos = INT_MAX;
    for(auto point : corners) pos = std::min(point.x, pos);
    rect.x = pos;

    pos = INT_MAX;
    for(auto point : corners) pos = std::min(point.y, pos);
    rect.y = pos;

    pos = -1;
    for(auto point : corners) pos = std::max(point.x, pos);
    rect.width = pos - rect.x;

    pos = -1;
    for(auto point : corners) pos = std::max(point.y, pos);
    rect.height = pos - rect.y;

    return rect;
}

bool getMaxLocRegion(cv::Mat& grayImage, const cv::Rect& region, cv::Point* pos, double* value)
{
    if(region.area() <= 0 || grayImage.empty())
        return false;

    auto startX = std::max(0, region.x);
    auto startY = std::max(0, region.y);
    auto endX = std::min(grayImage.cols-1, region.x + region.width);
    auto endY = std::min(grayImage.rows-1, region.y + region.height);

    double tmp = 0;
    for(int y = startY; y < endY; y++){
        for(int x = startX; x < endX; x++){
            if(grayImage.at<uchar>(y,x) > tmp){
                tmp = grayImage.at<uchar>(y,x);
                if(value) *value = tmp;
                if(pos) *pos = cv::Point(x,y);
            }
        }
    }
    return true;
}

QImage scaleCentered(QImage image, QSize size, QColor background)
{
    QImage result(size, QImage::Format_ARGB32);
    result.fill(background);
    if(!image.isNull()){
        image = image.scaled(size, Qt::KeepAspectRatio, Qt::FastTransformation);
        QPainter painter(&result);
        int xpos = (result.width() - image.width())/2;
        int ypos = (result.height()- image.height())/2;
        painter.drawImage(xpos, ypos, image);
    }
    return result;
}

QPolygonF makeCircle(QPointF center, float radius, int segments)
{
    QPolygonF result;
    for(int i = 0; i < segments; i++) {
        float angle = M_PI*2/segments*i;
        float x = cos(angle)*radius;
        float y = sin(angle)*radius;
        result.append(QPointF(x,y) + center);
    }
    return result;
}

}
