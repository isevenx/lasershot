#ifndef VIDEOTHUMBNAILGENERATOR_H
#define VIDEOTHUMBNAILGENERATOR_H

#include <QMediaPlayer>
#include <QAbstractVideoSurface>
#include <QEventLoop>
#include <QImage>
#include <QTimer>
#include <QString>

class SingleFrameGrabber : public QAbstractVideoSurface
{
    Q_OBJECT
    
private:
    bool _grabbed;
    
public:
    QVideoFrame receivedFrame;
    
    SingleFrameGrabber() :
        QAbstractVideoSurface(),
        _grabbed(false)
    {}
    
    QList<QVideoFrame::PixelFormat> supportedPixelFormats(QAbstractVideoBuffer::HandleType handleType) const override;
    bool present(const QVideoFrame &frame) override;
    
signals:
    void gotFrame();
};


QImage getThumbnail(QString path);

#endif // VIDEOTHUMBNAILGENERATOR_H
