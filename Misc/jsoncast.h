#ifndef JSONCAST_H
#define JSONCAST_H

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QBuffer>
#include <QImage>
#include <QPolygon>

namespace JsonCast
{
    QJsonValue ToJson(const QImage& image);

    QImage ToImage(const QJsonValue& val);

    QJsonValue ToJson(const QSize& size);

    QSize ToSize(const QJsonValue& val);

    QJsonValue ToJson(const QPointF& point);

    QPointF ToPoint(const QJsonValue& val);

    QJsonValue ToJson(const QPolygonF& polygon);

    QPolygonF ToPolygon(const QJsonValue& val);
}

#endif // JSONCAST_H
