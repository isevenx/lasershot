#ifndef QEXCEPTION_H
#define QEXCEPTION_H

#include <exception>
#include <QString>

struct QException : public std::exception
{
    QException(QString message) { this->message = message.toStdString(); }
    ~QException() throw() {}
    const char* what() const throw() { return message.c_str(); }
    qint64 size() { return message.size(); }
private:
    std::string message;
};

#endif // QSEXCEPTION_H
