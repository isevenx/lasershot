#include "jsoncast.h"

QJsonValue JsonCast::ToJson(const QImage& image)
{
    QByteArray data;
    QBuffer buffer(&data);
    buffer.open(QIODevice::WriteOnly);
    image.save(&buffer, "PNG");
    auto encoded = buffer.data().toBase64();
    return QJsonValue(QString::fromLatin1(encoded));
}

QImage JsonCast::ToImage(const QJsonValue& val)
{
    QImage image;
    QByteArray encoded = val.toString().toLatin1();
    if(!encoded.isEmpty() && !image.loadFromData(QByteArray::fromBase64(encoded), "PNG")){
        QString fileName = val.toString();
        image.load(fileName); // load from file
    }
    return image;
}

QJsonValue JsonCast::ToJson(const QSize& size)
{
    QJsonObject obj;
    obj["width"] = size.width();
    obj["height"] = size.height();
    return obj;
}

QSize JsonCast::ToSize(const QJsonValue& val)
{
    QSize result;
    QJsonObject obj = val.toObject();
    result.setWidth(obj["width"].toInt());
    result.setHeight(obj["height"].toInt());
    return result;
}

QJsonValue JsonCast::ToJson(const QPointF& point)
{
    QJsonObject obj;
    obj["x"] = point.x();
    obj["y"] = point.y();
    return obj;
}

QPointF JsonCast::ToPoint(const QJsonValue& val)
{
    QPointF result;
    QJsonObject obj = val.toObject();
    result.setX(obj["x"].toDouble());
    result.setY(obj["y"].toDouble());
    return result;
}

QJsonValue JsonCast::ToJson(const QPolygonF& polygon)
{
    QJsonArray array;
    QJsonObject tmp;
    auto list = polygon.toList();
    for(const auto& point : list){
        tmp["x"] = point.x();
        tmp["y"] = point.y();
        array.append(tmp);
    }
    return array;
}

QPolygonF JsonCast::ToPolygon(const QJsonValue& val)
{
    QPolygonF result;
    QJsonArray array = val.toArray();
    for(const auto& val : array){
        QJsonObject tmp = val.toObject();
        QPoint p(tmp["x"].toDouble(), tmp["y"].toDouble());
        result.append(p);
    }
    return result;
}
