#ifndef GRACEFULSTOPPER_H
#define GRACEFULSTOPPER_H

#include <QObject>
#include <QThread>
#include <QVector>
#include <QPointer>

class GracefulStopper : public QObject
{
    Q_OBJECT
public:
    explicit GracefulStopper(QObject *parent = 0);
    void setObjectCount(qint64 count);

signals:
    void finished();
public slots:
    void closeAll();
    void incTermCounter(QString objectName);
public:
    void add(QPointer<QThread> thread);

private:
    QVector<QPointer<QThread>> _threads;
    qint64 _objectCount;
    qint64 _terminatedCounter;
};

#endif // GRACEFULSTOPPER_H
