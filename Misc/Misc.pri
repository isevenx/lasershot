HEADERS += \
    $$PWD/qexception.h \
    $$PWD/gracefulstopper.h \
    $$PWD/videoframereceiver.h \
    $$PWD/youtubelinkhandler.h \
    $$PWD/filedownloader.h \
    $$PWD/jsoncast.h \
    $$PWD/imagealg.h \
    $$PWD/videothumbnailgenerator.h \
    $$PWD/globalsettings.h

SOURCES += \
    $$PWD/gracefulstopper.cpp \
    $$PWD/videoframereceiver.cpp \
    $$PWD/youtubelinkhandler.cpp \
    $$PWD/filedownloader.cpp \
    $$PWD/imagealg.cpp \
    $$PWD/jsoncast.cpp \
    $$PWD/videothumbnailgenerator.cpp
