#ifndef YOUTUBELINKHANDLER_H
#define YOUTUBELINKHANDLER_H

#include <QtNetwork>

class YouTubeLinkHandler : public QObject
{
    Q_OBJECT

public:
    explicit YouTubeLinkHandler(QObject *parent = 0);

public slots:
    void get(const QUrl& url);

signals:
    void finished(QString result);

protected slots:
    void parseResult();
    void debugError(QNetworkReply::NetworkError);

private:
    QNetworkAccessManager _manager;
    QNetworkReply* _reply;
};

#endif // YOUTUBELINKHANDLER_H
