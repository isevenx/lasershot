#include "filedownloader.h"

FileDownloader::FileDownloader(QObject *parent):
    QObject(parent),
    _manager(this),
    _reply(nullptr)
{
}

void FileDownloader::get(const QUrl &url)
{
    _reply = _manager.get(QNetworkRequest(url));
    connect(_reply, &QNetworkReply::finished, this, &FileDownloader::parseResult, Qt::QueuedConnection);
}

QByteArray FileDownloader::getLatestFile()
{
    return _data;
}

void FileDownloader::clear()
{
    _data.clear();
}

void FileDownloader::parseResult()
{
    auto statusCode = _reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    if (statusCode >= 200 && statusCode < 300){
        _data = _reply->readAll();
    }

    delete _reply;
    _reply = nullptr;
    emit finished();
}
