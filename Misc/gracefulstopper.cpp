#include "gracefulstopper.h"
#include <QApplication>
#include <QDebug>

GracefulStopper::GracefulStopper(QObject *parent) :
    QObject(parent),
    _objectCount(0),
    _terminatedCounter(0)
{

}

void GracefulStopper::setObjectCount(qint64 count)
{
    _objectCount = count;
}

void GracefulStopper::add(QPointer<QThread> thread)
{
    _threads.append(thread);
}

void GracefulStopper::incTermCounter(QString objectName)
{
    qDebug() << "Object terminated!" << objectName;
    _terminatedCounter++;
    if(_terminatedCounter == _objectCount)
        closeAll();
}

void GracefulStopper::closeAll()
{
    qDebug() << "Stopping all threads...";
    for(auto& thread : _threads){
        if(thread) thread->quit();
    }
    qDebug() << "Waiting for threads to finish...";
    for(auto& thread : _threads){
        if(thread) thread->wait(2000/_threads.size());
    }
    qDebug() << "All threads stopped.";
    emit finished();
}
