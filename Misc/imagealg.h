#ifndef IMAGEALG_H
#define IMAGEALG_H

#include <opencv2/core.hpp>
#include <QImage>
#include <QVideoFrame>
#include <QPolygon>

namespace ImageAlg
{
    cv::Rect getPerspRegion(const cv::Mat& lambda, cv::Size size);

    bool getMaxLocRegion(cv::Mat& grayImage, const cv::Rect& region, cv::Point* pos, double* value);

    QImage scaleCentered(QImage image, QSize size, QColor background);

    QPolygonF makeCircle(QPointF center, float radius, int segments);
}

#endif // IMAGEALG_H
