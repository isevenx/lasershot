#include "videoframereceiver.h"
#include <QThread>

VideoFrameReceiver::VideoFrameReceiver(QObject* parent):
    QAbstractVideoSurface(parent)
{
}

QList<QVideoFrame::PixelFormat> VideoFrameReceiver::supportedPixelFormats(
        QAbstractVideoBuffer::HandleType handleType) const
{
    Q_UNUSED(handleType);
    return QList<QVideoFrame::PixelFormat>() << QVideoFrame::Format_RGB565;
}

bool VideoFrameReceiver::present(const QVideoFrame& frame)
{
    QVideoFrame cloneFrame(frame);
    cloneFrame.map(QAbstractVideoBuffer::ReadOnly);
    QImage tmpImage(cloneFrame.bits(), cloneFrame.width(), cloneFrame.height(),
                        QVideoFrame::imageFormatFromPixelFormat(cloneFrame.pixelFormat()));
    QImage result = tmpImage.copy();
    cloneFrame.unmap();
    emit frameReceived(result);
    return true;
}
