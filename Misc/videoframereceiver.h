#ifndef VIDEOFRAMERECEIVER_H
#define VIDEOFRAMERECEIVER_H

#include <QAbstractVideoSurface>
#include <QVideoFrame>

class VideoFrameReceiver : public QAbstractVideoSurface
{
    Q_OBJECT
public:
    VideoFrameReceiver(QObject* parent = 0);

    QList<QVideoFrame::PixelFormat> supportedPixelFormats(
            QAbstractVideoBuffer::HandleType handleType = QAbstractVideoBuffer::NoHandle) const;
    bool present(const QVideoFrame& frame);

signals:
    void frameReceived(QImage frame);
};

#endif // VIDEOFRAMERECEIVER_H
