#ifndef GLOBALSETTTINGS_H
#define GLOBALSETTTINGS_H

#include <QDir>
#include <QColor>

namespace GlobalSettings
{
    const int RENDER_BUFFER_WIDTH  = 1920;
    const int RENDER_BUFFER_HEIGHT = 1080;
    const QColor COLOR_BLUE = QColor(QRgb(0x007BFF));
    const QColor COLOR_ORANGE = QColor(QRgb(0xFF9100));
    const QColor COLOR_LIGHT_GRAY = QColor(QRgb(0xcccccc));
    const QColor COLOR_GRAY = QColor(QRgb(0x555555));
    const QColor COLOR_DARK_GRAY = QColor(QRgb(0x333333));
    const QString ASSETS_TARGET_PATH   = QDir::homePath() + "/Documents/LaserShot/Targets/";
    const QString ASSETS_LEVEL_PATH    = QDir::homePath() + "/Documents/LaserShot/Levels/";
    const QString ASSETS_SCENARIO_PATH = QDir::homePath() + "/Documents/LaserShot/Scenarios/";
    const QString ASSETS_IMAGE_PATH    = QDir::homePath() + "/Documents/LaserShot/Images/";
    const QString ASSETS_SOUND_PATH    = QDir::homePath() + "/Documents/LaserShot/Sounds/";
    const QString ASSETS_VIDEO_PATH    = QDir::homePath() + "/Documents/LaserShot/Videos/";
}

#endif // GLOBALSETTTINGS_H
