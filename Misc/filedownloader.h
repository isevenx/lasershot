#ifndef FILEDOWNLOADER_H
#define FILEDOWNLOADER_H

#include <QtNetwork>

class FileDownloader : public QObject
{
    Q_OBJECT
public:
    explicit FileDownloader(QObject *parent = 0);
    QByteArray getLatestFile();
    void clear();
signals:
    void finished();
public slots:
    void get(const QUrl& url);
private slots:
    void parseResult();

private:
    QNetworkAccessManager _manager;
    QNetworkReply* _reply;
    QByteArray _data;
};

#endif // FILEDOWNLOADER_H
