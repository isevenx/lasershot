#include "gameengine.h"
#include <QPainter>
#include <QCursor>
#include "Mode/Shooting/shootingmode.h"
#include "Misc/imagealg.h"
#include "Misc/globalsettings.h"
#include <QSoundEffect>
using namespace MODE;
using namespace GlobalSettings;

GameEngine::GameEngine(QMediaPlayer* player, GameOutputDevice* gameDevice):
    QAbstractVideoSurface(nullptr),
    _gameDevice(gameDevice),
    _shotModel(nullptr),
    _mode(MODE::IDLE),
    _player(player),
    _defaultBackground(GlobalSettings::ASSETS_IMAGE_PATH+"/test_background.jpg"),
    _playingVideo(false),
    _latestShotTime(0),
    _timer(this),

    _ytHandler(this),
    _fileDownloader(this),
    _hitDrawMode(FS_CROSS),

    _layer(nullptr),
    _resetGame(true),
    _previewLayer(new LayerModel())
{
    connect(_player, SIGNAL(error(QMediaPlayer::Error)),
            this, SLOT(playerErrorHandle(QMediaPlayer::Error)),
            Qt::QueuedConnection);
    connect(_player, &QMediaPlayer::mediaStatusChanged, this, &GameEngine::handleMediaStatusChanged, Qt::QueuedConnection);    
//    connect(&_ytHandler, &YouTubeLinkHandler::finished, this, &GameEngine::setVideoLink, Qt::QueuedConnection);
//    connect(&_fileDownloader, &FileDownloader::finished, this, &GameEngine::setBackgroundImage, Qt::QueuedConnection);
    connect(&_timer, &QTimer::timeout, this, &GameEngine::createTimeEvent, Qt::QueuedConnection);
    _timer.setSingleShot(true);

    _player->setVideoOutput(this);
}
GameEngine::~GameEngine()
{
    delete _previewLayer;
}

void GameEngine::stop()
{
    _player->stop();
    _timer.stop();
}

void GameEngine::terminate()
{
    clearTargets();
    emit terminated("GameEngine");
    disconnect();
}

void GameEngine::set(ShotModel* model)
{
    _shotModel = model;
}
QList<QVideoFrame::PixelFormat> GameEngine::supportedPixelFormats(QAbstractVideoBuffer::HandleType handleType) const
{
    Q_UNUSED(handleType);
    return QList<QVideoFrame::PixelFormat>() << QVideoFrame::Format_RGB24;
}
#include <opencv2/imgproc.hpp>
bool GameEngine::present(const QVideoFrame& frame)
{
    if(_playingVideo){
        // Mat has to be allocated outside of if statement
        // because QImage stores a reference to the data
        // that it is initialized with (rgb.data)
        cv::Mat rgb;
        QImage img;
        QVideoFrame tmp(frame);
        tmp.map(QAbstractVideoBuffer::ReadOnly);
        if(tmp.pixelFormat() == QVideoFrame::Format_YUV420P) {
            cv::Mat yuv(tmp.size().height() + tmp.size().height()/2,
                        tmp.size().width(),
                        CV_8UC1,
                        tmp.bits());
            cv::cvtColor(yuv, rgb, CV_YUV2RGB_I420);
            img = QImage(rgb.data, rgb.cols, rgb.rows, QImage::Format_RGB888);
        }
        else {
            img = QImage(tmp.bits(), tmp.width(), tmp.height(),
                         tmp.bytesPerLine(),
                         QVideoFrame::imageFormatFromPixelFormat(tmp.pixelFormat()));
        }
        tmp.unmap();
        showFrame(img);
    }
    
    return true;
}

void GameEngine::reset()
{
    handleNewMode(_mode);
}

void GameEngine::resetTargets()
{
    clearTargets();
    loadTargets();
    if(_layer) {
        _gameDevice->set(_layer);
        _layer->start();
    }
    createTimeEvent();
    _shotModel->clear();
}
void GameEngine::handleNewMode(Mode mode)
{
    clearTargets();
    _shotModel->clear();
    hideExamTarget();

    _mode = mode;
    if(mode == SHOOTING::FREE){
        // Reset background
        if(isImage(_freeUrl))
            showFrame(QImage(_freeUrl));
        else {
            _playingVideo = true;
            _player->setMedia(QUrl(_freeUrl));
        }
    }
    else if(mode == SHOOTING::TARGET || mode == SHOOTING::EXAM) {
        resetTargets();
    }
    else if(mode == SHOOTING::SCENARIO){
        // doing nothing because scenario starts with link change (changeScenarioLink)
    }
    else {
        if(_player->state() == QMediaPlayer::PlayingState) {
            _player->stop();
            _playingVideo = false;
        }
    }
    if(_mode == MODE::IDLE) showFrame(_defaultBackground);
}

void GameEngine::loadTargets()
{
    if(_mode == SHOOTING::TARGET){
        if(_levelModel.fromFile(_levelPath)){
            _layer = _levelModel.getNextLayer();
            showFrame(QImage(_layer->getBackground()));
        }
    }
    else if(_mode == SHOOTING::EXAM){
        // Main layer
        LayerModel* layer = new LayerModel();
        QSize screenSize{RENDER_BUFFER_WIDTH, RENDER_BUFFER_HEIGHT};

        TargetModel* tm = new TargetModel();
        tm->setPosition({ 50, 50 });
        tm->setSize({ 450, 400 });
        layer->add(tm);
        TextAction* ta = new TextAction(tm, "Time: %rt%/%time%s\nAmmo: %ammo%\nHits: %hits%/" + QString::number(_examAmmo));
        layer->add(ta);
        layer->setSound(QDir::homePath() + "/Documents/LaserShot/Sounds/beep2.wav");


        tm = new TargetModel();
        tm->fromFile(_examTargetPath);
        tm->setScale(_examTargetScale, _examTargetScale);
        tm->setPosition({ screenSize.width()/2.0f - tm->getSize().width()/2.0f,
                          screenSize.height()/2.0f - tm->getSize().height()/2.0f });
        layer->add(tm);
        layer->setMissPenalty(200);

        TotalScore ts;
        ts.time = _examTime*1000LL;
        ts.ammo = _examAmmo;
        layer->set(ts);

        // Finish conditions
        FinishAction* fa = new FinishAction(TIME_EVENT, FINISH, ts.time);
        layer->add(fa);
        fa = new FinishAction(SHOT_EVENT, FINISH, _examAmmo);
        layer->add(fa);
        _levelModel.add(layer);
        _layer = _levelModel.getNextLayer();


        // End layer
        layer = new LayerModel();
        tm = new TargetModel();
        tm->setPosition({ screenSize.width()/2.0f - 225.0f, screenSize.height()/2.0f - 200.0f});
        tm->setSize({ 450, 400 });
        layer->add(tm);
        ta = new TextAction(tm, QString("Time: %time%\nHits: %hits%/%1").arg(_examAmmo));
        layer->add(ta);
        _levelModel.add(layer);

        QImage background({ 16, 9 }, QImage::Format_RGB32);
        background.fill(COLOR_DARK_GRAY);
        showFrame(background);
    }
}

void GameEngine::clearTargets()
{
    _levelModel.clear();
    _layer = nullptr;
    _timer.stop();
}

void GameEngine::nextLayer()
{
    auto nextLayer = _levelModel.getNextLayer();
    if(nextLayer){
        nextLayer->start();
        nextLayer->set(_layer->getScore());
        _layer = nextLayer;
        if(_mode == SHOOTING::TARGET)
            showFrame(QImage(_layer->getBackground()));
        _gameDevice->set(_layer);
    }
}


void GameEngine::createTimeEvent()
{
    _timer.start(1);
    if(_layer){
        _layer->createTimeEvent();
        if(_layer->isRestarted()) resetTargets();
        else if(_layer->isFinished()) nextLayer();
    }
}

void GameEngine::processLaserPoints(QVector<QPoint> points, qint64 timestamp)
{
    auto point = points[0];

    // Delay between shots (for how long laser points should not be)
    if(timestamp - _latestShotTime > 100){
        QSoundEffect* sound = new QSoundEffect(this);
        sound->setSource(QUrl::fromLocalFile(QDir::homePath() + "/Documents/LaserShot/Sounds/handgun.wav"));
        sound->play();
        QObject::connect(sound, &QSoundEffect::playingChanged, sound, &QSoundEffect::deleteLater, Qt::QueuedConnection);

        bool showShot = false;
        QImage screenShot;
        if(_mode == SHOOTING::FREE || _mode == SHOOTING::SCENARIO){
            screenShot = _latestFrame.copy();
            showShot = true;
        }
        else if(_mode == SHOOTING::TARGET || _mode == SHOOTING::EXAM){
            screenShot = _gameDevice->getScreenShot().scaled(QSize(RENDER_BUFFER_WIDTH, RENDER_BUFFER_HEIGHT));
            if(_layer){
                auto result = _layer->shootSelf(point);
                showShot = (result.missed || result.hitCount);
                if(_layer->isRestarted()){
                    resetTargets();
                    showShot = false;
                }
                else if(_layer->isFinished()) nextLayer();
            }
        }

        if(_shotModel && showShot){
            QPainter painter(&screenShot);
            if(_hitDrawMode == FS_CROSS) {
                painter.setBrush(Qt::white);
                painter.setPen(QPen(Qt::black, 4));
                painter.drawRect(point.x()-4, 0, 8, screenShot.height());
                painter.drawRect(0, point.y()-4, screenShot.width(), 8);
            }
            else {
                painter.setBrush(Qt::green);
                painter.setPen(QPen(Qt::red, 4));
                painter.drawRect(point.x()-8, point.y()-8, 16, 16);
            }
            if(_shotModel) _shotModel->addScreenShot(screenShot);
        }
    }

    _latestShotTime = timestamp;
}


bool GameEngine::isImage(QString url)
{
    return (url.contains("jpg") || url.contains("png") || url.contains("jpeg") || url.isEmpty() || url == "black");
}

void GameEngine::changeFreeLink(QString url)
{
    stop();
    _freeUrl = (isImage(url) ? url : QString("file://" + url));
}

void GameEngine::changeScenarioLink(QString url, bool start)
{
    stop();
    _scenarioUrl = (isImage(url) ? url : QString("file://" + url));
    _playingVideo = start;
    if(start) _player->setMedia(QUrl(_scenarioUrl));
}

void GameEngine::changeHitDrawMode(HitDrawMode hitDrawMode)
{
    _hitDrawMode = hitDrawMode;
}
void GameEngine::changeLevel(QString filePath)
{
    _levelPath = filePath;
}
void GameEngine::setExam(qint64 time, qint64 ammo, QString targetPath, float targetScale)
{
    _examTime = time;
    _examAmmo = ammo;
    _examTargetPath = targetPath;
    _examTargetScale = targetScale;

    TargetModel* tm = new TargetModel();
    tm->fromFile(_examTargetPath);
    tm->setScale(_examTargetScale, _examTargetScale);
    QSize screenSize{RENDER_BUFFER_WIDTH, RENDER_BUFFER_HEIGHT};
    tm->setPosition({ screenSize.width()/2.0f - tm->getSize().width()/2.0f,
                      screenSize.height()/2.0f - tm->getSize().height()/2.0f });

    _previewLayer->clear();
    _previewLayer->add(tm);
}

void GameEngine::showExamTarget()
{
    _gameDevice->set(_previewLayer);
}

void GameEngine::hideExamTarget()
{
    _gameDevice->set(nullptr);
}

void GameEngine::playVideo()
{
    if(!_playingVideo) {
        _player->play();
        _playingVideo = true;
    }
}

void GameEngine::pauseVideo()
{
    if(_playingVideo){
        _player->pause();
        _playingVideo = false;
    }
}

void GameEngine::showFrame(QImage frame)
{
    auto result = ImageAlg::scaleCentered(frame, QSize(RENDER_BUFFER_WIDTH, RENDER_BUFFER_HEIGHT), Qt::black);
    if(frame.isNull()) {
        qDebug() << "Bad frame"
                 << _player->isVideoAvailable()
                 << _player->isMetaDataAvailable()
                 << _player->isAvailable()
                 << _player->mediaStatus();
    }
    _gameDevice->set(result);
    _latestFrame = result.copy();
}

//void GameEngine::setVideoLink(QString url)
//{
//    _player->setMedia(QUrl(url));
//}
//void GameEngine::setBackgroundImage()
//{
//    auto image = QImage::fromData(_fileDownloader.getLatestFile());
//    showFrame(image);
//    _fileDownloader.clear();
//}

void GameEngine::playerErrorHandle(QMediaPlayer::Error error)
{
    emit modeChangeRequested(MODE::IDLE);
}

#include <QMediaContent>
void GameEngine::handleMediaStatusChanged(QMediaPlayer::MediaStatus status)
{
    // This is needed because QMediaPlayer cant start playing after video loaded by himself
    if(status == QMediaPlayer::MediaStatus::LoadedMedia && _playingVideo) {
        _player->play();
    }
    if(status == QMediaPlayer::MediaStatus::EndOfMedia) {
        emit videoEnded();
    }
}
