#ifndef FLYCAPGRABBER_H
#define FLYCAPGRABBER_H

#include "camera.h"
#include "camerasettings.h"
#include <flycapture/FlyCapture2.h>

typedef unsigned int uint;

class FlycapGrabber : public Camera, public CameraSettings
{
    Q_OBJECT
public:
// Camera interface
    const cv::Mat getFrame();
    quint64 getFrameNumber();

// Camera settings interface
    void setShutter(float percent);
    void setFps(float percent);
    float getShutter();
    float getFps();

// Grabber interface
    explicit FlycapGrabber(QObject* parent = nullptr);
    bool isConnected();
    uint getCameraCount();
signals:
    void newFrame();
    void newCamera();
    void terminated(QString objectName);
    void startFailed();
    void restartSucceeded();
public slots:
    void start();
    void stop();
    void terminate();
    void setIndex(uint index); // starts from 0

protected:
    FlyCapture2::Camera _camera;
    FlyCapture2::BusManager _busMgr;
    FlyCapture2::PGRGuid _guid;
    uint _index;
    std::atomic<bool> _grabbingStarted;

    FlyCapture2::Image _rawImage;
    cv::Mat _frame;
    std::atomic<quint64> _frameCounter;

    static void frameCallback(FlyCapture2::Image* pImage, const void* pCallbackData);
    static void camArrivalCallback(void* pParameter, unsigned int serialNumber);
    FlyCapture2::CallbackHandle _arrivalHandle;

    FlyCapture2::Error setLowRes();
    FlyCapture2::Error disableGain();

private slots:
    void setFrame(FlyCapture2::Image* pImage);
    void processNewCamera(uint serialNumber);
};

#endif // FLYCAPGRABBER_H
