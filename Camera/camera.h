#ifndef CAMERA_H
#define CAMERA_H

#include <QObject>
#include <opencv2/core.hpp>

class Camera : public QObject
{
    Q_OBJECT
public:
    explicit Camera(QObject* parent = nullptr): QObject(parent) {}
    virtual const cv::Mat getFrame() = 0;
    virtual quint64 getFrameNumber() = 0;
};

#endif // CAMERA_H
