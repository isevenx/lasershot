#ifndef CAMERASETTINGS_H
#define CAMERASETTINGS_H

class CameraSettings
{
public:
    virtual void setShutter(float percent) = 0; // from 0 to 100
    virtual float getShutter() = 0; // from 0 to 100
    virtual void setFps(float percent) = 0; // from 0 to 100
    virtual float getFps() = 0;
};

#endif // CAMERASETTINGS_H
