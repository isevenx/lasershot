#include "flycapgrabber.h"
#include <QDebug>
#include <QDateTime>
#include <opencv2/imgcodecs.hpp>
#include <QApplication>
#include "Misc/qexception.h"

using namespace cv;
using namespace FlyCapture2;

FlycapGrabber::FlycapGrabber(QObject *parent):
    Camera(parent),
    _index(0),
    _grabbingStarted(false),
    _frameCounter(0)
{
    _busMgr.RegisterCallback(&FlycapGrabber::camArrivalCallback, ARRIVAL, this, &_arrivalHandle);
}

bool FlycapGrabber::isConnected()
{
    return _camera.IsConnected();
}

void FlycapGrabber::start()
{
    FlyCapture2::Error error;
    _grabbingStarted = true;

    auto withError = [&](QString msg){
        return msg + QString(" index[%1] error[%2]").arg(_index).arg(error.GetDescription());
    };

    try{
        if(_index >= getCameraCount()) throw QException("Wrong camera index!");

        if((error = _busMgr.GetCameraFromIndex(_index, &_guid)) != PGRERROR_OK)
            throw QException("Failed to get camera guid");

        if((error = _camera.Connect(&_guid)) != PGRERROR_OK)
            throw QException("Failed to connect camera");

        // Grabbing format should be set before starting capture
        if((error = setLowRes()) != PGRERROR_OK)
            qWarning() << withError("Failed to set high fps");

        if((error = _camera.StartCapture(&FlycapGrabber::frameCallback, this)) != PGRERROR_OK)
            throw QException("Failed to start capture");


        qDebug() << "Camera connected! fps[" << getFps() << "]";
        if((error = disableGain()) != PGRERROR_OK)
            qWarning() << withError("Failed to disable gain");
    }
    catch(QException& e){
        qDebug() << withError(e.what());
        emit startFailed();
    }
}

void FlycapGrabber::stop()
{
    _grabbingStarted = false;
    qDebug() << "STOP!";
    if(isConnected()){
        qDebug() << "IS CONNECTED!";
        // Capture could not be stopped until callback is finished
        if(_camera.StopCapture() != FlyCapture2::PGRERROR_OK)
            qWarning() << "FlyCap camera prematurely disconnected";
        qDebug() << "STOPPED!";
        _camera.Disconnect();
        qDebug() << "DISCONNECTED!";
    }
}

void FlycapGrabber::terminate()
{
    qDebug() << "Terminating camera...";
    stop();
    emit terminated("FlycapGrabber");
    disconnect();    
}

void FlycapGrabber::setShutter(float percent)
{    
    if(isConnected()){
        PropertyInfo info;
        info.type = SHUTTER;
        _camera.GetPropertyInfo(&info);

        if(percent < 0) percent = 0;
        if(percent > 100) percent = 100;

        Property prop;
        prop.type = SHUTTER;
        prop.onOff = true;
        prop.autoManualMode = false;
        prop.absControl = true;
        prop.absValue = (info.absMax - info.absMin) * percent / 100;
        auto error = _camera.SetProperty(&prop);
        if (error != PGRERROR_OK)
            qWarning() << "SetShutter failed!" << error.GetDescription();
    }
    else qWarning() << "Cannot set shutter! Camera not connected!";
}

void FlycapGrabber::setFps(float percent)
{
    if(isConnected()){
        PropertyInfo info;
        info.type = FRAME_RATE;
        _camera.GetPropertyInfo(&info);

        if(percent < 0) percent = 0;
        if(percent > 100) percent = 100;

        Property prop;
        prop.type = FRAME_RATE;
        prop.onOff = true;
        prop.autoManualMode = false;
        prop.absControl = true;
        prop.absValue = (info.absMax - info.absMin) * percent / 100;
        auto error = _camera.SetProperty(&prop);
        if (error != PGRERROR_OK)
            qWarning() << "SetFps failed!" << error.GetDescription();
    }
    else qWarning() << "Cannot set fps! Camera not connected!";
}

float FlycapGrabber::getShutter()
{
    float result = 0;

    if(isConnected()){
        PropertyInfo info;
        info.type = SHUTTER;
        _camera.GetPropertyInfo(&info);

        Property prop;
        prop.type = SHUTTER;
        _camera.GetProperty(&prop);

        if(info.present && prop.present){
            auto range = info.absMax - info.absMin;
            if(range) result = (prop.absValue-info.absMin)/range * 100;
        }
    }

    return result;
}

float FlycapGrabber::getFps()
{
    float result = 0;

    if(isConnected()){
        Property prop;
        prop.type = FRAME_RATE;
        if(_camera.GetProperty(&prop) == PGRERROR_OK)
            result = prop.absValue;
    }

    return result;
}

uint FlycapGrabber::getCameraCount()
{
    uint numCameras = 0;
    auto error = _busMgr.GetNumOfCameras(&numCameras);
    if(error!= PGRERROR_OK)
        qWarning() << "CameraIface// GetNumOfCameras: " << error.GetDescription();
    return numCameras;
}

void FlycapGrabber::setIndex(uint index)
{
    _index = index;
}

const cv::Mat FlycapGrabber::getFrame()
{
    return _frame;
}

quint64 FlycapGrabber::getFrameNumber()
{
    return _frameCounter;
}

void FlycapGrabber::frameCallback(Image *pImage, const void *pCallbackData)
{
    auto grabber = static_cast<FlycapGrabber*>((void*)pCallbackData);
    if(grabber && grabber->_grabbingStarted){
        grabber->setFrame(pImage);
//        QMetaObject::invokeMethod(grabber, "setFrame", Qt::BlockingQueuedConnection, Q_ARG(FlyCapture2::Image*,pImage));
    }
}

void FlycapGrabber::camArrivalCallback(void *pParameter, unsigned int serialNumber)
{
    auto grabber = static_cast<FlycapGrabber*>(pParameter);
    if(grabber)
        QMetaObject::invokeMethod(grabber, "processNewCamera", Qt::QueuedConnection, Q_ARG(uint, serialNumber));
}

void FlycapGrabber::setFrame(Image *pImage)
{
    Image rgbImage;
    pImage->Convert(PIXEL_FORMAT_RGB, &rgbImage);

    Mat frame(rgbImage.GetRows(), rgbImage.GetCols(), CV_8UC3);
    memcpy(frame.data, rgbImage.GetData(), rgbImage.GetDataSize());
    _frame = frame; // changing data pointer (old frame will be deleted when its ref count is 0)
    _frameCounter++;

    emit newFrame();
}

void FlycapGrabber::processNewCamera(uint serialNumber)
{
    Q_UNUSED(serialNumber);
    emit newCamera();
    if(_grabbingStarted){
        start();
        if(isConnected()) emit restartSucceeded();
    }
}

FlyCapture2::Error FlycapGrabber::setLowRes()
{
    FlyCapture2::Error error;

    Format7Info format7Info;
    bool format7Supported;
    error = _camera.GetFormat7Info(&format7Info, &format7Supported);

    if(error == PGRERROR_OK && format7Supported){
        Format7ImageSettings f7s;
        Format7PacketInfo f7pi;
        bool settingsOk;

        f7s.mode = MODE_8;
        f7s.pixelFormat = PIXEL_FORMAT_RAW8;
        f7s.width = 1600;
        f7s.height = 900;
        f7s.offsetX = (format7Info.maxWidth-f7s.width)/2;
        f7s.offsetY = (format7Info.maxHeight-f7s.height)/2;

        error = _camera.ValidateFormat7Settings(&f7s, &settingsOk, &f7pi);
        if(error == PGRERROR_OK && settingsOk)
            error = _camera.SetFormat7Configuration(&f7s , 100.f);
    }

    return error;
}

FlyCapture2::Error FlycapGrabber::disableGain()
{
    Property prop;
    prop.type = GAIN;
    prop.onOff = true;
    prop.autoManualMode = false;
    prop.absControl = true;
    prop.absValue = 0;
    return _camera.SetProperty(&prop);
}
