#ifndef SHOTMODEL_H
#define SHOTMODEL_H

#include "imagemodel.h"
#include "listmodel.h"

class ShotModel : public ImageModel, public ListModel
{
public:
    ShotModel();
    QVector<QImage> getImages();
    void addScreenShot(QImage image);
    QVector<QString> getList();

    void setBufferSize(int s);
    void clear();
    
private:
    void cleanup();
    
    QVector<QImage> _images;
    int _bufferSize;
};

#endif // SHOTMODEL_H
