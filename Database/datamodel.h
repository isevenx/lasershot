#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QObject>

class DataModel : public QObject
{
    Q_OBJECT
public:
    virtual ~DataModel() = 0;
signals:
    void dataChanged();
    void deleted();
};
inline DataModel::~DataModel(){}

#endif // DATAMODEL_H
