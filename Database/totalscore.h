#ifndef TOTALSCORE_H
#define TOTALSCORE_H

#include "shotresult.h"
#include <qtypeinfo.h>

struct TotalScore
{
    TotalScore() { clear(); }

    void operator+=(const ShotResult& other)
    {
        score += other.score;
        hitCount += other.hitCount;
        shotCount++;
    }

    void clear()
    {
        score = 0;
        hitCount = 0;
        shotCount = 0;
        ammo = 0;
        time = 0;
    }

    qint64 score;
    qint64 hitCount;
    qint64 shotCount;
    qint64 ammo;
    qint64 time;
    
    // @ make better
    TotalScore& operator=(const TotalScore& other) {
        if(other.score != -1) {
            score = other.score;
        }
        if(other.hitCount != -1) {
            hitCount = other.hitCount;
        }
        if(other.shotCount != -1) {
            shotCount = other.shotCount;
        }
        if(other.ammo != -1) {
            ammo = other.ammo;
        }
        if(other.time != -1) {
            time = other.time;
        }
        
        return *this;
    }
};

#endif // TOTALSCORE_H
