#ifndef SHOTRESULT_H
#define SHOTRESULT_H

struct ShotResult
{
    long score = 0;
    long hitCount = 0;
    bool missed = false;

    void operator+=(const ShotResult& other)
    {
        score += other.score;
        hitCount += other.hitCount;
    }

    void clear()
    {
        score = 0;
        hitCount = 0;
        missed = false;
    }
};

#endif // SHOTRESULT_H
