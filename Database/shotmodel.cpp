#include "shotmodel.h"

ShotModel::ShotModel() :
    _bufferSize(5)
{}

QVector<QImage> ShotModel::getImages()
{
    return _images;
}

QVector<QString> ShotModel::getList()
{
    return QVector<QString>();
}

void ShotModel::setBufferSize(int s)
{
    _bufferSize = s;
    cleanup();
}
void ShotModel::clear()
{
    _images.clear();
    
    emit dataChanged();
}

void ShotModel::addScreenShot(QImage image)
{
    _images.append(image);
    cleanup();
    
    emit dataChanged();
}

void ShotModel::cleanup()
{
    while(_images.size() > _bufferSize) {
        _images.pop_front();
    }
}
