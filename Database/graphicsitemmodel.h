#ifndef GRAPHICSITEMMODEL_H
#define GRAPHICSITEMMODEL_H

#include "abstractdatamodel.h"
#include <QGraphicsPixmapItem>

class GraphicsItemModel : virtual public AbstractDataModel
{
public:
    QVector<QGraphicsItem*> virtual getItems() = 0;
};

#endif // GRAPHICSITEMMODEL_H
