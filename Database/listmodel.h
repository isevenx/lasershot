#ifndef LISTMODEL_H
#define LISTMODEL_H

#include "datamodel.h"

class ListModel : virtual public DataModel
{
    QVector<QString> virtual getList() = 0;
};

#endif // LISTMODEL_H
