#ifndef IMAGEMODEL_H
#define IMAGEMODEL_H

#include "datamodel.h"
#include <QImage>
#include <QVector>

class ImageModel : virtual public DataModel
{
public:
    QVector<QImage> virtual getImages() = 0;
};

#endif // IMAGEMODEL_H
