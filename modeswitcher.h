#ifndef MODESWITCHER_H
#define MODESWITCHER_H

#include <QObject>
#include <QPointer>
#include <QThread>
#include "Camera/camera.h"
#include "Camera/camerasettings.h"
#include "Ui/frameoutputdevice.h"
#include "Mode/mode.h"
#include "Mode/runnable.h"


class ModeSwitcher : public QObject
{
    Q_OBJECT
public:
    explicit ModeSwitcher(Camera* camera, CameraSettings* camSettings, FrameOutputDevice* outDevice, QObject *parent = 0);
    ~ModeSwitcher();
signals:
    void newDebugFrame(QImage frame);
    void newLaserPoints(QVector<QPoint> points, qint64 timestamp);
    void modeChanged(Mode mode);
    void terminated(QString objectName);
public slots:
    void changeMode(Mode mode);
    void processFrame();
    void stop();
    void terminate();
    void createShot(QPoint position);
private slots:
    void sendDebugFrame(cv::Mat frame);
    void saveShootingShutter(float shutter);
    void saveLambda(cv::Mat lambda);
    void finishCalibration();
    void sendLaserPoints(QVector<QPoint> points);

private:
    Mode _mode;
    QVector<Mode> _modeQueue;
    Camera* _camera;
    CameraSettings* _settings;
    FrameOutputDevice* _outDevice;
    QPointer<Runnable> _runnable;
    QThread _runnableThread;

    float _shootingShutter;
    cv::Mat _lambda;
    cv::Rect _projectorRegion;
    void startCalibration();
    void startShooting();
    void startTest();
};

#endif // MODESWITCHER_H
