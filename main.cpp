#include <QApplication>
#include <QThread>
#include "Ui/mainwindow.h"
#include "Ui/gamewindow.h"
#include "Misc/gracefulstopper.h"
#include "Camera/flycapgrabber.h"
#include "modeswitcher.h"
#include "gameengine.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    qRegisterMetaType<Mode>("Mode");
    qRegisterMetaType<cv::Mat>("cv::Mat");
    qRegisterMetaType<HitDrawMode>("HitDrawMode");

    GameWindow gameWindow;
    MainWindow mainWindow;
    QMediaPlayer player; // works only in gui thread...

    FlycapGrabber grabber;
    QThread grabberThread;
    grabber.moveToThread(&grabberThread);
    grabberThread.start();
    grabber.setIndex(0);
    QTimer::singleShot(100,&grabber,SLOT(start()));

    ModeSwitcher switcher(&grabber, &grabber, &gameWindow);
    QThread switcherThread;
    switcher.moveToThread(&switcherThread);
    switcherThread.start();

    GameEngine gameEngine(&player, &gameWindow);
    QThread engineThread;
    gameEngine.moveToThread(&engineThread);
    engineThread.start();

    // Graceful app closing
    GracefulStopper stopper;
    QThread stopperThread;
    stopper.moveToThread(&stopperThread);
    stopperThread.start();
    stopper.setObjectCount(3);
    stopper.add(&engineThread);
    stopper.add(&grabberThread);
    stopper.add(&switcherThread);
    QObject::connect(&mainWindow, &MainWindow::aboutToClose, &gameEngine, &GameEngine::terminate, Qt::QueuedConnection);
    QObject::connect(&mainWindow, &MainWindow::aboutToClose, &grabber, &FlycapGrabber::terminate, Qt::QueuedConnection);
    QObject::connect(&mainWindow, &MainWindow::aboutToClose, &switcher, &ModeSwitcher::terminate, Qt::QueuedConnection);
    QObject::connect(&switcher, &ModeSwitcher::terminated, &stopper, &GracefulStopper::incTermCounter, Qt::QueuedConnection);
    QObject::connect(&gameEngine, &GameEngine::terminated, &stopper, &GracefulStopper::incTermCounter, Qt::QueuedConnection);
    QObject::connect(&grabber, &FlycapGrabber::terminated, &stopper, &GracefulStopper::incTermCounter, Qt::QueuedConnection);
    QObject::connect(&stopper, &GracefulStopper::finished, &stopperThread, &QThread::quit, Qt::QueuedConnection);
    QObject::connect(&stopperThread, &QThread::finished, &a, &QApplication::quit, Qt::QueuedConnection);

    // Error messaging
    QObject::connect(&grabber, &FlycapGrabber::startFailed, &mainWindow, &MainWindow::notifyCameraError, Qt::QueuedConnection);
    QObject::connect(&grabber, &FlycapGrabber::restartSucceeded, &mainWindow, &MainWindow::notifyCameraRestart, Qt::QueuedConnection);

    // Biding ui controls with game engine
    QObject::connect(&mainWindow, &MainWindow::freeLinkRequested, &gameEngine, &GameEngine::changeFreeLink, Qt::QueuedConnection);
    QObject::connect(&mainWindow, &MainWindow::scenarioLinkRequested, &gameEngine, &GameEngine::changeScenarioLink, Qt::QueuedConnection);
    QObject::connect(&mainWindow, &MainWindow::videoPlayRequested, &gameEngine, &GameEngine::playVideo, Qt::QueuedConnection);
    QObject::connect(&mainWindow, &MainWindow::videoPauseRequested, &gameEngine, &GameEngine::pauseVideo, Qt::QueuedConnection);
    QObject::connect(&mainWindow, &MainWindow::hitDrawModeRequested, &gameEngine, &GameEngine::changeHitDrawMode, Qt::QueuedConnection);
    QObject::connect(&mainWindow, &MainWindow::levelSelected, &gameEngine, &GameEngine::changeLevel, Qt::QueuedConnection);
    QObject::connect(&mainWindow, &MainWindow::resetRequested, &gameEngine, &GameEngine::reset, Qt::QueuedConnection);
    QObject::connect(&mainWindow, &MainWindow::examSelected, &gameEngine, &GameEngine::setExam, Qt::QueuedConnection);
    QObject::connect(&mainWindow, &MainWindow::examShowRequested, &gameEngine, &GameEngine::showExamTarget, Qt::QueuedConnection);
    QObject::connect(&mainWindow, &MainWindow::examHideRequested, &gameEngine, &GameEngine::hideExamTarget, Qt::QueuedConnection);
        
    // Binding ui controls with mode switcher
    QObject::connect(&mainWindow, &MainWindow::modeChangeRequested, &switcher, &ModeSwitcher::changeMode, Qt::QueuedConnection);
    QObject::connect(&switcher, &ModeSwitcher::modeChanged, &mainWindow, &MainWindow::handleNewMode, Qt::QueuedConnection);
    QObject::connect(&switcher, &ModeSwitcher::newDebugFrame, &mainWindow, &MainWindow::showImage, Qt::QueuedConnection);
    QObject::connect(&grabber, &FlycapGrabber::newFrame, &switcher, &ModeSwitcher::processFrame, Qt::QueuedConnection);

    // Mimic laser shot with gamewindow mouse press
    QObject::connect(&gameWindow, &GameWindow::mousePressed, &switcher, &ModeSwitcher::createShot, Qt::QueuedConnection);

    // Bind mode switcher with game engine
    QObject::connect(&switcher, &ModeSwitcher::modeChanged, &gameEngine, &GameEngine::handleNewMode, Qt::QueuedConnection);
    QObject::connect(&switcher, &ModeSwitcher::newLaserPoints, &gameEngine, &GameEngine::processLaserPoints, Qt::QueuedConnection);
    QObject::connect(&gameEngine, &GameEngine::modeChangeRequested, &switcher, &ModeSwitcher::changeMode, Qt::QueuedConnection);
    QObject::connect(&gameEngine, &GameEngine::videoEnded, &mainWindow, &MainWindow::handleVideoEnded, Qt::QueuedConnection);

    // Forwarding key presses to mainWindow
    QObject::connect(&gameWindow, &GameWindow::keyPressed, &mainWindow, &MainWindow::receiveKeyPress, Qt::QueuedConnection);
    
    // Bind data with action & ui classes
    ShotModel shotModel;
    shotModel.setBufferSize(30);
    mainWindow.set(&shotModel);
    gameEngine.set(&shotModel);

    // Show Ui
    mainWindow.set(&grabber);
    mainWindow.show();
    gameWindow.setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint | Qt::WindowMinMaxButtonsHint);
    gameWindow.show();
    switcher.changeMode(MODE::IDLE);

    return a.exec();
}
