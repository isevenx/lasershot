#ifndef CREATIONMODE_H
#define CREATIONMODE_H

#include "mode.h"

namespace MODE
{
    struct CREATION {
        static const Mode TARGET;
        static const Mode LEVEL;
        static const Mode SCENARIO;
    };
}

#endif // CREATIONMODE_H
