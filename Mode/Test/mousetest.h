#ifndef MOUSETEST_H
#define MOUSETEST_H

#include "Mode/runnable.h"
#include "Ui/frameoutputdevice.h"
#include <QScreen>

class MouseTest : public Runnable
{
    Q_OBJECT
public:
    MouseTest(FrameOutputDevice* outDevice);
    ~MouseTest();
    bool isReady();
public slots:
    void processFrame(cv::Mat frame);
    void terminate();

private:
    std::atomic<bool> _isReady;
    FrameOutputDevice* _outDevice;
    QScreen* _screen;
    cv::Mat _outDeviceFrame;
};

#endif // MOUSETEST_H
