#include "delaytest.h"
#include <opencv2/imgproc.hpp>
#include <QDateTime>
#include <QKeyEvent>
#include "Misc/globalsettings.h"
using namespace GlobalSettings;

#define NOW_TS QDateTime::currentMSecsSinceEpoch()

DelayTest::DelayTest(FrameOutputDevice* outDevice, cv::Mat lambda):
    _isReady(true),
    _outDevice(outDevice),
    _lambda(lambda),
    _debugFrame(RENDER_BUFFER_HEIGHT, RENDER_BUFFER_WIDTH, CV_8UC3)
{
    _frameCounter = 0;
    _grabStartTs = QDateTime::currentMSecsSinceEpoch();
    _debugFrame.setTo(0);
    QImage img((const uchar*)_debugFrame.data, _debugFrame.cols, _debugFrame.rows, _debugFrame.step, QImage::Format_RGB888);
    _outDevice->set(img);
}

DelayTest::~DelayTest()
{

}

bool DelayTest::isReady()
{
    return _isReady;
}

void DelayTest::terminate()
{
    _isReady = false;
}

void DelayTest::processFrame(cv::Mat frame)
{
    _isReady = false;
    if(!frame.empty()){

        double min, max;
        cv::Point maxLoc;
        cv::Point result;
        cv::Mat gray;
        cv::cvtColor(frame, gray, CV_RGB2GRAY);
        cv::minMaxLoc(gray, &min, &max, nullptr, &maxLoc);

        if(max > 100){
            std::vector<cv::Point2f> in = {maxLoc};
            std::vector<cv::Point2f> out = {result};
            cv::perspectiveTransform(in, out, _lambda.inv());

//            _debugFrame.setTo(0);
            cv::drawMarker(_debugFrame, out[0], cv::Scalar(255,255,255), cv::MARKER_CROSS, 25, 3);
            QImage img((const uchar*)_debugFrame.data, _debugFrame.cols, _debugFrame.rows, _debugFrame.step, QImage::Format_RGB888);
            _outDevice->set(img);
        }
    }
    _isReady = true;
}
