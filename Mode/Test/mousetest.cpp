#include "mousetest.h"
#include <QCursor>
#include <QApplication>
#include <opencv2/imgproc.hpp>
#include <QThread>

using namespace cv;

MouseTest::MouseTest(FrameOutputDevice* outDevice):
    _isReady(true),
    _outDevice(outDevice),
    _screen(QApplication::primaryScreen()),
    _outDeviceFrame(_screen->geometry().height(), _screen->geometry().width(), CV_8UC3)
{
    _outDeviceFrame = Scalar(255,255,255);
}

MouseTest::~MouseTest()
{

}

bool MouseTest::isReady()
{
    return _isReady;
}

void MouseTest::terminate()
{
    _isReady = false;
}

void MouseTest::processFrame(cv::Mat frame)
{
    _isReady = false;
    if(!frame.empty()){
        QPoint pos = QCursor::pos() - _screen->geometry().topLeft();
        if(pos.x() > 0){
            _outDeviceFrame.setTo(255);
            drawMarker(_outDeviceFrame,  Point(pos.x(), pos.y()), Scalar(0, 0, 255), MARKER_SQUARE, 15, 3);

            QImage tmp((const uchar*)_outDeviceFrame.data, _outDeviceFrame.cols, _outDeviceFrame.rows, _outDeviceFrame.step, QImage::Format_RGB888);
            _outDevice->set(tmp);
        }
    }
    _isReady = true;
}
