#ifndef TESTMODE_H
#define TESTMODE_H

#include "Mode/mode.h"

namespace MODE {
    struct TEST {
        static const Mode LASER_DELAY;
        static const Mode MOUSE_DELAY;
    };
}

#endif // TESTMODE_H
