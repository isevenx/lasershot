#ifndef DELAYTEST_H
#define DELAYTEST_H

#include "Mode/runnable.h"
#include "Ui/frameoutputdevice.h"

class DelayTest : public Runnable
{
    Q_OBJECT
public:
    DelayTest(FrameOutputDevice* outDevice, cv::Mat lambda);
    ~DelayTest();
    bool isReady();
signals:
    void newDebugFrame(cv::Mat frame);
public slots:
    void processFrame(cv::Mat frame);
    void terminate();

private:
private:
    std::atomic<bool> _isReady;
    FrameOutputDevice* _outDevice;
    cv::Mat _lambda;
    qint64 _frameCounter;
    qint64 _grabStartTs;
    cv::Mat _debugFrame;
};

#endif // DELAYTEST_H
