#ifndef SHOTDETECTION_H
#define SHOTDETECTION_H

#include "Mode/runnable.h"
#include "Ui/frameoutputdevice.h"

class LaserDetection : public Runnable
{
    Q_OBJECT
public:
    LaserDetection(cv::Mat lambda);
    ~LaserDetection();
    bool isReady();
    void setRegion(cv::Rect rect);
signals:
    void laserDetected(QVector<QPoint> points);
    void newDebugFrame(cv::Mat frame);
public slots:
    void processFrame(const cv::Mat frame);
    void terminate();

private:
    std::atomic<bool> _isReady;
    cv::Mat _lambda;
    qint64 _frameCounter;
    qint64 _grabStartTs;
    cv::Mat _debugFrame;
    cv::Rect _region;
};

#endif // SHOTDETECTION_H
