#ifndef SHOOTINGMODE_H
#define SHOOTINGMODE_H

#include "Mode/mode.h"

namespace MODE
{
    struct SHOOTING
    {
        static const Mode FREE;
        static const Mode TARGET;
        static const Mode SCENARIO;
        static const Mode EXAM;
    };
}

#endif // SHOOTINGMODE_H
