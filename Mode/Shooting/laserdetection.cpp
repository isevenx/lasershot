#include "laserdetection.h"
#include <opencv2/imgproc.hpp>
#include <QDateTime>
#include <QKeyEvent>
#include "Misc/imagealg.h"
#include "Misc/globalsettings.h"
using namespace GlobalSettings;

#define NOW_TS QDateTime::currentMSecsSinceEpoch()

LaserDetection::LaserDetection(cv::Mat lambda):
    _isReady(true),
    _lambda(lambda),
    _debugFrame(RENDER_BUFFER_HEIGHT, RENDER_BUFFER_WIDTH, CV_8UC3)
{
    _frameCounter = 0;
    _grabStartTs = QDateTime::currentMSecsSinceEpoch();
}

LaserDetection::~LaserDetection()
{

}

bool LaserDetection::isReady()
{
    return _isReady;
}

void LaserDetection::terminate()
{
    _isReady = false;
}

void LaserDetection::setRegion(cv::Rect rect)
{
    _region = rect;
}

void LaserDetection::processFrame(const cv::Mat frame)
{
    _isReady = false;
    if(!frame.empty()){

        double min, max = -1;
        cv::Point maxLoc;
        cv::Mat gray;
        cv::cvtColor(frame, gray, CV_RGB2GRAY);

        if(_region.area() > 0)
            ImageAlg::getMaxLocRegion(gray, _region, &maxLoc, &max);
        else
            cv::minMaxLoc(gray, &min, &max, nullptr, &maxLoc);

        if(max > 100){
            std::vector<cv::Point2f> in = {maxLoc};
            std::vector<cv::Point2f> result;
            cv::perspectiveTransform(in, result, _lambda.inv());

            QVector<QPoint> vec{QPoint(result[0].x, result[0].y)};
            emit laserDetected(vec);
        }
    }
    _isReady = true;
}
