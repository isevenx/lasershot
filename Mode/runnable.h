#ifndef RUNABLEMODE_H
#define RUNABLEMODE_H

#include <QObject>
#include <opencv2/core.hpp>

class Runnable : public QObject
{
    Q_OBJECT
public:
    virtual ~Runnable() = 0;
    virtual bool isReady() = 0;
public slots:
    virtual void processFrame(const cv::Mat frame) = 0;
    virtual void terminate() = 0;
};

inline Runnable::~Runnable(){}

#endif // RUNABLEMODE_H
