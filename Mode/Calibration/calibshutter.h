#ifndef CALIBSHUTTER_H
#define CALIBSHUTTER_H

#include "Mode/runnable.h"
#include <QObject>
#include "Camera/camerasettings.h"
#include "Ui/frameoutputdevice.h"

class CalibShutter : public Runnable
{
    Q_OBJECT
public:
    CalibShutter(CameraSettings* settings, FrameOutputDevice* outDevice);
    ~CalibShutter();
    bool isReady();
    void setRegion(cv::Rect rect);
signals:
    void shutterCalibrated(float percent);
    void finished();
    void newDebugFrame(cv::Mat frame);
public slots:
    void processFrame(const cv::Mat frame);
    void terminate();

private:
    std::atomic<bool> _isReady;
    CameraSettings* _settings;
    FrameOutputDevice* _outDevice;
    bool _shutterCalibrated;
    qint64 _shutterCalibTime;
    float _currentShutter;
    qint64 _frameCounter;
    cv::Mat _debugFrame;
    cv::Rect _region;
};

#endif // CALIBSHUTTER_H
