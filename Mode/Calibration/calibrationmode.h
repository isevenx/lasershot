#ifndef CALIBRATIONMODE_H
#define CALIBRATIONMODE_H

#include "Mode/mode.h"

namespace MODE {
    struct CALIBRATION {
        static const Mode SHOOTING_SHUTTER;
        static const Mode AVG_BRIGHT;
        static const Mode DISPLAY_COORDINATES;
        static const Mode CAMERA_FOCUS;
    };
}

#endif // CALIBRATIONMODE_H
