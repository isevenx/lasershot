#include "calibshutter.h"
#include <opencv2/imgproc.hpp>
#include <QDateTime>
#include "Misc/imagealg.h"

#define NOW_TS QDateTime::currentMSecsSinceEpoch()
using namespace cv;

CalibShutter::CalibShutter(CameraSettings* settings, FrameOutputDevice* outDevice):
    _isReady(true),
    _settings(settings),
    _outDevice(outDevice),
    _shutterCalibrated(false),
    _shutterCalibTime(0),
    _currentShutter(_settings->getShutter()),
    _frameCounter(0)
{
}

CalibShutter::~CalibShutter()
{
}

bool CalibShutter::isReady()
{
    return _isReady;
}

void CalibShutter::terminate()
{
    _isReady = false;
}

void CalibShutter::setRegion(cv::Rect rect)
{
    _region = rect;
}

void CalibShutter::processFrame(const cv::Mat frame)
{
    _isReady = false;

    if(!frame.empty() && !_shutterCalibrated){

        _debugFrame.release();
        _debugFrame = frame.clone();
        _frameCounter++;

        if(!_shutterCalibTime) _shutterCalibTime = NOW_TS;
        if(_shutterCalibTime + 1000 > NOW_TS){
            double min, max;
            cv::Mat gray;
            cv::cvtColor(frame, gray, CV_RGB2GRAY);
            if(_region.area() > 0)
                ImageAlg::getMaxLocRegion(gray, _region, nullptr, &max);
            else
                cv::minMaxLoc(gray, &min, &max);

            if(_frameCounter % 4 == 0){
                if(max > 50){
                    _currentShutter -= 5;
                    if(_currentShutter < 0) _currentShutter = 0;
                    _shutterCalibTime = NOW_TS;
                }
                else if(max < 20)
                    _currentShutter += 1;
                if(max < 10) _shutterCalibTime = NOW_TS;

                QImage img(1920, 1080, QImage::Format_ARGB32);
                img.fill(Qt::white);

                _outDevice->set(img);
                _settings->setShutter(_currentShutter);
            }

            rectangle(_debugFrame, _region, Scalar(255,0,0), 5);
            putText(_debugFrame, QString("Max: %1").arg(max).toStdString().c_str(),
                    Point(50,100), FONT_HERSHEY_SIMPLEX, 2, Scalar(0,200,200), 4);
        }
        else{
            _shutterCalibrated = true;
            emit shutterCalibrated(_currentShutter);
            emit finished();
        }
        emit newDebugFrame(_debugFrame);
    }

    _isReady = true;
}
