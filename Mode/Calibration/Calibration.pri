HEADERS += \
    $$PWD/calibration.h \
    $$PWD/calibrationmode.h \
    $$PWD/calibrod.h \
    $$PWD/calibtest.h \
    $$PWD/calibshutter.h \
    $$PWD/calibfocus.h

SOURCES += \
    $$PWD/calibration.cpp \
    $$PWD/calibrationmode.cpp \
    $$PWD/calibrod.cpp \
    $$PWD/calibtest.cpp \
    $$PWD/calibshutter.cpp \
    $$PWD/calibfocus.cpp
