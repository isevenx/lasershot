#ifndef CALIBRATION_H
#define CALIBRATION_H

#include "calibrod.h"
#include "calibshutter.h"

class Calibration : public Runnable
{
    Q_OBJECT
public:
    Calibration(CameraSettings* settings, FrameOutputDevice* outDevice);
    ~Calibration();
    bool isReady();
signals:
    void newDebugFrame(cv::Mat frame);
    void lambdaCalculated(cv::Mat lambda);
    void shutterCalibrated(float percent);
    void finished();
public slots:
    void processFrame(const cv::Mat frame);
    void terminate();
private slots:
    void sendLambda(cv::Mat lambda);
    void sendShutter(float percent);
private:
    std::atomic<bool> _isReady;
    CalibRod _calibRod;
    CalibShutter _calibShutter;

    bool _rodCalibrated;
    bool _shutterCalibrated;
};

#endif // CALIBRATION_H
