#ifndef CALIBTEST_H
#define CALIBTEST_H

#include <Qt>
#include <vector>
#include "opencv2/imgproc.hpp"
#include "opencv2/opencv.hpp"

using namespace std;
using namespace cv;


class CalibTest
{
public:
    CalibTest();

    bool calibrate(
            vector<Point> &pts, // row,col
            vector<Point2f> &ptsCoordOrig, // x,y
            vector<Point2f> &ptsCoordFound,
            Mat &calibMat,
            Mat &drawFrame
    );

private:
    float dist;
    vector<Point> * pts; // row, col
    vector<Point2f> * ptsCoordOrig; // x, y
    vector<Point2f> * ptsCoordFound; // ~x, ~y
    Mat * drawFrame;

    vector<vector<Point>> rects;
    vector<vector<Point2f>> rectsOrig;
    vector<vector<Point2f>> rectsCamera;

    vector<Point2f> cornersOrig;
    Mat warp;

    bool getAllRects(vector<Point> &pts/*, vector<vector<Point>>& rects*/);
    bool isWithinMarginOfError(vector<Point2f> pts, Point2f &avg);
    bool getAllWarpedCorners();


};

#endif // CALIBTEST_H
