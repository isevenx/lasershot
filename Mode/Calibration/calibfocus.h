#ifndef LIVEVIEWFOCUS_H
#define LIVEVIEWFOCUS_H

#include "Mode/runnable.h"
#include "Camera/camerasettings.h"
#include "Ui/frameoutputdevice.h"

class CalibFocus : public Runnable
{
    Q_OBJECT
public:
    CalibFocus(FrameOutputDevice* outDevice);
    ~CalibFocus();
    bool isReady();
signals:
    void newDebugFrame(cv::Mat frame);
public slots:
    void processFrame(const cv::Mat frame);
    void terminate();

private:
    std::atomic<bool> _isReady;
    FrameOutputDevice* _outDevice;
    QImage _focusImage;
    qint64 _frameCounter;
};

#endif // LIVEVIEWFOCUS_H
