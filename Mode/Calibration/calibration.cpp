#include "calibration.h"
#include "Misc/imagealg.h"
#include "Misc/globalsettings.h"
using namespace GlobalSettings;

Calibration::Calibration(CameraSettings *settings, FrameOutputDevice *outDevice):
    _isReady(true),
    _calibRod(outDevice),
    _calibShutter(settings, outDevice),
    _rodCalibrated(false),
    _shutterCalibrated(false)
{
    connect(&_calibRod, &CalibRod::lambdaCalculated, this, &Calibration::sendLambda, Qt::QueuedConnection);
    connect(&_calibRod, &CalibRod::newDebugFrame, this, &Calibration::newDebugFrame, Qt::QueuedConnection);
    connect(&_calibShutter, &CalibShutter::shutterCalibrated, this, &Calibration::sendShutter, Qt::QueuedConnection);
    connect(&_calibShutter, &CalibShutter::newDebugFrame, this, &Calibration::newDebugFrame, Qt::QueuedConnection);
}

Calibration::~Calibration()
{
}

bool Calibration::isReady()
{
    return _isReady;
}

void Calibration::terminate()
{
    _isReady = false;
    _calibRod.terminate();
    _calibShutter.terminate();
}

void Calibration::processFrame(const cv::Mat frame)
{
    _isReady = false;

    if(!_rodCalibrated) _calibRod.processFrame(frame);
    else if(!_shutterCalibrated) _calibShutter.processFrame(frame);
    else emit finished();

    _isReady = true;
}

void Calibration::sendLambda(cv::Mat lambda)
{
    emit lambdaCalculated(lambda);
    auto projector = ImageAlg::getPerspRegion(lambda, cv::Size(RENDER_BUFFER_WIDTH, RENDER_BUFFER_HEIGHT));
    _calibShutter.setRegion(projector);
    _rodCalibrated = true;
}

void Calibration::sendShutter(float percent)
{
    emit shutterCalibrated(percent);
    _shutterCalibrated = true;
}


