#include "calibrod.h"
#include <opencv2/imgproc.hpp>
#include <QSettings>
#include "Misc/globalsettings.h"
using namespace cv;
using namespace aruco;
using namespace GlobalSettings;

CalibRod::CalibRod(FrameOutputDevice* outDevice):
    _isReady(true),
    _outDevice(outDevice),
    _frameCounter(0),
    _bx(7), _by(4),
#if CV_VERSION_MAJOR == 3 && CV_VERSION_MINOR > 1
    _dictionary(getPredefinedDictionary(DICT_ARUCO_ORIGINAL)),
#else
    _dictionary(new Dictionary(getPredefinedDictionary(DICT_ARUCO_ORIGINAL))),
#endif
    _repeatThreshold(50)
{
    Mat tmp, checkerBoard;

#if CV_VERSION_MAJOR == 3 && CV_VERSION_MINOR > 1
    _board = CharucoBoard::create(_bx+1, _by+1, 0.1, 0.05, _dictionary);
#else
    _board = new CharucoBoard(CharucoBoard::create(_bx+1, _by+1, 0.1, 0.05, *_dictionary));
#endif

    _board->draw(Size(RENDER_BUFFER_WIDTH, RENDER_BUFFER_HEIGHT), tmp, 10, 1);
    if(!tmp.empty()){
         cvtColor(tmp, checkerBoard, CV_GRAY2RGB);
         _checkerBoard = QImage((const uchar*)checkerBoard.data,
                                checkerBoard.cols, checkerBoard.rows, checkerBoard.step,
                                QImage::Format_RGB888).copy();
    }

    std::vector<cv::Point2f> charucoCorners;
    std::vector<int> charucoIds;
    Mat cameraMatrix, distCoeffs;
    std::vector<int> ids;
    std::vector<std::vector<Point2f>> corners;

#if CV_VERSION_MAJOR == 3 && CV_VERSION_MINOR > 1
    detectMarkers(checkerBoard, _dictionary, corners, ids);
#else
   detectMarkers(checkerBoard, *_dictionary, corners, ids);
#endif
    if(ids.size()){
    #if CV_VERSION_MAJOR == 3 && CV_VERSION_MINOR > 1
        interpolateCornersCharuco(corners, ids, checkerBoard, _board, charucoCorners, charucoIds, cameraMatrix, distCoeffs);
    #else
        interpolateCornersCharuco(corners, ids, checkerBoard, *_board, charucoCorners, charucoIds, cameraMatrix, distCoeffs);
    #endif
        if(charucoIds.size()){
            int i = 0;
            for(const auto& corner : charucoCorners)
                _defaultMarkers.append(Marker(charucoIds[i++], corner));
        }
    }

    _outDevice->set(_checkerBoard);
}

CalibRod::~CalibRod()
{

}

bool CalibRod::isReady()
{
    return _isReady;
}

void CalibRod::terminate()
{
    _isReady = false;
}

void CalibRod::processFrame(const cv::Mat frame)
{
    _isReady = false;
    if(!frame.empty() && _lambda.empty()){
        _debugFrame.release();
        _debugFrame = frame.clone();

        if(_frameCounter++ % 10 == 0)
            _outDevice->set(_checkerBoard);

        Mat cameraMatrix, distCoeffs;
        std::vector<int> ids;
        std::vector<std::vector<Point2f>> corners;
        std::vector<cv::Point2f> charucoCorners;
        std::vector<int> charucoIds;

    #if CV_VERSION_MAJOR == 3 && CV_VERSION_MINOR > 1
        detectMarkers(frame, _dictionary, corners, ids);
    #else
           detectMarkers(frame, *_dictionary, corners, ids);
    #endif
        if(ids.size()){
    #if CV_VERSION_MAJOR == 3 && CV_VERSION_MINOR > 1
            interpolateCornersCharuco(corners, ids, frame, _board, charucoCorners, charucoIds, cameraMatrix, distCoeffs);
    #else
            interpolateCornersCharuco(corners, ids, frame, *_board, charucoCorners, charucoIds, cameraMatrix, distCoeffs);
    #endif
            if(charucoIds.size()){

                drawDetectedCornersCharuco(_debugFrame, charucoCorners, charucoIds, cv::Scalar(255, 0, 0));

//                std::vector<Point> coords;
//                std::vector<Point2f> foundOrig;
//                std::vector<Point2f> foundFrame;
//                int i = 0;
//                for(const auto& id : charucoIds){
//                    coords.push_back(Point(id%_bx, _by-1-int(id/_bx)));
//                    foundOrig.push_back(findPoint(_defaultMarkers, id));
//                    foundFrame.push_back(charucoCorners[i++]);
//                }

//                cv::Mat lambda;
//                if(_calibTest.calibrate(coords, foundOrig, foundFrame, _lambda, _debugFrame)){
//                    std::vector<Point2f> cornersOrig = {Point2f(0,0),Point2f(RWidth,RHeight),Point2f(RWidth,0),Point2f(0,RHeight)};
//                    std::vector<Point2f> cornersPicture;

//                    cv::perspectiveTransform(cornersOrig,cornersPicture, _lambda);

//                    for(size_t ci = 0; ci < cornersPicture.size(); ci++)
//                        cv::circle(_debugFrame,cornersPicture[ci],5,Scalar(255,0,0),2);

//                    emit lambdaCalculated(_lambda);
//                    emit finished();
//                }



                auto qdl = findLargestQDL(charucoCorners, charucoIds, Size(frame.cols, frame.rows));
                line(_debugFrame, qdl.topLeft.point, qdl.topRight.point, Scalar(0,0,255), 5);
                line(_debugFrame, qdl.topRight.point, qdl.bottomRight.point, Scalar(0,0,255), 5);
                line(_debugFrame, qdl.bottomRight.point, qdl.bottomLeft.point, Scalar(0,0,255), 5);
                line(_debugFrame, qdl.bottomLeft.point, qdl.topLeft.point, Scalar(0,0,255), 5);

                circle(_debugFrame, qdl.topLeft.point, 5, Scalar(0,0,255), 5);
                circle(_debugFrame, qdl.topRight.point, 5, Scalar(0,255,255), 5);
                circle(_debugFrame, qdl.bottomRight.point, 5, Scalar(255,0,255), 5);
                circle(_debugFrame, qdl.bottomLeft.point, 5, Scalar(255,0,0), 5);

                Point2f cameraPoints[4];
                Point2f originalPoints[4];

                cameraPoints[0] = qdl.topLeft.point;
                originalPoints[0] = findPoint(_defaultMarkers, qdl.topLeft.id);
                cameraPoints[1] = qdl.topRight.point;
                originalPoints[1] = findPoint(_defaultMarkers, qdl.topRight.id);
                cameraPoints[2] = qdl.bottomRight.point;
                originalPoints[2] = findPoint(_defaultMarkers, qdl.bottomRight.id);
                cameraPoints[3] = qdl.bottomLeft.point;
                originalPoints[3] = findPoint(_defaultMarkers, qdl.bottomLeft.id);

                auto lambda = cv::getPerspectiveTransform(originalPoints, cameraPoints);
                std::vector<Point2f> cornersOrig = {Point2f(0,0),Point2f(RENDER_BUFFER_WIDTH, RENDER_BUFFER_HEIGHT),Point2f(RENDER_BUFFER_WIDTH,0),Point2f(0,RENDER_BUFFER_HEIGHT)};
                std::vector<Point2f> cornersPicture;

                cv::perspectiveTransform(cornersOrig,cornersPicture, lambda);

                for(size_t ci = 0; ci < cornersPicture.size(); ci++)
                    cv::circle(_debugFrame,cornersPicture[ci],5,Scalar(255,0,0),2);


                if(charucoIds.size() > 15){
                    for(auto& it : _possibleCorners){
                        if(qdl == it) {
                            it.repeatCount++;
                            qdl.repeatCount = it.repeatCount;
                            break;
                        }
                    }
                    if(!qdl.repeatCount) _possibleCorners.append(qdl);
                    else if(qdl.repeatCount >= _repeatThreshold){
                        _lambda = lambda;
                        emit lambdaCalculated(_lambda);
                        emit finished();
                    }
                }
            }
        }

        emit newDebugFrame(_debugFrame);
    }

    _isReady = true;
}

CalibRod::Marker::Marker():
    id(0)
{}
CalibRod::Marker::Marker(int id, cv::Point2f point):
    id(id), point(point)
{}

bool CalibRod::Marker::operator==(int id) const
{
    return this->id == id;
}

bool CalibRod::Marker::inRange(const Marker& other) const
{
    return (dist(point, other.point) < 10);
}

float CalibRod::dist(const Point2f& p1, const Point2f& p2)
{
    return std::sqrt(std::pow(p1.x-p2.x, 2) + std::pow(p1.y-p2.y, 2));
}

void CalibRod::addDiff(const Mat& frame)
{
    if(!prevFrame.empty()){
        Mat tmp;
        absdiff(prevFrame, frame, tmp);

        if(diffabs.empty()){
            diffabs = tmp.clone();
        }else if(cv::mean(tmp).val[1] > 4){
            diffabs = (diffabs*2+tmp)/3;
        }
        refFrame = 180-diffabs.clone()*4;
    }
    prevFrame = frame.clone();
}

Mat CalibRod::filterBoard(const Mat& frame)
{
    Mat tmp, result;
    cvtColor(frame, result, CV_RGB2GRAY);
//    threshold(result, tmp, 0, 255, THRESH_BINARY+THRESH_OTSU);
//    GaussianBlur(result, tmp, Size(3,3), 0);
//    threshold(result, tmp, 0, 255, THRESH_BINARY+THRESH_OTSU);
    cvtColor(result, tmp, CV_GRAY2RGB);
    result = tmp;

    return result;
}

CalibRod::Quadrilateral CalibRod::findLargestQDL(std::vector<Point2f> points, std::vector<int> ids, Size size)
{
    Quadrilateral result;
    if(ids.size() < 4) return result;

    struct BestPoint { Point2f p; int id = 0; float d = INT_MAX; };
    BestPoint topLeft, topRight, bottomRight, bottomLeft;

    auto checkBest = [](BestPoint& bestP, const Point& p, const int& id, const Point& corner){
        auto d = dist(p, corner);
        if(d < bestP.d){
            bestP.d = d;
            bestP.p = p;
            bestP.id = id;
            return true;
        }
        return false;
    };

    auto findp = [&](BestPoint& bestP, Point corner){
        int i = 0, pos = 0;
        for(const auto& p : points){
            if(checkBest(bestP, p, ids[i++], corner))
                pos = i-1;
        }
        points.erase(points.begin()+pos);
        ids.erase(ids.begin()+pos);
    };

    findp(topLeft, Point(0, 0));
    findp(topRight, Point(size.width, 0));
    findp(bottomRight, Point(size.width, size.height));
    findp(bottomLeft, Point(0, size.height));

    auto mapp = [](BestPoint& bp, Marker& mk){
        mk.point = bp.p;
        mk.id = bp.id;
    };

    mapp(topLeft, result.topLeft);
    mapp(topRight, result.topRight);
    mapp(bottomLeft, result.bottomLeft);
    mapp(bottomRight, result.bottomRight);

    return result;
}

Point2f CalibRod::findPoint(const QVector<Marker>& markers, int id)
{
    Point2f result;
    for(const auto& m : markers){
        if(m == id){
            result = m.point;
            break;
        }
    }
    return result;
}

