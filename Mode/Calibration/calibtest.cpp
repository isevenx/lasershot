#include "calibtest.h"
#include <QDebug>
#include <opencv2/imgproc.hpp>
#include "Misc/globalsettings.h"
using namespace GlobalSettings;

CalibTest::CalibTest():
    dist(30.f),
    cornersOrig({Point2f(0,0),Point2f(RENDER_BUFFER_WIDTH,RENDER_BUFFER_HEIGHT),Point2f(RENDER_BUFFER_WIDTH,0),Point2f(0,RENDER_BUFFER_HEIGHT)})
{

}

bool CalibTest::calibrate(
        vector<Point> &pts, // row,col
        vector<Point2f> &ptsCoordOrig, // x,y
        vector<Point2f> &ptsCoordFound,
        Mat &calibMat,
        Mat &drawFrame
        )
{
    calibMat = Mat();
    if(pts.size() != ptsCoordOrig.size() || ptsCoordOrig.size() != ptsCoordFound.size() || pts.empty()){
        qDebug() << "Wrong arguments";
    }
    this->pts = &pts;
    this->ptsCoordOrig = &ptsCoordOrig;
    this->ptsCoordFound = &ptsCoordFound;
    this->drawFrame = &drawFrame;

    rects.clear();
    rectsOrig.clear();
    rectsCamera.clear();

    qDebug() << "getting rects...";
    bool hasRez = getAllRects(pts);
    if(!hasRez) return false;

    qDebug() << "getting warped corners... "<< rects.size();
    hasRez = getAllWarpedCorners();

    if(hasRez) calibMat = warp;

    return hasRez;
}


bool CalibTest::getAllRects(vector<Point> &pts/*, vector<vector<Point>>& rects*/)
{
    rects.clear();
    if(pts.size() < 4) return false;
    int divx, divy, divy2;
    int divxBefore, divxAfter;

    int divideIdx = 0;
    int before = 0, after = pts.size();
    while(before < after){
        divideIdx++;
        int inrow = 0;
        for(auto it = pts.begin(); it != pts.end(); ++it) if(it->x == divideIdx) inrow++;
        if(after - inrow <= 1){
            divideIdx--;
            break;
        }
        before += inrow;
        after -= inrow;
    }
    divx = divideIdx;
    if(before <= 1) return false;
    divxBefore = before;
    divxAfter = after;

    divideIdx = 0;
    before = 0, after = divxBefore;
    while(before < after){
        divideIdx++;
        int inrow = 0;
        for(auto it = pts.begin(); it != pts.end(); ++it) if(it->y == divideIdx && it->x < divx) inrow++;
        if(after - inrow <= 1){
            divideIdx--;
            break;
        }
        before += inrow;
        after -= inrow;
    }
    divy = divideIdx;
    if(before <= 1) return false;

    divideIdx = 0;
    before = 0, after = divxAfter;
    while(before < after){
        divideIdx++;
        int inrow = 0;
        for(auto it = pts.begin(); it != pts.end(); ++it) if(it->y == divideIdx && it->x >= divx) inrow++;
        if(after - inrow <= 1){
            divideIdx--;
            break;
        }
        before += inrow;
        after -= inrow;
    }
    divy2 = divideIdx;
    if(before <= 1) return false;

    vector<unsigned long> cpts[4];
//    for(auto it = pts.begin(); it != pts.end(); ++it){
//        if(it->x < divx && it->y < divy) cpts[0].push_back(*it); // topleft
//        if(it->x < divx && it->y >= divy) cpts[3].push_back(*it); // bottom left
//        if(it->x >= divx && it->y < divy2) cpts[1].push_back(*it); // topright
//        if(it->x >= divx && it->y >= divy2) cpts[2].push_back(*it); // bottom right
//    }
    for(unsigned long i = 0; i < pts.size(); ++i){
        if(pts[i].x < divx && pts[i].y < divy) cpts[0].push_back(i); // topleft
        if(pts[i].x < divx && pts[i].y >= divy) cpts[3].push_back(i); // bottom left
        if(pts[i].x >= divx && pts[i].y < divy2) cpts[1].push_back(i); // topright
        if(pts[i].x >= divx && pts[i].y >= divy2) cpts[2].push_back(i); // bottom right
    }


    //const int maxRectCount = 100;

    rectsOrig.clear();
    rectsCamera.clear();
    for(auto a = cpts[0].begin(); a != cpts[0].end(); ++a)
        for(auto b = cpts[1].begin(); b != cpts[1].end(); ++b)
            for(auto c = cpts[2].begin(); c != cpts[2].end(); ++c)
                for(auto d = cpts[3].begin(); d != cpts[3].end(); ++d){
                    double e1 = cv::norm(cv::Mat(pts[*a]),cv::Mat(pts[*c]));
                    double e2 = cv::norm(cv::Mat(pts[*a]),cv::Mat(pts[*c]));
                    float d1Length = sqrt(pow(e1,2.)+pow(e2,2.));
                    double e3 = cv::norm(cv::Mat(pts[*b]),cv::Mat(pts[*d]));
                    double e4 = cv::norm(cv::Mat(pts[*b]),cv::Mat(pts[*d]));
                    float d2Length = sqrt(pow(e3,2.)+pow(e4,2.));
                    if(d1Length != d2Length || d2Length < 3.  || pts[*a].y != pts[*b].y) continue;

                    rects.push_back(vector<Point>{pts[*a],pts[*b],pts[*c],pts[*d]});
                    rectsOrig.push_back(vector<Point2f>{ptsCoordOrig->at(*a),ptsCoordOrig->at(*b),ptsCoordOrig->at(*c),ptsCoordOrig->at(*d)});
                    rectsCamera.push_back(vector<Point2f>{ptsCoordFound->at(*a),ptsCoordFound->at(*b),ptsCoordFound->at(*c),ptsCoordFound->at(*d)});
                }
    return true;
}



bool CalibTest::isWithinMarginOfError(vector<Point2f> pts, Point2f &avg)
{
    int minCount = 30;
    float avgX = 0;
    float avgY = 0;
    vector<Point2f> pts2;
    qint64 withinThreshold = 0;

    while((int)pts.size() > minCount || withinThreshold >= minCount){
        avgX = 0;
        avgY = 0;
        withinThreshold = 0;
        pts2.clear();

        for(auto it = pts.begin(); it != pts.end(); ++it){
            avgX += it->x;
            avgY += it->y;
        }
        avgX /= pts.size();
        avgY /= pts.size();

        auto maxIt = pts.end();
        float maxDist = -1.f;

        for(auto it = pts.begin(); it != pts.end(); ++it){
            const float d = abs(it->x - avgX) + abs(it->y - avgY);
            if(maxIt == pts.end() || maxDist > d){
                maxIt = it; maxDist = d;
            }
            if(abs(it->x - avgX) < dist && abs(it->y - avgY) < dist) withinThreshold++;
        }

        if(maxIt != pts.end() && withinThreshold < minCount){
            pts.erase(maxIt);
        }else{
            break;
        }
    }

    avgX = 0;
    avgY = 0;
    for(auto it = pts.begin(); it != pts.end(); ++it){
        avgX += it->x/float(pts.size());
        avgY += it->y/float(pts.size());
    }

    //    for(auto it = pts.begin(); it != pts.end(); ++it){
    //        if(abs(it->x - avgX) < dist && abs(it->y - avgY) < dist) qDebug() << it->x << it->y;
    //    }

    avg.x =avgX;
    avg.y = avgY;
    return int(pts.size()) > minCount;
}

bool CalibTest::getAllWarpedCorners()
{
//    qDebug() << "warping corners" << rectsOrig.size();
    if(rectsOrig.size() != rectsCamera.size() || rectsCamera.empty()) return false;

    vector<Point2f> allcorners[4];

//    int counter = 0;
//    for(const auto& ptsVec : rectsCamera){
//        char r = 0 + counter*30;
//        char g = 100 + counter*30;
//        char b = 0 + counter*30;
//        for(int i = 0; i < ptsVec.size(); i++){
//            circle(*drawFrame, ptsVec[i], 5, Scalar(r,g,b), 3);
//        }
//        if(++counter == 2)break;
//    }

    for(unsigned long i = 0; i < rectsOrig.size(); ++i){
        auto lambda = cv::getPerspectiveTransform(rectsOrig[i], rectsCamera[i]);

        vector<Point2f> cornersPicture;
        cv::perspectiveTransform(cornersOrig,cornersPicture,lambda);
        if(cornersPicture.empty()) continue;
        allcorners[0].push_back(cornersPicture[0]);
        allcorners[1].push_back(cornersPicture[1]);
        allcorners[2].push_back(cornersPicture[2]);
        allcorners[3].push_back(cornersPicture[3]);
        //        qDebug() << "cornersPicture "
        //                 << cornersOrig[1].x << cornersOrig[1].y
        //                 << cornersPicture[1].x << cornersPicture[1].y;
    }

    bool ok = true;
    vector<Point2f> cornersCamera(4);
    for(int c = 0; c < 4; ++c){
        bool tok =  isWithinMarginOfError(allcorners[c],cornersCamera[c]);
        qDebug() << "isWithinMarginOfError " << c << tok << allcorners[c].size();
        if(!tok) ok = false;
    }

    if(!ok) return false;
    warp =  cv::getPerspectiveTransform(cornersOrig, cornersCamera);

    return true;
}
