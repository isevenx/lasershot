#ifndef CALIBROD_H
#define CALIBROD_H

#include "Mode/runnable.h"
#include "Camera/camera.h"
#include "Camera/camerasettings.h"
#include "Ui/frameoutputdevice.h"
#include "calibtest.h"
#include <opencv2/aruco/charuco.hpp>

// Calibrate region of display
class CalibRod : public Runnable
{
    Q_OBJECT
public:
    CalibRod(FrameOutputDevice* outDevice);
    ~CalibRod();
    bool isReady();
signals:
    void lambdaCalculated(cv::Mat lambda);
    void newDebugFrame(cv::Mat frame);
    void finished();
public slots:
    void processFrame(const cv::Mat frame);
    void terminate();

private:
    std::atomic<bool> _isReady;
    FrameOutputDevice* _outDevice;
    cv::Mat _debugFrame;
    qint64 _frameCounter;

    int _bx, _by;
    
    cv::Ptr<cv::aruco::Dictionary> _dictionary;
    cv::Ptr<cv::aruco::CharucoBoard> _board;
    QImage _checkerBoard;
    cv::Mat _lambda;
    CalibTest _calibTest;

    void addDiff(const cv::Mat& frame);
    static float dist(const cv::Point2f& p1, const cv::Point2f& p2);
    static cv::Mat filterBoard(const cv::Mat& frame);

    struct Marker
    {
        Marker();
        Marker(int id, cv::Point2f point);
        bool operator==(int id) const;
        bool inRange(const Marker& other) const;

        int id;
        cv::Point2f point;
    };
    QVector<Marker> _defaultMarkers;
    static cv::Point2f findPoint(const QVector<Marker>& markers, int id);

    struct Quadrilateral
    {
        Marker topLeft, topRight, bottomRight, bottomLeft;
        bool operator==(const Quadrilateral& other)
        {
            const float e = 5;
            return (dist(topLeft.point, other.topLeft.point) < e &&
                    dist(topRight.point, other.topRight.point) < e &&
                    dist(bottomLeft.point, other.bottomLeft.point) < e &&
                    dist(bottomRight.point, other.bottomRight.point) < e);
        }
        int repeatCount = 0;
    };
    QVector<Quadrilateral> _possibleCorners;
    static Quadrilateral findLargestQDL(std::vector<cv::Point2f> points, std::vector<int> ids, cv::Size size);

    qint64 _repeatThreshold;
    cv::Mat prevFrame;
    cv::Mat diffabs;
    cv::Mat refFrame;
    bool isInverted;
    bool sw;
};

#endif // CALIBROD_H
