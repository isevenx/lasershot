#include "calibfocus.h"
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

#include "Misc/globalsettings.h"

using namespace GlobalSettings;

CalibFocus::CalibFocus(FrameOutputDevice* outDevice):
    _isReady(true),
    _outDevice(outDevice),
    _frameCounter(0)
{
    _focusImage.load(ASSETS_IMAGE_PATH + "sharpness.jpg");
}

CalibFocus::~CalibFocus()
{}

bool CalibFocus::isReady()
{
    return _isReady;
}

void CalibFocus::terminate()
{
    _isReady = false;
}

void CalibFocus::processFrame(const cv::Mat frame)
{
    _isReady = false;
    if(_frameCounter++ % 10 == 0)
        _outDevice->set(_focusImage);
    emit newDebugFrame(frame);
    _isReady = true;
}
