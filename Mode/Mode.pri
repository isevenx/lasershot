HEADERS += \
    $$PWD/runnable.h \
    $$PWD/mode.h \
    $$PWD/drawmode.h \
    $$PWD/creationmode.h

include(Calibration/Calibration.pri)
include(Test/Test.pri)
include(Shooting/Shooting.pri)

SOURCES += \
    $$PWD/creationmode.cpp
