#ifndef MODE_H
#define MODE_H

#define BITS_PER_SUBGROUP 5
struct Mode
{
    unsigned int val;

    Mode():val(-1){}
    explicit Mode(unsigned int i) : val(i) {}
    inline Mode& operator=(unsigned int v) { val = v; return *this; }
    inline operator unsigned int() const { return val; }
    inline Mode operator+(int i) const { return Mode((val << BITS_PER_SUBGROUP) + i); }
    inline bool operator==(Mode other) { return other == val; }
};
inline bool operator<=(Mode left, Mode right) {
    while(left) {
        if(left == right) return true;
        left = left >> BITS_PER_SUBGROUP;
    }
    return false;
}

namespace MODE {
    static const Mode IDLE = Mode(0);
    static const Mode SHOOTING = Mode(1);
    static const Mode CALIBRATION = Mode(2);
    static const Mode TEST = Mode(3);
    static const Mode CREATION = Mode(4);
}

#endif // MODE_H
