#ifndef SCENARIOSCENEPIXMAP_H
#define SCENARIOSCENEPIXMAP_H

#include "movablepixmapitem.h"
#include "scenariosceneconnector.h"
#include <QVector>
#include <QMap>
#include "GameItem/scenario.h"

#include "scenariocreationobjecttypes.h"

class ScenarioScenePixmap : public QObject, public MovablePixmapItem    
{
    Q_OBJECT
    
private:
    static const int _thisType = ScenarioObjectType::SCENE_PIXMAP;
    
    QPixmap _thumbnail;
    QGraphicsTextItem* _tooltip;
    QVector<ScenarioSceneConnector*> _connectors;
    ScenarioSceneConnector* _defaultConnector;

public:
    QString path;
    
    ScenarioScenePixmap(QPixmap const& pixmap, QString filepath);
    virtual ~ScenarioScenePixmap();
    
    void setDefaultConnector(ScenarioSceneConnector* conn);
    void addConnector(ScenarioSceneConnector* cnctr);
    void rmConnector(ScenarioSceneConnector* cnctr);
    inline ScenarioSceneConnector* getDefaultConnector() { return _defaultConnector; }
    ScenarioSceneConnector* connectorTo(ScenarioScenePixmap* other);
    // Connections != Connectors, connector is the scenario editor object connecting two scenes,
    // it is replaced by a pointer in the Scenario data structure and by a key-scenarioID pair in JSON
    QMap<VKey, ScenarioScenePixmap*> connections(int* err = nullptr);
    ScenarioScenePixmap* defaultConnection();
    
    void setBorder(QColor color, uint thickness = 10);
    void removeBorder();
    
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    
    inline int type() const { return _thisType; }
};

#endif // SCENARIOSCENEPIXMAP_H
