#include "movablepixmapitem.h"
#include <QGraphicsScene>

#include <numeric>

MovablePixmapItem::MovablePixmapItem(QPixmap const& pixmap) :
    QGraphicsPixmapItem(pixmap)
{
    setFlag(ItemIsMovable, true);
}

void MovablePixmapItem::bringToFront()
{
    for(QGraphicsItem* i : scene()->items()){         
        i->setZValue(i->zValue() - 0.1);
    }
    setZValue(0);
}

void MovablePixmapItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsPixmapItem::mouseMoveEvent(event);
}
void MovablePixmapItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    bringToFront();
    QGraphicsPixmapItem::mousePressEvent(event);
}
