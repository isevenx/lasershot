#ifndef CALIBRATIONVIEW_H
#define CALIBRATIONVIEW_H

#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include "Camera/camerasettings.h"
#include "Mode/mode.h"

namespace Ui { class CalibrationView; }

class CalibrationView : public QWidget
{
    Q_OBJECT
public:
    explicit CalibrationView(QWidget *parent = 0);
    ~CalibrationView();
    void setCameraSettings(CameraSettings*);
public slots:
    void showImage(QImage image);
    void handleNewMode(Mode mode);
private slots:
    void setShutter(int value);
    void resizeEvent(QResizeEvent*);

private:
    Ui::CalibrationView* _ui;
    QGraphicsView* _view;
    QGraphicsScene* _scene;
    QGraphicsPixmapItem* _item;
    CameraSettings* _cameraSettings;
};

#endif // CALIBRATIONVIEW_H
