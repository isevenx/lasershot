#include "screenshotview.h"
#include "ui_screenshotview.h"
#include <QScrollBar>

#include "Misc/globalsettings.h"

using namespace GlobalSettings;
typedef QPair<QListWidgetItem*, QImage> ThumbnailItem;

ScreenShotView::ScreenShotView(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::ScreenShotView),
    _scene(new QGraphicsScene(this)),
    _bgItem(new QGraphicsPixmapItem)
{
    _ui->setupUi(this);
    _ui->listWidget->setSizeAdjustPolicy(QListWidget::AdjustToContents);
    _ui->listWidget->setResizeMode(QListWidget::Adjust);
    _ui->listWidget->setIconSize(QSize(300, 125));
    _ui->listWidget->setSpacing(2);
    _ui->listWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    connect(_ui->listWidget, &QListWidget::itemClicked, this, &ScreenShotView::showImage, Qt::QueuedConnection);

    _scene->addItem(_bgItem);
    _scene->setBackgroundBrush(QBrush(COLOR_GRAY));
    _ui->graphicsView->setScene(_scene);
    _ui->graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _ui->graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _ui->graphicsView->setFrameStyle(QFrame::NoFrame);
    _ui->graphicsView->setInteractive(false);
    _ui->graphicsView->show();
}

ScreenShotView::~ScreenShotView()
{
    delete _ui;
    delete _scene;
}

void ScreenShotView::set(ImageModel* model)
{
    _model = model;
    connect(_model, &ImageModel::dataChanged, this, &ScreenShotView::checkModel, Qt::QueuedConnection);
}

void ScreenShotView::checkModel()
{
    auto images = _model->getImages();
    
    if(!images.size()) {
        showImage(nullptr);
    }

    QMutableVectorIterator<ThumbnailItem> it(_items);
    while(it.hasNext()){
        auto value = it.next();
        if(!images.contains(value.second)){
            if(value.first->isSelected()) {
                value.first->setSelected(false);
                _ui->listWidget->setCurrentItem(nullptr);
                _bgItem->setPixmap(QPixmap());
            }
            delete value.first;
            it.remove();
        }
    }

    for(const auto& image : images){
        bool contains = false;
        for(const auto& item : _items){
            if(item.second == image){
                contains = true;
                break;
            }
        }
        if(!contains) addScreenShot(image);
    }
}

void ScreenShotView::showImage(QListWidgetItem* item)
{
    if(!item) {
        QPixmap blank;
        _bgItem->setPixmap(blank);
    }
    
    for(const auto& it : _items){
        if(it.first == item){
            _bgItem->setPixmap(QPixmap::fromImage(it.second));
            _ui->graphicsView->fitInView(_bgItem);
            break;
        }
    }
}

void ScreenShotView::addScreenShot(QImage image)
{
    if(!image.isNull()){
        auto item = new QListWidgetItem(QIcon(QPixmap::fromImage(image)), QString());
        _ui->listWidget->insertItem(0, item);
        _items.prepend(ThumbnailItem(item, image));
        
        showImage(item);
    }
}
