#include "scenarioscenepixmap.h"

#include "Misc/globalsettings.h"

using namespace GlobalSettings;

ScenarioScenePixmap::ScenarioScenePixmap(QPixmap const& pixmap, QString filepath) :
    MovablePixmapItem(pixmap),
    _thumbnail(pixmap),
    _tooltip(nullptr),
    _defaultConnector(nullptr),
    
    path(filepath)
{
    setCursor(Qt::CursorShape::PointingHandCursor);
    setAcceptHoverEvents(true);
}
ScenarioScenePixmap::~ScenarioScenePixmap()
{
    while(_connectors.size()) {
        delete _connectors.back();
    }
}

void ScenarioScenePixmap::setDefaultConnector(ScenarioSceneConnector* cnctr) 
{
    if(!cnctr || _connectors.contains(cnctr)) {
        if(_defaultConnector) {
            _defaultConnector->makeNonDefault();
        }
        _defaultConnector = cnctr;
    }
}
void ScenarioScenePixmap::addConnector(ScenarioSceneConnector* cnctr)
{
    if(!_connectors.contains(cnctr)) {
        _connectors.push_back(cnctr);
    }
}
void ScenarioScenePixmap::rmConnector(ScenarioSceneConnector* cnctr)
{
    _connectors.removeOne(cnctr);
    if(_defaultConnector == cnctr) {
        _defaultConnector = nullptr;
    }
}
ScenarioSceneConnector* ScenarioScenePixmap::connectorTo(ScenarioScenePixmap* other)
{
    for(ScenarioSceneConnector* c : _connectors) {
        if(c->_twoDirectional) {
            // Recursive connectors are never two-directional by definition
            if(c->_endPts[0] == other && c->_endPts[1] == this) 
            {
                return c;
            }
            if(c->_endPts[1] == other && c->_endPts[0] == this) 
            {
                return c;                
            }
        }
        else if(c->_endPts[1] == other && c->_endPts[0] == this) {
            return c;
        }
    }
    
    return nullptr;
}
QMap<VKey, ScenarioScenePixmap*> ScenarioScenePixmap::connections(int* err)
{
    QMap<VKey, ScenarioScenePixmap*> connections;
    // Assuming that there are no duplicate keys
    for(ScenarioSceneConnector* c : _connectors) {
        VKey k = 0;
        if(c->_keyLabel) {
            k = c->_keyLabel->toPlainText().at(0).toLatin1();
        }
        
        if(err && connections.keys().contains(k)) {
            if(k) {
                *err = 1;
            }
            else {
                *err = 2;
            }
            return connections;
        }

        if(c->_endPts[1] == this) {
            if(c->_twoDirectional || c->_recursive) {
                connections.insert(k, c->_endPts[0]);
            }
        }
        else {
            connections.insert(k, c->_endPts[1]);        
        }
    }
    
    *err = 0;
    return connections;
}
ScenarioScenePixmap* ScenarioScenePixmap::defaultConnection()
{
    if(!_defaultConnector) {
        return nullptr;
    }
    
    return (_defaultConnector->_endPts[1] == this) ? 
                _defaultConnector->_endPts[0] :
                _defaultConnector->_endPts[1];
}


#include <QPainter>
void ScenarioScenePixmap::setBorder(QColor color, uint thickness)
{
    QPixmap withBorder = _thumbnail;
    QPainter painter(&withBorder);
    QPen pen(color);
    pen.setWidth((int)thickness);
    
    painter.setPen(pen);
    painter.drawPath(shape());
    
    setPixmap(withBorder);
}
void ScenarioScenePixmap::removeBorder()
{
    setPixmap(_thumbnail);
}


void ScenarioScenePixmap::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    for(ScenarioSceneConnector* c : _connectors) {
        c->updateEnds();
    }
    
    MovablePixmapItem::mouseMoveEvent(event);
}
void ScenarioScenePixmap::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    for(ScenarioSceneConnector* c : _connectors) {
        c->updateEnds();
    }
    
    MovablePixmapItem::mouseReleaseEvent(event);
}

#include <QToolTip>
#include <QGraphicsScene>
#include <QGraphicsView>

#define TOOLTIP_WIDTH 500.0
void ScenarioScenePixmap::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);
    
    if(!_tooltip) {
        _tooltip = new QGraphicsTextItem(this);
        _tooltip->setDefaultTextColor(Qt::white);
        QFont font = _tooltip->font();
        font.setPixelSize(20);
        _tooltip->setFont(font);
        _tooltip->setHtml("<p align=\"center\">" + path + "</p>");
        _tooltip->setTextWidth(TOOLTIP_WIDTH);
        
        QPointF offset;
        offset.ry() = pixmap().size().height();
        offset.rx() = (pixmap().size().width() - TOOLTIP_WIDTH)/2;
                      
        _tooltip->setPos(offset);
        scene()->addItem(_tooltip);
    }
}
void ScenarioScenePixmap::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);
    
    if(!_tooltip) {
        _tooltip = new QGraphicsTextItem(this);
        _tooltip->setPos(QPointF(pixmap().size().width(), pixmap().size().height()));
        _tooltip->setPlainText(path);
        scene()->addItem(_tooltip);
    }
}
void ScenarioScenePixmap::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    Q_UNUSED(event);
    
    if(_tooltip) {
        delete _tooltip;
        _tooltip = nullptr;
    }
}
