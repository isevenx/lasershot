#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>
#include <QSettings>
#include <QMessageBox>
#include <QFileDialog>
#include <QStyledItemDelegate>

#include "Mode/Calibration/calibrationmode.h"
#include "Mode/Test/testmode.h"
#include "Mode/Shooting/shootingmode.h"
#include "Mode/creationmode.h"

#include "Misc/globalsettings.h"
#include "Misc/videothumbnailgenerator.h"


// @ temp
#include "gamewindow.h"
using namespace MODE;
using namespace GlobalSettings;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    _UISettingsLoaded(false),
    _ui(new Ui::MainWindow),
    _targetScale(1),
    _showTargetPreview(false)
{
    QFile sheet(":/Ui/stylesheet.qss");
    sheet.open(QFile::ReadOnly);
    setStyleSheet(sheet.readAll());
    
    _ui->setupUi(this);
    
    connect(_ui->freeShootingButton, &QPushButton::clicked, this, &MainWindow::playFreeShooting, Qt::QueuedConnection);
    connect(_ui->levelButton, &QPushButton::clicked, this, &MainWindow::playLevel, Qt::QueuedConnection);
    connect(_ui->scenarioButton, &QPushButton::clicked, this, &MainWindow::playScenario, Qt::QueuedConnection);
    connect(_ui->examButton, &QPushButton::clicked, this, &MainWindow::playExam, Qt::QueuedConnection);
    connect(_ui->chooseBgButton, &QPushButton::clicked, this, &MainWindow::chooseBackground, Qt::QueuedConnection);
    connect(_ui->backgroundEntry, &QLineEdit::textChanged, this, &MainWindow::freeLinkRequested, Qt::QueuedConnection);
    connect(_ui->cameraViewButton, &QPushButton::clicked, this, &MainWindow::startLiveFocus, Qt::QueuedConnection);
    connect(_ui->calibrateButton, &QPushButton::clicked, this, &MainWindow::startCalibration, Qt::QueuedConnection);
    connect(_ui->scenarioEditorButton, &QPushButton::clicked, this, &MainWindow::startScenarioCreation, Qt::QueuedConnection);
    connect(_ui->actionTest_Lag, &QAction::triggered, this, &MainWindow::startDelayTest, Qt::QueuedConnection);
    connect(_ui->actionTest_Mouse_Lag, &QAction::triggered, this, &MainWindow::startMouseTest, Qt::QueuedConnection);
    connect(_ui->backButton, &QPushButton::clicked, this, &MainWindow::returnToMain, Qt::QueuedConnection);
    connect(_ui->createTargetButton, &QPushButton::clicked, this, &MainWindow::startTargetCreation, Qt::QueuedConnection);
    connect(_ui->createLevelButton, &QPushButton::clicked, this, &MainWindow::startLevelCreation, Qt::QueuedConnection);
    connect(_ui->resetButton, &QPushButton::clicked, this, &MainWindow::reset, Qt::QueuedConnection);
    connect(_ui->levelSelection, (void (QComboBox::*)(int))&QComboBox::currentIndexChanged, this, &MainWindow::chooseLevel, Qt::QueuedConnection);
    connect(_ui->scenarioSelection, (void (QComboBox::*)(int))&QComboBox::currentIndexChanged, this, &MainWindow::chooseScenario, Qt::QueuedConnection);
    connect(_ui->targetPreviewButton, &QPushButton::clicked, this, &MainWindow::toggleTargetPreview, Qt::QueuedConnection);
    connect(_ui->tabWidget, &QTabWidget::currentChanged, this, &MainWindow::refresh, Qt::QueuedConnection);
    connect(_ui->playButton, &QPushButton::clicked, this, &MainWindow::changeVideoState, Qt::QueuedConnection);

    // Scenario
    connect(_ui->scenarioOperatorView, &ScenarioOperatorView::videoSwitchRequested, this, &MainWindow::scenarioLinkRequested, Qt::QueuedConnection);
    
    // Exam
    connect(_ui->timeEntry, (void (QSpinBox::*)(int))&QSpinBox::valueChanged, this, &MainWindow::updateExamParams, Qt::QueuedConnection);
    connect(_ui->ammoEntry, (void (QSpinBox::*)(int))&QSpinBox::valueChanged, this, &MainWindow::updateExamParams, Qt::QueuedConnection);
    connect(_ui->plusButton, &QPushButton::clicked, this, &MainWindow::incrementScale, Qt::QueuedConnection);
    connect(_ui->minusButton, &QPushButton::clicked, this, &MainWindow::decrementScale, Qt::QueuedConnection);

    _ui->tabWidget->setCurrentIndex(0);

    _ui->targetSelection->setIconSize({ 40, 40 });
    
    QSettings settings("settings.conf", QSettings::NativeFormat);
    restoreGeometry(settings.value("MainWindow").toByteArray());
}

MainWindow::~MainWindow()
{
    saveUI();
    delete _ui;

    QSettings settings("settings.conf", QSettings::NativeFormat);
    settings.setValue("MainWindow", saveGeometry());
}

void MainWindow::closeEvent(QCloseEvent*)
{
    emit aboutToClose();
}

void MainWindow::handleNewMode(Mode mode)
{    
    if(mode <= MODE::CALIBRATION){
        _ui->stackedWidget->setCurrentIndex(1);
        _ui->page_2->handleNewMode(mode);
    }
    else if(mode <= MODE::SHOOTING){
        if(mode == SHOOTING::FREE){
            _ui->stackedWidget->setCurrentIndex(7);
            _ui->playButton->setText("Pause");

        }
        else if(mode == SHOOTING::TARGET)     _ui->stackedWidget->setCurrentIndex(7);
        else if(mode == SHOOTING::SCENARIO) { _ui->stackedWidget->setCurrentIndex(6); _ui->scenarioOperatorView->start(); }
        else if(mode == SHOOTING::EXAM)       _ui->stackedWidget->setCurrentIndex(7);
    }
    else if(mode <= MODE::CREATION){
        if(mode == CREATION::TARGET)        _ui->stackedWidget->setCurrentIndex(3);
        else if(mode == CREATION::LEVEL)    _ui->stackedWidget->setCurrentIndex(4);
        else if(mode == CREATION::SCENARIO) _ui->stackedWidget->setCurrentIndex(5);
    }
    else {
        if(mode == IDLE) {
            refresh();
        }
        _ui->stackedWidget->setCurrentIndex(0);
    }
    
    _mode = mode;
    
    _ui->cameraViewButton->setVisible(_ui->stackedWidget->currentIndex() == 0);
    _ui->calibrateButton->setVisible(_ui->stackedWidget->currentIndex() == 0);
    _ui->backButton->setVisible(_ui->stackedWidget->currentIndex() != 0);
    _ui->resetButton->setVisible(mode <= MODE::SHOOTING);
    _ui->playButton->setVisible(mode == SHOOTING::FREE);
}

void MainWindow::reset()
{
    if(_mode == SHOOTING::SCENARIO) _ui->scenarioOperatorView->start();
    else if(_mode == SHOOTING::FREE) _ui->playButton->setText("Pause");
    emit resetRequested();
}
void MainWindow::handleVideoEnded()
{
    if(_mode == SHOOTING::SCENARIO) {
        _ui->scenarioOperatorView->nextVideo(0);
    }
}

void MainWindow::refresh(int switchedTabIndex)
{
    if(switchedTabIndex == 0) {
        refreshTargetList();
        refreshLevelList();
        refreshScenarioList();
        updateExamParams();
        updateTargetPreview();

        loadUI();
    }
}

void MainWindow::playFreeShooting()
{
    emit modeChangeRequested(MODE::SHOOTING::FREE);
}
void MainWindow::playLevel()
{
    emit modeChangeRequested(MODE::SHOOTING::TARGET);
}
void MainWindow::playScenario()
{
    emit modeChangeRequested(MODE::SHOOTING::SCENARIO);
}
void MainWindow::playExam()
{
    emit modeChangeRequested(MODE::SHOOTING::EXAM);
}

void MainWindow::startLiveFocus()        { emit modeChangeRequested(CALIBRATION::CAMERA_FOCUS); }
void MainWindow::startCalibration()      { emit modeChangeRequested(MODE::CALIBRATION); }
void MainWindow::startMouseTest()        { emit modeChangeRequested(TEST::MOUSE_DELAY); }
void MainWindow::startDelayTest()        { emit modeChangeRequested(TEST::LASER_DELAY); }
void MainWindow::returnToMain()          { emit modeChangeRequested(MODE::IDLE); }
void MainWindow::startTargetCreation()   { emit modeChangeRequested(CREATION::TARGET); }
void MainWindow::startLevelCreation()    { emit modeChangeRequested(CREATION::LEVEL); }
void MainWindow::startScenarioCreation() { emit modeChangeRequested(CREATION::SCENARIO); }

//void MainWindow::resetLink()             { emit newLinkRequested(_ui->backgroundEntry->text()); }

void MainWindow::chooseBackground()
{
    auto result = QFileDialog::getOpenFileName(this, "Choose Background", ASSETS_VIDEO_PATH, "*.mp4 *.avi *.mkv *.jpg *.png *.jpeg");
    if(!result.isEmpty()) {
        _ui->backgroundEntry->setText(result);
    }
    saveUI();
}

void MainWindow::changeVideoState()
{
    if(_ui->playButton->text() == "Pause"){
        emit videoPauseRequested();
        _ui->playButton->setText("Play");
    }
    else{
        emit videoPlayRequested();
        _ui->playButton->setText("Pause");
    }
}

void MainWindow::sendHitDrawMode(int index)
{
    if(index == 0) emit hitDrawModeRequested(FS_CROSS);
    else emit hitDrawModeRequested(SM_SQUARE);
}
void MainWindow::chooseLevel()
{
    QString level = _ui->levelSelection->currentText();
        
    if(!level.isEmpty()) {
        emit levelSelected(ASSETS_LEVEL_PATH + level + ".level");
    }
    
    saveUI();
}
void MainWindow::chooseScenario()
{
    QString scenario = _ui->scenarioSelection->currentText();
        
    if(!scenario.isEmpty()) {
        if(!_ui->scenarioOperatorView->setScenario(ASSETS_SCENARIO_PATH + scenario)) {
            _ui->scenarioButton->setEnabled(false);
            QMessageBox::warning(this, "Warning", "Failed to load scenario");
        }
        else {
            _ui->scenarioButton->setEnabled(true);
        }
    }
    saveUI();    
}
void MainWindow::updateExamParams()
{
    saveUI();
    QString targetPath = ASSETS_TARGET_PATH + _ui->targetSelection->currentText() + ".target";    
    
    emit examSelected(_ui->timeEntry->value(),
                      _ui->ammoEntry->value(),
                      targetPath,
                      _targetScale);
}

void MainWindow::set(CameraSettings* cameraSettings)
{
    _ui->page_2->setCameraSettings(cameraSettings);
}
void MainWindow::set(ImageModel* model)
{
    _ui->page_3->set(model);
    _ui->scenarioOperatorView->set(model);
    _ui->examOperatorView->set(model);
}

void MainWindow::showImage(QImage image)
{
    _ui->page_2->showImage(image);
}

void MainWindow::notifyCameraError()
{
    QMessageBox::warning(this, tr("Error"),
                               tr("Failed to start camera!\n"
                                  "Try to replug camera cable."),
                               QMessageBox::Ok);
}

void MainWindow::notifyCameraRestart()
{
    QMessageBox::information(this, tr("Success"), tr("Camera started!"),
                             QMessageBox::Ok);
}


#include <QKeyEvent>
void MainWindow::receiveKeyPress(int key)
{
    QKeyEvent event(QEvent::KeyPress, key, Qt::NoModifier);
    keyPressEvent(&event);
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
    if(_mode == MODE::SHOOTING::SCENARIO) {
        _ui->scenarioOperatorView->nextVideo(event->key());
    }
    
    QMainWindow::keyPressEvent(event);
}

void MainWindow::refreshTargetList()
{    
    QDir dir(ASSETS_TARGET_PATH);
    QList<QString> targetFiles = dir.entryList(QDir::Files);
    
    
    disconnect(_ui->targetSelection,(void (QComboBox::*)(int)) &QComboBox::currentIndexChanged, this, &MainWindow::updateExamParams);
    disconnect(_ui->targetSelection,(void (QComboBox::*)(int)) &QComboBox::currentIndexChanged, this, &MainWindow::updateTargetPreview);

    _ui->targetSelection->clear();
    
    for(QString& tf : targetFiles) {
        TargetModel target;
        target.fromFile(ASSETS_TARGET_PATH + tf);
        QIcon icon(QPixmap::fromImage(target.getImage()));
        if(tf.endsWith(".target")) {
            _ui->targetSelection->addItem(icon, tf.replace(".target", ""));
        }
    }

    connect(_ui->targetSelection,(void (QComboBox::*)(int)) &QComboBox::currentIndexChanged, this, &MainWindow::updateExamParams, Qt::QueuedConnection);
    connect(_ui->targetSelection,(void (QComboBox::*)(int)) &QComboBox::currentIndexChanged, this, &MainWindow::updateTargetPreview, Qt::QueuedConnection);

    loadUI();
}
void MainWindow::refreshLevelList()
{    
    QDir dir(ASSETS_LEVEL_PATH);
    QList<QString> targetFiles = dir.entryList(QDir::Files);
    
    _ui->levelSelection->clear();
    
    for(QString& tf : targetFiles) {
        if(tf.endsWith(".level")) {
            _ui->levelSelection->addItem(tf.replace(".level", ""));
        }
    }
    
    loadUI();
}
void MainWindow::refreshScenarioList()
{   
    QDir dir(ASSETS_SCENARIO_PATH);
    QList<QString> targetDirs = dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    
    _ui->scenarioSelection->clear();
    
    for(QString& td : targetDirs) {
        bool isScenario = false;
        
        // @ need to rework scenario folder structure/filename logic
        QList<QString> containedFiles = QDir(ASSETS_SCENARIO_PATH + "/" + td).entryList(QDir::Files);
        for(QString& f : containedFiles) {
            if(f == "scenario.json") {
                isScenario = true;
                break;
            }
        }
        
        if(isScenario) {
            _ui->scenarioSelection->addItem(td);
        }
    }
    
    loadUI();
}

void MainWindow::updateTargetPreview()
{  
    if(_showTargetPreview) {
        emit examShowRequested();
    }
    else {
        emit examHideRequested();
    }
}
void MainWindow::incrementScale()
{
    _targetScale += 0.1f;
    updateExamParams();
    updateTargetPreview();
}
void MainWindow::decrementScale()
{
    _targetScale -= 0.1f;
    updateExamParams();
    updateTargetPreview();
}
void MainWindow::toggleTargetPreview()
{
    _showTargetPreview = !_showTargetPreview;
    if(_showTargetPreview) {
        _ui->targetPreviewButton->setText("Hide\nPreview");
    }
    else {
        _ui->targetPreviewButton->setText("Show\nPreview");
    }
    
    saveUI();
    updateTargetPreview();
}


void MainWindow::saveUI()
{
    if(!_UISettingsLoaded) {
        return;
    }
    
    QSettings settings("settings.conf", QSettings::NativeFormat);
    settings.beginGroup("MainWindow_UIState");
    
    settings.setValue("background", _ui->backgroundEntry->text());
    settings.setValue("time", _ui->timeEntry->value());
    settings.setValue("ammo", _ui->ammoEntry->value());
    settings.setValue("selectedScenario", _ui->scenarioSelection->currentText());
    settings.setValue("selectedTarget", _ui->targetSelection->currentText());    
    settings.setValue("selectedLevel", _ui->levelSelection->currentText());
    
    settings.setValue("targetScale", _targetScale);
    settings.setValue("showTargetPreview", _showTargetPreview);
    
    settings.endGroup();
}
void MainWindow::loadUI()
{
    QSettings settings("settings.conf", QSettings::NativeFormat);
    settings.beginGroup("MainWindow_UIState");
    
    _ui->backgroundEntry->setText(settings.value("background", ASSETS_IMAGE_PATH + "test_background.jpg").toString());
    _ui->levelSelection->setCurrentText(settings.value("selectedLevel").toString());
    _ui->timeEntry->setValue(settings.value("time", 10).toInt());
    _ui->ammoEntry->setValue(settings.value("ammo", 10).toInt());
    _ui->scenarioSelection->setCurrentText(settings.value("selectedScenario").toString());
    _ui->targetSelection->setCurrentText(settings.value("selectedTarget").toString());
    
    _targetScale = settings.value("targetScale", 1.0f).toFloat();
    bool tmp = settings.value("showTargetPreview", false).toBool();
    if(tmp != _showTargetPreview) toggleTargetPreview();
    _showTargetPreview = tmp;
    
    settings.endGroup();
    _UISettingsLoaded = true;
}

