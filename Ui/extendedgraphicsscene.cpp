#include "extendedgraphicsscene.h"
#include <QGraphicsSceneDragDropEvent>

#include "Misc/globalsettings.h"
using namespace GlobalSettings;

ExtendedGraphicsScene::ExtendedGraphicsScene(QObject *parent) :
    QGraphicsScene(parent)
{
    QBrush bgBrush(QImage(ASSETS_IMAGE_PATH + "tiles.png").scaled(160, 160));
    setBackgroundBrush(bgBrush);
}

void ExtendedGraphicsScene::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    event->accept();
}
void ExtendedGraphicsScene::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{
    event->accept();
}
void ExtendedGraphicsScene::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
    event->accept();
}
