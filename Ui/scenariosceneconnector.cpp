#include "scenariosceneconnector.h"

#include <QGraphicsScene>
#include <QVector2D>

#include "scenarioscenepixmap.h"
#include "Misc/globalsettings.h"

using namespace GlobalSettings;
class ExtendedLineEdit : public QLineEdit 
{
    void focusOutEvent(QFocusEvent* event) {
        Q_UNUSED(event);
        emit editingFinished();
    }
};

ScenarioSceneConnector::ScenarioSceneConnector(QGraphicsItem* parent) :
    QObject(),
    QGraphicsLineItem(parent),
    QGraphicsEllipseItem(parent),
    _keyLabel(nullptr),
    _keyInput(nullptr),
    
    _recursive(false),
    _twoDirectional(false),
    _default(false)
{
    {
        QPen pen = QGraphicsLineItem::pen();
        pen.setColor(COLOR_BLUE);
        pen.setWidth(6.0);
        pen.setStyle(Qt::CustomDashLine);
        QVector<qreal> pattern;
        pattern << 25.0 << 2.0;
        for(qreal i = 0; i < 100; i += 1) {
            pattern << 1.0 << 3.0;
        }
        pen.setDashPattern(pattern);
        QGraphicsLineItem::setPen(pen);
        QGraphicsLineItem::setZValue(-100000.0); // Always in the background
        
    }
    {
        QPen pen = QGraphicsEllipseItem::pen();
        pen.setColor(QColor(COLOR_BLUE));
        pen.setWidth(6.0);
        pen.setStyle(Qt::CustomDashLine);
        QVector<qreal> pattern;
        pattern << 0 << 5 << 1 << 3 << 1 << 3 << 1 << 3 << 1 << 3 << 1 << 3 << 1 << 3 << 1 << 3; 
        pen.setDashPattern(pattern);
        
        QGraphicsEllipseItem::setSpanAngle(359*16);
        QGraphicsEllipseItem::setPen(pen);
        QGraphicsEllipseItem::setStartAngle(-90*16);
        QGraphicsEllipseItem::setZValue(-100000.0); // Always in the background        
    }
    
    QGraphicsLineItem::setCursor(Qt::CursorShape::PointingHandCursor);        
    QGraphicsEllipseItem::setCursor(Qt::CursorShape::PointingHandCursor);        
}
ScenarioSceneConnector::~ScenarioSceneConnector()
{
    if(_keyLabel) {
        delete _keyLabel;
    }
    if(_keyInput) {
        delete _keyInput;
    }
    
    if(_endPts[0]) {
        _endPts[0]->rmConnector(this);
        _endPts[0] = nullptr;
    }
    if(_endPts[1]) {
        _endPts[1]->rmConnector(this);
        _endPts[1] = nullptr;
    }
}


#include <QGraphicsSceneMoveEvent>
#include <QGraphicsProxyWidget>
#define CIRCLE_RADIUS 25
void ScenarioSceneConnector::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    Q_UNUSED(event);
    if(event->button() != Qt::LeftButton) {
        return;
    }
    
    if(!_keyInput) {
        _keyInput = QPointer<ExtendedLineEdit>(new ExtendedLineEdit());
        QFont font = _keyInput->font();
        font.setPixelSize(20);
        _keyInput->setFont(font);
        QGraphicsProxyWidget* proxy = QGraphicsLineItem::scene()->addWidget(_keyInput);
        proxy->setPos(calcLabelPos());
        
        _keyInput->setFixedSize(_keyInput->size().height(), _keyInput->size().height());
        _keyInput->setFocus();
        if(_keyLabel) {
            _keyInput->setText(_keyLabel->toPlainText().at(0));
            _keyInput->selectAll();
        }
        
        QObject::connect(_keyInput, &ExtendedLineEdit::textChanged, this, &ScenarioSceneConnector::setLabel, Qt::QueuedConnection);
        
        QObject::connect(_keyInput, &ExtendedLineEdit::textChanged, _keyInput, &ExtendedLineEdit::deleteLater, Qt::QueuedConnection);
        QObject::connect(_keyInput, &ExtendedLineEdit::cursorPositionChanged, _keyInput, &ExtendedLineEdit::deleteLater, Qt::QueuedConnection);        
        QObject::connect(_keyInput, &ExtendedLineEdit::editingFinished, _keyInput, &ExtendedLineEdit::deleteLater, Qt::QueuedConnection);        
        QObject::connect(_keyInput, &ExtendedLineEdit::returnPressed, _keyInput, &ExtendedLineEdit::deleteLater, Qt::QueuedConnection);        
        QObject::connect(_keyInput, &ExtendedLineEdit::selectionChanged, _keyInput, &ExtendedLineEdit::deleteLater, Qt::QueuedConnection);        
    }
}

void ScenarioSceneConnector::ScenarioSceneConnector::updateEnds()
{   
    // Test for recursion
    if(_endPts[0] == _endPts[1]) {
        if(!_recursive) {
            QGraphicsLineItem::setVisible(false);
            QGraphicsEllipseItem::setVisible(true);
            _recursive = true;
        }
        
        // Update loop visual position
        QPointF topLeft((100-2*CIRCLE_RADIUS)/2, -CIRCLE_RADIUS*2 + 3);
        QSizeF size(2*CIRCLE_RADIUS, 2*CIRCLE_RADIUS);
        QGraphicsEllipseItem::setRect(QRectF(_endPts[0]->pos() + topLeft, size));
    }
    else {
        if(_recursive) {
            QGraphicsLineItem::setVisible(true);
            QGraphicsEllipseItem::setVisible(false);
            _recursive = false;
        }
    
        // Update line visual position
        QPointF pts[2] = { line().p1(), line().p2() };
        for(int i = 0; i < 2; i++) {
            if(_endPts[i]) {
                QRectF&& brect = _endPts[i]->boundingRect();
                pts[i] = { brect.x() + brect.width()/2, 
                           brect.y() + brect.height()/2 };
                pts[i] += _endPts[i]->pos();
            }
        }
        setLine({ pts[0], pts[1] });
    }
    
    updateLabelPos();
}
void ScenarioSceneConnector::snapToPixmaps()
{
    QPointF pts[2] = { line().p1(), line().p2() };
    
    for(int i = 0; i < 2; i++) {
        QList<QGraphicsItem*> items = QGraphicsLineItem::scene()->items(pts[i], Qt::ItemSelectionMode::IntersectsItemBoundingRect, 
                                                                        Qt::SortOrder::DescendingOrder);
        // Discard all other lines
        while(items.size() && items.first()->type() != ScenarioObjectType::SCENE_PIXMAP) {
            items.removeFirst();
        }
        
        // If anything is left, assume that it's a pixmap
        if(items.size()) {
            _endPts[i] = (ScenarioScenePixmap*)items.first();
        }
        else {
            _endPts[i] = nullptr;
        }
    }
    
    updateEnds();
}
bool ScenarioSceneConnector::lockToPixmaps()
{
    // Both ends must be snapped to something
    if(!_endPts[0] || !_endPts[1]) {
        return false;
    }
    
    // If already connected, don't connect again
    if(_endPts[0]->connectorTo(_endPts[1])) {
        return false;
    }        
    
    // If already connected, only in the opposite direction,
    // make existing connector two-directional
    ScenarioSceneConnector* toThis = _endPts[1]->connectorTo(_endPts[0]);
    if(toThis && !toThis->_twoDirectional) {
        toThis->_twoDirectional = true;
        QPen pen = ((QGraphicsLineItem*)toThis)->pen();
        pen.setStyle(Qt::SolidLine);
        ((QGraphicsLineItem*)toThis)->setPen(pen);
        return false;
    }
    
    _endPts[0]->addConnector(this);
    _endPts[1]->addConnector(this);
    return true;
}
void ScenarioSceneConnector::makeDefault()
{
    if(!_default)  {
        _default = true;
        
        if(_endPts[0]) {
            _endPts[0]->setDefaultConnector(this);
        }
        
        if(_endPts[1] && _twoDirectional) {
            _endPts[1]->setDefaultConnector(this);        
        }
        
        QPen pen = QGraphicsLineItem::pen();
        pen.setColor(COLOR_ORANGE);
        QGraphicsLineItem::setPen(pen);
        
        pen = QGraphicsEllipseItem::pen();
        pen.setColor(COLOR_ORANGE);
        QGraphicsEllipseItem::setPen(pen);
    }
}
void ScenarioSceneConnector::makeNonDefault()
{
    if(_default) {
        _default = false;
        
        if(_endPts[0]) {
            _endPts[0]->setDefaultConnector(nullptr);
        }
        
        if(_endPts[1] && _twoDirectional) {
            _endPts[1]->setDefaultConnector(nullptr);        
        }

        QPen pen = QGraphicsLineItem::pen();
        pen.setColor(COLOR_BLUE);
        QGraphicsLineItem::setPen(pen);
        
        pen = QGraphicsEllipseItem::pen();
        pen.setColor(COLOR_BLUE);
        QGraphicsEllipseItem::setPen(pen);
    }
}

QPointF ScenarioSceneConnector::calcLabelPos()
{
    if(!_recursive) {
        QPointF dir = line().p2() - line().p1();
        QVector2D offset(dir.y(), -dir.x());
        offset.normalize();
        offset *= 20;
        offset += QVector2D(-12.0, -12.0); // Adjusting because QTextEdit's anchor is at its top left
        return (line().p1() + line().p2())/2 + offset.toPointF();
    }
    else {
        QVector2D offset(0, -CIRCLE_RADIUS - 15.0);
        offset += QVector2D(-12.0, -12.0); // Adjusting because QTextEdit's anchor is at its top left
        return (rect().topLeft() + rect().bottomRight())/2 + offset.toPointF();
    }
}
void ScenarioSceneConnector::updateLabelPos()
{
    if(_keyLabel) { 
        _keyLabel->setPos(calcLabelPos());
    }
}


void ScenarioSceneConnector::setLabel(QString const& label)
{
    if(label.size() != 1) {
        return;
    }
    
    if(!_keyLabel) {
        // Note: do not provide parent here, because it will try 
        // to anchor relative to either just the 
        // QGraphicsLineItem or QGraphicsEllipseItem position
        _keyLabel = new QGraphicsTextItem();
        _keyLabel->setDefaultTextColor(Qt::white);
        QFont font = _keyLabel->font();
        font.setPixelSize(20);
        _keyLabel->setFont(font);
        QGraphicsLineItem::scene()->addItem(_keyLabel);
    }
    
    _keyLabel->setPlainText(label.toUpper());
    updateLabelPos();
}
