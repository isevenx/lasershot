#ifndef FRAMEOUTPUTDEVICE_H
#define FRAMEOUTPUTDEVICE_H

#include <QImage>

class FrameOutputDevice
{
public:
    virtual void set(QImage frame) = 0;
};

#endif // FRAMEOUTPUTDEVICE_H
