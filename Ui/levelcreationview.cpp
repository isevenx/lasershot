#include "levelcreationview.h"
#include "ui_levelcreationview.h"
#include "Misc/globalsettings.h"
#include <QFileDialog>
#include <QInputDialog>
#include "GameItem/Action/finishaction.h"
#include <QDebug>
#include <QFormLayout>
#include <QDialogButtonBox>
#include <QTextEdit>
#include <QMessageBox>

using namespace GlobalSettings;

static const int ICON_SIZE = 103;

QIcon getIcon(QString fileName)
{
    return QIcon(QPixmap(fileName).scaled(QSize(ICON_SIZE, ICON_SIZE), Qt::IgnoreAspectRatio, Qt::SmoothTransformation));
}

LevelCreationView::LevelCreationView(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::LevelCreationView),
    _scene(new QGraphicsScene(this)),
    _defaultBg(ASSETS_IMAGE_PATH + "test_background.jpg"),
    _layer(nullptr)
{
    _ui->setupUi(this);

    auto list = _ui->listWidget;
    list->setSizeAdjustPolicy(QListWidget::AdjustToContents);
    list->setResizeMode(QListWidget::Adjust);
    list->setIconSize(QSize(ICON_SIZE, ICON_SIZE));
    list->setGridSize(QSize(ICON_SIZE, ICON_SIZE+20));
    list->setMinimumWidth(ICON_SIZE);
    list->setViewMode(QListView::IconMode);
    list->setFlow(QListView::LeftToRight);
    list->setWrapping(true);
    list->setMovement(QListView::Static);
    list->setDragDropMode(QListView::DragOnly);
    list->setSelectionMode(QAbstractItemView::SingleSelection);
    list->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    _bgModel.setImage(QImage(_defaultBg));
    _bgModel.setSize(QSize(1920, 1080));
    _bgItem = new TargetItem(&_bgModel);
    connect(_bgItem, &TargetItem::dropped, this, &LevelCreationView::addItem, Qt::DirectConnection);
    _scene->addItem(_bgItem);
    QBrush bgBrush(QImage(ASSETS_IMAGE_PATH + "tiles.png"));
    _scene->setBackgroundBrush(bgBrush);

    _ui->graphicsView->setFrameStyle(QFrame::NoFrame);
    _ui->graphicsView->setAcceptDrops(true);
    _ui->graphicsView->setScene(_scene);

    connect(_ui->comboBox, &QComboBox::currentTextChanged, this, &LevelCreationView::changeDragItems, Qt::DirectConnection);

    auto targetThumbnail     = new DragItem(getIcon(ASSETS_IMAGE_PATH + "target_icon.png"),     "Target");
    auto backgroundThumbnail = new DragItem(getIcon(ASSETS_IMAGE_PATH + "background_icon.png"), "Background");
    auto textThumbnail       = new DragItem(getIcon(ASSETS_IMAGE_PATH + "texticon.png"),        "Text");
    auto stopThumbnail       = new DragItem(getIcon(ASSETS_IMAGE_PATH + "stop.png"),            "Finish");
    auto restartThumbnail    = new DragItem(getIcon(ASSETS_IMAGE_PATH + "restart_icon.png"),    "Restart Button");
    auto onHitThumbnail      = new DragItem(getIcon(ASSETS_IMAGE_PATH + "add3.png"),            "On Hit");
    auto missThumbnail       = new DragItem(getIcon(ASSETS_IMAGE_PATH + "missed_icon.png"), "Miss Penalty");

    _dragItems.insert("General", {targetThumbnail, backgroundThumbnail, textThumbnail, restartThumbnail, stopThumbnail});
    _dragItems.insert("Target", {onHitThumbnail, missThumbnail});

    for(auto key : _dragItems.keys())
        _ui->comboBox->addItem(key);
    
    _ui->comboBox->addItem("Layer");

    connect(_ui->saveButton, &QPushButton::pressed, this, &LevelCreationView::saveLevel, Qt::DirectConnection);
    connect(_ui->prevLayerButton, &QPushButton::pressed, this, &LevelCreationView::prevLayer, Qt::DirectConnection);
    connect(_ui->nextLayerButton, &QPushButton::pressed, this, &LevelCreationView::nextLayer, Qt::DirectConnection);
    connect(_ui->addLayerButton, &QPushButton::pressed, this, &LevelCreationView::addLayer, Qt::DirectConnection);
    connect(_ui->openButton, &QPushButton::pressed, this, &LevelCreationView::openLayer, Qt::DirectConnection);

}

LevelCreationView::~LevelCreationView()
{
    delete _ui;
    delete _scene;

    for(auto model : _dragItems)
        for(auto item : model)
            delete item;
    _dragItems.clear();
    _levelModel.clear();
}

void LevelCreationView::clear()
{
    for(auto targetItems : _layerItems){
        for(auto item : targetItems){
            _scene->removeItem(item);
            delete item;
        }
    }
    _layerItems.clear();

    auto layerCount = _levelModel.getLayers().size();
    for(int i = 0; i < layerCount; i++) {
        auto layerName = QString::number(i);
        auto it = _dragItems.find(layerName);
        if(it != _dragItems.end()){
            for(auto item : it.value()) delete item;
            _dragItems.remove(layerName);
        }
    }

    _levelModel.clear();
    addLayer();
    _curLayerName = QString::number(0);

    _ui->graphicsView->fitInView(_bgItem, Qt::KeepAspectRatio);
}

void LevelCreationView::showEvent(QShowEvent* event)
{
    QWidget::showEvent(event);
    clear();
}

void LevelCreationView::resizeEvent(QResizeEvent* event)
{
    _ui->graphicsView->fitInView(_bgItem, Qt::KeepAspectRatio);
    QWidget::resizeEvent(event);
}

TargetItem* LevelCreationView::addTargetItem(TargetModel* model)
{
    auto item = new TargetItem(model);
    item->setMovable(true);
    item->setResizeable(true);
    item->setBorder(true);
    item->setParentItem(_bgItem);

    connect(item, &TargetItem::dropped, this, &LevelCreationView::addItem, Qt::QueuedConnection);
//    auto layerItem = dragItem->clone();
//    layerItem->setDataPtr(target);
//    _dragItems[_curLayerName].append(layerItem);
    _layerItems[_curLayerName].append(item);
    return item;
}

void LevelCreationView::addItem(DragItem* dragItem, QPointF pos)
{
    auto item = static_cast<TargetItem*>(sender());
    if(!item){
        qWarning() << "FAILED TO DROP!";
        return;
    }

    if(dragItem->getName() == "Target"){
        auto result = QFileDialog::getOpenFileName(this, "Choose Target", ASSETS_TARGET_PATH, "*.target");
        if(!result.isEmpty()){
            auto target = new TargetModel;
            if(target->fromFile(result)){
                target->setPosition(pos);
                _layer->add(target);
                addTargetItem(target);
            }
            else delete target;
        }
    }
    else if(dragItem->getName() == "Background"){
        auto result = QFileDialog::getOpenFileName(this, "Choose Background", ASSETS_IMAGE_PATH, "*.jpg *jpeg *png");
        if(!result.isEmpty()){
            _layer->setBackground(result);
            _bgModel.setImage(QImage(result));
        }
    }
    else if(dragItem->getName() == "Finish"){
        QDialog d(this);
        QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        connect(&buttonBox, &QDialogButtonBox::accepted, &d, &QDialog::accept);
        connect(&buttonBox, &QDialogButtonBox::rejected, &d, &QDialog::reject);
        QLineEdit shotLimit;
        QLineEdit timeLimit;

        QFormLayout layout;
        layout.addRow("Shot limit:", &shotLimit);
        layout.addRow("Time limit(seconds):", &timeLimit);
        layout.addWidget(&buttonBox);
        d.setLayout(&layout);
        d.setWindowTitle("Layer Finish:");

        auto result = d.exec();
        if(result == QDialog::Accepted){
            _layer->clearLayerActions(FINISH);
            if(!shotLimit.text().isEmpty())
                _layer->add(new FinishAction(SHOT_EVENT, FINISH, shotLimit.text().toInt()));
            if(!timeLimit.text().isEmpty())
                _layer->add(new FinishAction(TIME_EVENT, FINISH, timeLimit.text().toInt()*1000));
            auto layerItem = dragItem->clone();
            _dragItems[_curLayerName].append(layerItem);
        }
    }
    else if(dragItem->getName() == "On Hit"){
        auto model = item->getModel();
        QString defaultStr = "NoChange";
        QDialog d(this);
        QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        connect(&buttonBox, &QDialogButtonBox::accepted, &d, &QDialog::accept);
        connect(&buttonBox, &QDialogButtonBox::rejected, &d, &QDialog::reject);
        QComboBox posBox;
        posBox.addItem(defaultStr);
        posBox.addItem("Random");
        QFormLayout layout;
        layout.addRow("Change position:", &posBox);
        layout.addWidget(&buttonBox);
        d.setLayout(&layout);
        d.setWindowTitle("On target hit:");

        auto result = d.exec();
        if(result == QDialog::Accepted){
            _layer->clearTargetActions(model);
            if(posBox.currentText() == "Random"){
                _layer->add(new PosChange(model));
            }
        }
    }
    else if(dragItem->getName() == "Text"){
        QDialog d(this);
        QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        connect(&buttonBox, &QDialogButtonBox::accepted, &d, &QDialog::accept);
        connect(&buttonBox, &QDialogButtonBox::rejected, &d, &QDialog::reject);
        QTextEdit textFormat;
        QFormLayout layout;
        layout.addRow("Text Format:", &textFormat);
        layout.addWidget(&buttonBox);
        d.setLayout(&layout);
        d.setWindowTitle("Text Format:");

        auto result = d.exec();
        if(result == QDialog::Accepted){
            auto target = new TargetModel;
            target->setSize(QSize(500, 300));
            target->setPosition(pos);
            _layer->add(target);

            auto textAction = new TextAction(target, textFormat.toPlainText());
            _layer->add(textAction);

            auto item = addTargetItem(target);
            item->setAcceptDrops(false);
        }
    }
    else if(dragItem->getName() == "Restart Button"){

        auto target = new TargetModel;
        QImage iconImage(ASSETS_IMAGE_PATH + "restart_icon.png");
        target->setImage(iconImage);
        QPolygonF region({QPoint(0,0), QPoint(iconImage.width(), 0), QPoint(iconImage.width(), iconImage.height()), QPoint(0, iconImage.height())});
        target->addRegion(region, 0);
        target->setPosition(pos);

        _layer->add(target);
        auto action = new TargetButton(target);
        _layer->add(action);
        auto finishAction = new FinishAction(action, RESTART);
        _layer->add(finishAction);

        addTargetItem(target);
    }
    else if(dragItem->getName() == "Miss Penalty"){
        QDialog d(this);
        QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        connect(&buttonBox, &QDialogButtonBox::accepted, &d, &QDialog::accept);
        connect(&buttonBox, &QDialogButtonBox::rejected, &d, &QDialog::reject);
        QLineEdit missPenalty;
        QFormLayout layout;
        layout.addRow("Score penalty:", &missPenalty);
        layout.addWidget(&buttonBox);
        d.setLayout(&layout);
        d.setWindowTitle("Miss penalty");

        auto result = d.exec();
        if(result == QDialog::Accepted){
            _layer->setMissPenalty(missPenalty.text().toDouble());
        }
    }

}

void LevelCreationView::prevLayer()
{
    changeLayer(_curLayerName.toInt()-1);
}

void LevelCreationView::nextLayer()
{
    changeLayer(_curLayerName.toInt()+1);
}

void LevelCreationView::lastLayer()
{
    changeLayer(_levelModel.getLayers().size()-1);
}

void LevelCreationView::openLayer()
{
    clear();

    auto result = QFileDialog::getOpenFileName(this, "Choose level to edit", ASSETS_LEVEL_PATH);
    if(!result.isEmpty() && _levelModel.fromFile(result)){
        int id = 0;
        for(auto layer : _levelModel.getLayers()){
            changeLayer(id++);
            for(auto target : layer->getTargets())
                addTargetItem(target);
        }
    }
    else QMessageBox::warning(this, "Open Layer", QString("Failed to open: %1").arg(result));
}

void LevelCreationView::changeLayer(int id)
{
    if(id >= 0 && id < _levelModel.getLayers().size()){
        for(auto target : _layerItems[_curLayerName])
            target->hide();

        _layer = _levelModel.getLayers()[id];
        _curLayerName = QString::number(id);
        for(auto target : _layerItems[_curLayerName])
            target->show();

        _bgModel.setImage(QImage(_layer->getBackground()));
        if(_ui->comboBox->currentText() == "Layer") changeDragItems("Layer");
    }
}

void LevelCreationView::addLayer()
{
    _layer = new LayerModel;
    _layer->setBackground(_defaultBg);
    _levelModel.add(_layer);
    lastLayer();
}

void LevelCreationView::changeDragItems(QString name)
{
    _ui->listWidget->clear();

    if(name == "Layer"){
        _ui->listWidget->setDragEnabled(false);
        for(auto item : _dragItems.value(_curLayerName))
            _ui->listWidget->addItem(item->clone());
    }
    else{
        _ui->listWidget->setDragEnabled(true);
        for(auto item : _dragItems.value(name))
            _ui->listWidget->addItem(item->clone());
    }
}

void LevelCreationView::saveLevel()
{
    auto result = QInputDialog::getText(this, "Choose level name", "Level name:");
    QDir tmp; tmp.mkpath(ASSETS_LEVEL_PATH);
    _levelModel.toFile(ASSETS_LEVEL_PATH + result + ".level");
}
