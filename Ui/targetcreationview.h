#ifndef TARGETCREATIONVIEW_H
#define TARGETCREATIONVIEW_H

#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include "targetitem.h"

namespace Ui { class TargetCreationView; }

class TargetCreationView : public QWidget
{
    Q_OBJECT

public:
    explicit TargetCreationView(QWidget *parent = 0);
    ~TargetCreationView();

private slots:
    void setImage();
    void resetSetRegion();
//    void createRegion();
//    void addPart();
//    void removePart();
//    void createPart();
    void addRegion();
    void removeLastRegion();
    void save();
    void import();
    void showEvent(QShowEvent*);

private:
    Ui::TargetCreationView* _ui;
    QGraphicsScene* _scene;

    TargetModel _model;
    TargetItem* _item;

    QString _targetPath;

    void clear();
};

#endif // TARGETCREATIONVIEW_H
