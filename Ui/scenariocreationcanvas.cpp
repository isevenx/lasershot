#include "scenariocreationcanvas.h"
#include <QDropEvent>
#include <QListWidgetItem>
#include <QGraphicsItem>

#include "scenarioscenepixmap.h"
#include "extendedgraphicsscene.h"
#include "scenariosceneconnector.h"

#include "Misc/videothumbnailgenerator.h"
#include "Misc/globalsettings.h"

using namespace GlobalSettings;

#define BLUE_BORDER_THICKNESS 9

ScenarioCreationCanvas::ScenarioCreationCanvas(QWidget* parent) :
    QGraphicsView(parent),
    _pos(0, 0),
    _size(size().width(), size().height()),
    
    _panning(false),
    _connector(nullptr)
{
    setScene(new ExtendedGraphicsScene());
    setSceneRect({ _pos, _size });
    setAlignment(Qt::AlignTop | Qt::AlignLeft);
    setAcceptDrops(true);
    setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);    
    
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, &QWidget::customContextMenuRequested, this, &ScenarioCreationCanvas::showContextMenu, Qt::QueuedConnection);
}

Scenario* ScenarioCreationCanvas::getScenario()
{
    if(!_firstScene) {
        return nullptr;
    }
    
    Scenario* scenario = new Scenario();
    
    QMap<ScenarioScenePixmap*, Scene*> pixmapScenes;
    scenario->scenes.reserve(scene()->items().size());
    scenario->scenes.push_back(Scene(_firstScene->path, _firstScene->pos()));
    pixmapScenes.insert(_firstScene, &scenario->scenes.back());
    
    // Map graphics to actual data structure
    QList<QGraphicsItem*>&& items = scene()->items();
    for(auto it = items.begin(); it != items.end(); it++) {
        if((*it)->type() != ScenarioObjectType::SCENE_PIXMAP || *it == _firstScene) {
            continue;
        }
        
        ScenarioScenePixmap* pixmap = (ScenarioScenePixmap*)*it;
        scenario->scenes.push_back(Scene(pixmap->path, 
                                         pixmap->pos()));
        pixmapScenes.insert(pixmap, &scenario->scenes.back());
    }
    
    // Link up scenes
    for(auto it = pixmapScenes.begin(); it != pixmapScenes.end(); it++) {
        QMap<VKey, ScenarioScenePixmap*>&& connections = it.key()->connections(&c_lastErrCode);
        if(c_lastErrCode) {
            delete scenario;
            return nullptr;
        }
        for(auto c = connections.begin(); c != connections.end(); c++) {
            it.value()->connect(c.key(), pixmapScenes.value(c.value()));
        }
        
        ScenarioScenePixmap* defConn = it.key()->defaultConnection();
        if(defConn) {
            it.value()->defaultTransition.first = pixmapScenes.value(defConn)->_id;
            it.value()->defaultTransition.second = pixmapScenes.value(defConn);
        }
    }
    
    return scenario;    
}
void ScenarioCreationCanvas::setScenario(Scenario* scenario) 
{
    QMap<Scene*, ScenarioScenePixmap*> pixmapScenes;
    for(Scene& s : scenario->scenes) {
        QImage thumbnail = getThumbnail(scenario->path + "/" + s.path);
        if(thumbnail.isNull()) {
            thumbnail = QImage({ 100, 80 }, QImage::Format::Format_RGB32);
            thumbnail.fill(Qt::black);
        }
        ScenarioScenePixmap* pixmap = new ScenarioScenePixmap(QPixmap::fromImage(thumbnail.scaled(100, 80)), s.path);
        pixmap->setBorder(COLOR_BLUE, BLUE_BORDER_THICKNESS);
        pixmapScenes.insert(&s, pixmap);
        scene()->addItem(pixmap);
        pixmap->setPos(s.pos);
        pixmap->bringToFront();
        if(!_firstScene) {
            _firstScene = pixmap;
            _firstScene->setBorder(COLOR_ORANGE);
        }
    }
    
    for(Scene& s : scenario->scenes) {
        ScenarioScenePixmap* start = pixmapScenes.value(&s);
        QMap<VKey, Scene*>&& conns = s.connections();
        for(auto it = conns.begin(); it != conns.end(); it++) {
            ScenarioScenePixmap* end = pixmapScenes.value(it.value());
            ScenarioSceneConnector* connector = new ScenarioSceneConnector();
            scene()->addItem((QGraphicsLineItem*)connector);
            scene()->addItem((QGraphicsEllipseItem*)connector);
            connector->setLine({ start->pos(), end->pos() });
            connector->snapToPixmaps();
            if(!connector->lockToPixmaps()) {
                connector->deleteLater();
                continue;
            }
            if(it.key()) {
                connector->setLabel(QString((char)it.key()));
            }
            if(s.defaultTransition.second == it.value()) {
                connector->makeDefault();
            }
        }
    }
}

QString ScenarioCreationCanvas::getError()
{
    if(!_firstScene) {
        return "No first scene set!";
    }
    
    switch(c_lastErrCode) {
        case 1:
        {
            return "Some scenes have duplicate connection keys!";
        }
        case 2:
        {
            return "Some scenes have multiple anonymous connections!";
        }
        
        default: break;
    }
    
    return QString();
}

void ScenarioCreationCanvas::dropEvent(QDropEvent *event)
{
    QListWidget* src = (QListWidget*)event->source();
    ScenarioScenePixmap* pixmap = new ScenarioScenePixmap(src->currentItem()->icon().pixmap(100, 80), src->currentItem()->text());
    pixmap->setBorder(COLOR_BLUE, BLUE_BORDER_THICKNESS);
    scene()->addItem(pixmap);
    pixmap->setPos(event->posF() - QPointF(50.0f, 40.0f) + _pos);
    pixmap->bringToFront();
    if(!_firstScene) {
        _firstScene = pixmap;
        _firstScene->setBorder(COLOR_ORANGE);
    }
    
    if(_connector) {
        _connector->deleteLater();
        _connector = nullptr;
    }
}
void ScenarioCreationCanvas::mousePressEvent(QMouseEvent *event)
{
    if(_connector) {
        _connector->deleteLater();
        _connector = nullptr;
    }
    if(event->button() == Qt::RightButton) {
        _panning = true;
        _panStart = event->pos();
    }
    else {
        _panning = false;
    }

    if(event->button() == Qt::MiddleButton) {
        QList<QGraphicsItem*>&& items = scene()->items(event->pos() + _pos);
        while(items.size() && items.first()->type() != ScenarioObjectType::SCENE_PIXMAP) {
            items.removeFirst();
        }
            
        if(items.size()) {
            _connector = new ScenarioSceneConnector();
            scene()->addItem((QGraphicsLineItem*)_connector);
            scene()->addItem((QGraphicsEllipseItem*)_connector);
            _connector->setLine({event->pos() + _pos, event->pos() + _pos});
            _connector->snapToPixmaps();
        }
    }
    
    QGraphicsView::mousePressEvent(event);
}
void ScenarioCreationCanvas::mouseReleaseEvent(QMouseEvent *event)
{
    if(_panning) {
        _panning = false;
        _pos = _pos - event->pos() + _panStart;
    }
    
    if(event->button() == Qt::MiddleButton) {
        if(_connector) {
            _connector->setLine({_connector->line().p1(), event->pos() + _pos});
            _connector->snapToPixmaps();
            if(!_connector->lockToPixmaps()) {
                _connector->deleteLater();
            }
            _connector = nullptr;
        }
    }
 
    if(_connector) {
        _connector->deleteLater();
        _connector = nullptr;
    }
    
    QGraphicsView::mouseReleaseEvent(event);
}
void ScenarioCreationCanvas::mouseMoveEvent(QMouseEvent *event)
{
    if(_panning) {
        setSceneRect({ _pos - event->pos() + _panStart, _size });
    }

    if(_connector) {
        _connector->setLine({_connector->line().p1(), event->pos() + _pos});
        _connector->snapToPixmaps();
    }
    
    QGraphicsView::mouseMoveEvent(event);
}


typedef QPair<void*, int> ContextActionData; 
Q_DECLARE_METATYPE(ContextActionData)
#include <QMenu>
void ScenarioCreationCanvas::showContextMenu(QPoint pos)
{
    QList<QGraphicsItem*>&& items = scene()->items(pos + _pos, Qt::ItemSelectionMode::IntersectsItemShape);

    if(items.isEmpty()) {
        return;
    }
    
    // !!! IMPORTANT !!! (1)
    // This pointer is offset from ScenarioScenePixmap* by sizeof(QObject) because of multiple inheritance:
    //  [sizeof(QObject) bytes] [sizeof(QGraphicsPixmapItem) bytes]
    //                         ^ item pointer is here
    // 
    // Applies to both ScenarioScenePixmap and ScenarioSceneConnector
    QGraphicsItem* item = items.first();
    
    QMenu* menu = new QMenu(this);
    switch(item->type()) {
        case ScenarioObjectType::CONNECTOR:
        {
            // !!! IMPORTANT !!! (2)
            // Explicit cast, see (3)
            //  [sizeof(QObject) bytes] [sizeof(QGraphicsLineItem) bytes] [sizeof(QGraphicsEllipseItem) bytes]
            // ^ casted pointer is here
            ScenarioSceneConnector* conn = dynamic_cast<ScenarioSceneConnector*>(item);
            
            QAction* actionMakeDefault; 
            if(!conn->isDefault()) {
                actionMakeDefault = new QAction("Make default", this);
            }
            else {
                actionMakeDefault = new QAction("Make non-default", this);
            }
            connect(actionMakeDefault, &QAction::triggered, this, &ScenarioCreationCanvas::execAction, Qt::QueuedConnection);
            actionMakeDefault->setData(QVariant::fromValue(ContextActionData(conn, ScenarioContextMenuActions::MAKE_DEFAULT)));
            menu->addAction(actionMakeDefault);
            

            QAction* actionDelete = new QAction("Delete", this);
            connect(actionDelete, &QAction::triggered, this, &ScenarioCreationCanvas::execAction, Qt::QueuedConnection);
            actionDelete->setData(QVariant::fromValue(ContextActionData(item, ScenarioContextMenuActions::DELETE)));
            menu->addAction(actionDelete);
            
            break;
        }
        
        case ScenarioObjectType::SCENE_PIXMAP:
        {
            // !!! IMPORTANT !!! (3)
            // Explicit cast to (ScenarioScenePixmap*) because the cast fixes the aforementioned offset:
            //  [sizeof(QObject) bytes] [sizeof(QGraphicsPixmapItem) bytes]
            // ^ casted pointer is here
            // 
            // THIS DOES NOT HAPPEN AUTOMATICALLY BECAUSE ContextActionData::first IS void*
            ScenarioScenePixmap* scene = (ScenarioScenePixmap*)item;
            
            QAction* actionMakeFirst = new QAction("Make first", this);
            connect(actionMakeFirst, &QAction::triggered, this, &ScenarioCreationCanvas::execAction, Qt::QueuedConnection);
            actionMakeFirst->setData(QVariant::fromValue(ContextActionData(scene, ScenarioContextMenuActions::MAKE_FIRST)));
            menu->addAction(actionMakeFirst);
            
            
            QAction* actionDelete = new QAction("Delete", this);
            connect(actionDelete, &QAction::triggered, this, &ScenarioCreationCanvas::execAction, Qt::QueuedConnection);
            actionDelete->setData(QVariant::fromValue(ContextActionData(item, ScenarioContextMenuActions::DELETE)));
            menu->addAction(actionDelete);
            
            break;            
        }
        
        default: break;
    }

    menu->popup(viewport()->mapToGlobal(pos));
    _panning = false;
}

void ScenarioCreationCanvas::execAction()
{
    ContextActionData data = ((QAction*)QObject::sender())->data().value<ContextActionData>();
    
    switch(data.second) {
        case ScenarioContextMenuActions::DELETE:
        {
            delete (QGraphicsItem*)data.first;
            break;            
        }
        
        case ScenarioContextMenuActions::MAKE_DEFAULT:
        {
            ScenarioSceneConnector* conn = (ScenarioSceneConnector*)data.first;
            
            if(!conn->isDefault()) {
                conn->makeDefault();
            }
            else {
                conn->makeNonDefault();
            }
            
            break;
        }
        
        case ScenarioContextMenuActions::MAKE_FIRST:
        {
            if(_firstScene) {
                _firstScene->setBorder(COLOR_BLUE, BLUE_BORDER_THICKNESS);
            }
            
            _firstScene = (ScenarioScenePixmap*)data.first;
            _firstScene->setBorder(COLOR_ORANGE);
            
            break;
        }
        
        default: break;
    }
}
