#ifndef MOVABLEIMAGEITEM_H
#define MOVABLEIMAGEITEM_H

#include <QGraphicsPixmapItem>

class MovablePixmapItem : public QGraphicsPixmapItem
{
public:
    MovablePixmapItem(QPixmap const& pixmap);

    void bringToFront();
    
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
};

#endif // MOVABLEIMAGEITEM_H
