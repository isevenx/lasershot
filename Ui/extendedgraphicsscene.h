#ifndef EXTENDEDGRAPHICSSCENE_H
#define EXTENDEDGRAPHICSSCENE_H

#include <QGraphicsScene>

class ExtendedGraphicsScene : public QGraphicsScene
{
public:
    ExtendedGraphicsScene(QObject *parent = nullptr);
    
    virtual void dragEnterEvent(QGraphicsSceneDragDropEvent *event);
    virtual void dragMoveEvent(QGraphicsSceneDragDropEvent *event);
    virtual void dragLeaveEvent(QGraphicsSceneDragDropEvent *event);
};

#endif // EXTENDEDGRAPHICSSCENE_H
