#ifndef EXTENDEDLISTWIDGET_H
#define EXTENDEDLISTWIDGET_H

#include <QListWidget>
#include <QObject>

class ExtendedListWidget : public QListWidget
{
    Q_OBJECT
    
public:
    using QListWidget::QListWidget;
    
signals:
    void keyPressed(QKeyEvent *event);
    
private slots:
    void keyPressEvent(QKeyEvent *event)
    {
        emit keyPressed(event);
        QListWidget::keyPressEvent(event);
    }
};

#endif // EXTENDEDLISTWIDGET_H
