#ifndef SCENARIOCREATIONOBJECTTYPES_H
#define SCENARIOCREATIONOBJECTTYPES_H

namespace ScenarioObjectType {
enum {
    INVALID,
    CONNECTOR,
    SCENE_PIXMAP,
};
}

namespace ScenarioContextMenuActions {
enum {
    INVALID,
    DELETE,
    MAKE_DEFAULT,
    MAKE_FIRST,
};
}

#endif // SCENARIOCREATIONOBJECTTYPES_H
