#ifndef SCALINGLABEL_H
#define SCALINGLABEL_H

#include <QLabel>

class ScalingLabel : public QLabel
{
public:
    using QLabel::QLabel;
    
    void resizeEvent(QResizeEvent* event) 
    {
        QWidget::resizeEvent(event);
        
        QSize&& s = size();
        QFont f = font();
        f.setPixelSize(s.width()/10);
        setFont(f);
    }
};

#endif // SCALINGLABEL_H
