#include "scenariocreationview.h"
#include "ui_scenariocreationview.h"

#include <QMessageBox>
#include <QFileDialog>

#include <QDataStream>
#include <QJsonDocument>

#include <QGraphicsPixmapItem>

#include "Misc/globalsettings.h"
#include "Misc/videothumbnailgenerator.h"
#include <opencv2/imgproc.hpp>

inline bool operator==(const QPair<QPixmap, QString>& left, const QPair<QPixmap, QString>& right)
{ 
    return left.second == right.second; 
}

static QVector<const char*> g_videoFileExtensions = {
    ".mp4",
    ".webm",
    ".avi",
    ".flv",
    ".wmv",
    ".mov",
};

ScenarioCreationView::ScenarioCreationView(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::ScenarioCreationView())
{
    _ui->setupUi(this);
    
    _ui->videoFileList->setIconSize({50, 40});
    _ui->videoFileList->setDragDropMode(QAbstractItemView::DragDropMode::DragOnly);
    
    QTimer::singleShot(50, this, &ScenarioCreationView::autoRefreshFilesList);
}
ScenarioCreationView::~ScenarioCreationView()
{
    delete _ui;
}


void ScenarioCreationView::autoDetectFiles()
{
    if(!isVisible()) {
        return;
    }
    
    QDir dir(_path);
    if(!dir.exists()) {
        return;
    }
    
    QVector<QPair<QPixmap, QString>> freshVideos;
    QStringList files = dir.entryList(QDir::Filter::Files);
    for(auto it = files.begin(); it != files.end(); ) {
        // Check if extension is recognized
        bool recognizedExtension = false;
        for(const char* ext : g_videoFileExtensions) {
            if(it->endsWith(ext)) {
                recognizedExtension = true;
                break;
            }
        }
        // If not, discard
        if(!recognizedExtension) {
            it = files.erase(it);
            continue;
        }
        
        // Else, add to available scenes
        QImage thumbnail = getThumbnail(_path + "/" + *it);
        freshVideos.push_back({ QPixmap::fromImage(thumbnail.scaled(100, 80)), *it });
        it++;
    }
    
    if(freshVideos != _availableVideos) {
        _availableVideos = freshVideos;
        refreshVideoFileList();
    }
}

bool ScenarioCreationView::scenarioExists()
{
    QFile file(_path + SCENARIO_FILENAME);
    
    file.open(QFile::ReadOnly);
    bool exists = file.isOpen();
    file.close();
    return exists;
}
void ScenarioCreationView::initScenario()
{
    _ui->canvas->scene()->clear();
    
    QFile file(_path + SCENARIO_FILENAME);
    file.open(QFile::WriteOnly | QFile::Truncate);
    if(!file.isOpen()) {
        QMessageBox::warning(this, "Error", "Failed to create scenario file");
        return;
    }
    
    autoDetectFiles();
    
    // Save metadata
    QJsonDocument jdoc;
    jdoc.setObject(Scenario().toJson());
    file.write(jdoc.toJson());
    file.close();
}
void ScenarioCreationView::loadScenario()
{
    _ui->canvas->scene()->clear();
    
    QFile file(_path + SCENARIO_FILENAME);
    file.open(QFile::ReadOnly);
    if(!file.isOpen()) {
        QMessageBox::warning(this, "Error", "Failed to open scenario file");
        return;
    }
    
    autoDetectFiles();
    // @ need to load paths from the scenario file
    
    QJsonDocument jdoc = QJsonDocument::fromJson(file.readAll());
    Scenario* scenario = Scenario::fromJson(jdoc.object());
    if(scenario) {
        scenario->path = _path;
        _ui->canvas->setScenario(scenario);
        delete scenario;
    }
    file.close();
}
void ScenarioCreationView::refreshVideoFileList()
{
    _ui->videoFileList->clear();
    for(auto it = _availableVideos.begin(); it != _availableVideos.end(); it++) {
        _ui->videoFileList->insertItem(0, new QListWidgetItem(QIcon(it->first), it->second));
    }
}


void ScenarioCreationView::autoRefreshFilesList()
{
    autoDetectFiles();
            
    QTimer::singleShot(50, this, &ScenarioCreationView::autoRefreshFilesList);
}

void ScenarioCreationView::on_newButton_clicked()
{
    _path = QFileDialog::getExistingDirectory(this, "Create new scenario in...", GlobalSettings::ASSETS_SCENARIO_PATH);

    if(scenarioExists()) {
        QMessageBox::StandardButton overwrite = QMessageBox::question(this, "Info", 
                                                                      "The specified directory already contains a scenario! Overwrite?",
                                                                      QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
        if(overwrite == QMessageBox::No) {
            _path = "";
            return;
        }
    }
    
    initScenario();
}
void ScenarioCreationView::on_loadButton_clicked()
{
    _path = QFileDialog::getExistingDirectory(this, "Load existing scenario...", GlobalSettings::ASSETS_SCENARIO_PATH);
    
    if(!scenarioExists()) {
        QMessageBox::warning(this, "Error", "Couldn't find a scenario in specified directory", QMessageBox::Ok);
        _path = "";
        return;
    }
    
    loadScenario();
}
void ScenarioCreationView::on_saveButton_clicked()
{
    Scenario* scenario = _ui->canvas->getScenario();
    if(!scenario) {
        QString err = _ui->canvas->getError();
        
        if(!err.isEmpty()) {
            QMessageBox::warning(this, "Error", err);
        }
        return;
    }
    
    QFile file(_path + SCENARIO_FILENAME);
    file.open(QFile::WriteOnly | QFile::Truncate);
    if(!file.isOpen()) {
        QMessageBox::warning(this, "Error", "Failed to save scenario: couldn't open file");
        delete scenario;
        return;
    }
    
    QJsonDocument jdoc;
    jdoc.setObject(scenario->toJson());
    file.write(jdoc.toJson());
    file.close();    
    delete scenario;
}
