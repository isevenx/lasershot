#ifndef TARGETITEM_H
#define TARGETITEM_H

#include <QPointer>
#include <QGraphicsPixmapItem>
#include <QThread>
#include "GameItem/targetmodel.h"
#include "resizeitem.h"
#include "dragitem.h"

class TargetItem : public QObject,
        public QGraphicsPixmapItem
{
    Q_OBJECT
signals:
    void dropped(DragItem*, QPointF pos);
public:
    virtual ~TargetItem() {}
    TargetItem(TargetModel*, QObject* parent = nullptr);
    void setMovable(bool enabled);
    void setEditable(bool enabled);
    void setResizeable(bool enabled);
    void setBorder(bool enabled);

    TargetModel* getModel() const { return _model; }
    operator TargetModel*() const { return _model; }
    bool isEmpty();
private slots:
    void redraw();
    void redrawModel();
    void removeModel();
    void deleteModel();
public:
    void mousePressEvent(QGraphicsSceneMouseEvent* event);
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant& value);
    void dropEvent(QGraphicsSceneDragDropEvent* event);
private:
    QPointer<TargetModel> _model;
    bool _isMoveable;
    bool _isEditable;
    ResizeItem* _resizeItem;
    bool _drawBorder;
    QThread* _drawThread;

    Qt::ConnectionType connType();
};

#endif // TARGETITEM_H
