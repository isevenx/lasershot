#ifndef TARGETOUTPUTDEVICE_H
#define TARGETOUTPUTDEVICE_H

#include "GameItem/targetgroup.h"
#include "frameoutputdevice.h"

class GameOutputDevice : public FrameOutputDevice
{
public:
    virtual void set(QImage) = 0;
    virtual void set(TargetGroup*) = 0;
    virtual QImage getScreenShot() = 0;
};

#endif // TARGETOUTPUTDEVICE_H
