#ifndef SCENARIOOPERATORVIEW_H
#define SCENARIOOPERATORVIEW_H

#include <QWidget>
#include "Database/imagemodel.h"
#include "GameItem/scenario.h"

#include <atomic>

namespace Ui {
class ScenarioOperatorView;
}

class ScenarioOperatorView : public QWidget
{
    Q_OBJECT

private:
    Ui::ScenarioOperatorView* _ui;
    Scenario* _scenario;
    Scene* _currentScene;
    
public:
    explicit ScenarioOperatorView(QWidget *parent = 0);
    ~ScenarioOperatorView();
    
    void set(ImageModel* model);
    bool setScenario(QString path);
    void start();
    
private:
    void refreshBranchList();
    
signals:
    void videoSwitchRequested(QString link, bool start);

public slots:
    void nextVideo(VKey k = 0);
    
private slots:
    void switchVideo(QString link);
    
    void keyPressEvent(QKeyEvent *event);
    void on_branchList_doubleClicked(const QModelIndex &index);
};

#endif // SCENARIOOPERATORVIEW_H
