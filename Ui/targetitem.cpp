#include "targetitem.h"
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsScene>
#include "resizeitem.h"
#include <QPainter>
#include <QListWidget>
#include "dragitem.h"

TargetItem::TargetItem(TargetModel* model, QObject* parent):
    QObject(parent),
    _model(model),
    _isMoveable(false),
    _isEditable(false),
    _resizeItem(nullptr),
    _drawBorder(false),
    _drawThread(QThread::currentThread())
{
    connect(_model, &TargetModel::dataChanged, this, &TargetItem::redrawModel, Qt::DirectConnection);
    connect(_model, &TargetModel::deleted, this, &TargetItem::removeModel, Qt::DirectConnection);
    setShapeMode(BoundingRectShape);
    setAcceptDrops(true);
    redraw();
}

void TargetItem::setMovable(bool enabled)
{
    _isMoveable = enabled;
    setFlag(ItemIsMovable, enabled);
}

void TargetItem::setEditable(bool enabled)
{
    _isEditable = enabled;
}

void TargetItem::setResizeable(bool enabled)
{
    if(enabled){
        if(!_resizeItem) _resizeItem = new ResizeItem(*this);
    }
    else if(_resizeItem){
        delete _resizeItem; // scene should delete item from self
        _resizeItem = nullptr;
    }
}

void TargetItem::setBorder(bool enabled)
{
    if(_drawBorder != enabled){
        _drawBorder = enabled;
        redraw();
    }
}

bool TargetItem::isEmpty()
{
    return (!_model || _model.isNull());
}

void TargetItem::redrawModel()
{
    QMetaObject::invokeMethod(this, "redraw", connType());
}

void TargetItem::redraw()
{
    setFlag(QGraphicsItem::ItemSendsGeometryChanges, false);
    if(_model.data()){
        if(parentItem()){
            auto size = _model->getSize();
            auto rect = parentItem()->boundingRect();
            if(size.width() > rect.width() || size.height() > rect.height()){
                if(size.width() > rect.width()) size.setWidth(rect.width());
                if(size.height() > rect.height()) size.setHeight(rect.height());
                _model->setSize(size);
            }
        }

        auto pix = QPixmap::fromImage(_model->getImage());
        if(_drawBorder){
            QPainter painter(&pix);
            painter.setPen(QPen(QColor(240, 52, 52), 10));
            painter.drawRect(QRect(QPoint(0,0), pix.size()));
        }

        if(_resizeItem) _resizeItem->reset();

        setPixmap(pix);
        setPos(_model->getPosition());
    }
    setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
}

Qt::ConnectionType TargetItem::connType()
{
    return (QThread::currentThread() == thread())? Qt::DirectConnection : Qt::BlockingQueuedConnection;
}

void TargetItem::removeModel()
{
    QMetaObject::invokeMethod(this, "deleteModel", connType());
}

void TargetItem::deleteModel()
{
    _model = nullptr;
}

QVariant TargetItem::itemChange(GraphicsItemChange change, const QVariant& value)
{
    if(_model){
        if(change == GraphicsItemChange::ItemPositionChange){
            QPointF newPos = value.toPointF();
            QRectF rect;
            float w = boundingRect().width();
            float h = boundingRect().height();

            if(parentItem()) rect = parentItem()->boundingRect();
            else rect = scene()->sceneRect();
            if (!rect.contains(newPos) || !rect.contains(newPos.x()+w, newPos.y()+h)) {
                // Keep the item inside the scene rect.
                newPos.setX(qMin(rect.right()-w, qMax(newPos.x(), rect.left())));
                newPos.setY(qMin(rect.bottom()-h, qMax(newPos.y(), rect.top())));
            }

            disconnect(_model, &TargetModel::dataChanged, this, &TargetItem::redraw);
            _model->setPosition(newPos);
            connect(_model, &TargetModel::dataChanged, this, &TargetItem::redraw, Qt::QueuedConnection);
            return newPos;
        }
    }
    return QGraphicsPixmapItem::itemChange(change, value);
}

void TargetItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    if(_isEditable){
        QPoint point(event->pos().x(), event->pos().y());
        _model->addLastRegion(point);
        _model->drawRegions();
    }
    QGraphicsPixmapItem::mousePressEvent(event);
}

void TargetItem::dropEvent(QGraphicsSceneDragDropEvent* event)
{
    auto list = dynamic_cast<QListWidget*>(event->source());
    if(list){
        auto item = static_cast<DragItem*>(list->currentItem());
        if(item) emit dropped(item, event->pos());
    }
    QGraphicsItem::dropEvent(event);
}
