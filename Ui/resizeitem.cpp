#include "resizeitem.h"
#include <QGraphicsSceneMouseEvent>
#include "targetitem.h"
#include <QPen>

ResizeItem::ResizeItem(QGraphicsItem& parent):
    _scaleX(1), _scaleY(1)
{
    setParentItem(&parent);
    setBrush(QBrush(QColor(240, 52, 52)));
    setRect(QRect(0,0,10,10));

    reset();
}

void ResizeItem::reset()
{
    setFlag(ItemIsMovable, false);
    setFlag(ItemSendsGeometryChanges, false);

    if(parentItem()){
        auto x = parentItem()->boundingRect().width() - rect().width();
        auto y = parentItem()->boundingRect().height() - rect().height();
        setPos(x, y);
    }

    setFlag(ItemIsMovable, true);
    setFlag(ItemSendsGeometryChanges, true);
}

QVariant ResizeItem::itemChange(GraphicsItemChange change, const QVariant& value)
{
    if(change == GraphicsItemChange::ItemPositionChange){
        QPointF newPos = value.toPointF();
        auto parent = parentItem();

        // Limit resize region
        if(parent && parent->parentItem()){
            auto pos = newPos;
            auto maxWidth = parent->parentItem()->boundingRect().width() - rect().width() - parent->x();
            auto maxHeight = parent->parentItem()->boundingRect().height() - rect().height() - parent->y();
            if(pos.x() > maxWidth) pos.setX(maxWidth);
            if(pos.y() > maxHeight) pos.setY(maxHeight);
            if(pos.x() < 0) pos.setX(0);
            if(pos.y() < 0) pos.setY(0);
            if(pos != newPos){
                setPos(pos);
                return pos;
            }
        }

        if(parent){
            auto delta = newPos - pos();
            auto targetItem = static_cast<TargetItem*>(parent);
            if(targetItem){
                auto model = (TargetModel*)*targetItem;
                if(model){
                    auto size = model->getOriginalSize();
                    _scaleX += delta.x()/float(size.width());
                    _scaleY += delta.y()/float(size.height());
                    model->setScale(_scaleX, _scaleY);
                }
            }
        }
    }
    return QGraphicsRectItem::itemChange(change, value);
}

void ResizeItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    if(event->button() == Qt::RightButton){

        auto parent = parentItem();
        if(parent){
            auto targetItem = static_cast<TargetItem*>(parent);
            if(targetItem){
                auto model = (TargetModel*)*targetItem;
                auto origSize = model->getOriginalSize();
                auto curSize = model->getSize();
                auto origRatio = origSize.width()/float(origSize.height());

                if(curSize.width() > curSize.height())
                    setX(x() - (curSize.width() - curSize.height()*origRatio));
                else
                    setY(y() - (curSize.height() - curSize.width()/origRatio));
            }
        }
    }
    QGraphicsRectItem::mousePressEvent(event);
}
