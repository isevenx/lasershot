#ifndef SCENARIOCREATIONCANVAS_H
#define SCENARIOCREATIONCANVAS_H

#include <QGraphicsView>
#include <QPointer>

class Scenario;
class ScenarioSceneConnector;
class ScenarioScenePixmap;
class ScenarioCreationCanvas : public QGraphicsView
{
    typedef QPointer<ScenarioScenePixmap> ScenePtr;
    
    QPoint _pos;
    QPoint _size;
    
    QPoint _panStart;
    bool _panning;
    
    ScenarioSceneConnector* _connector;
    ScenePtr _firstScene;
    
    int c_lastErrCode;
    
public:
    ScenarioCreationCanvas(QWidget* parent);
    
    Scenario* getScenario();
    void setScenario(Scenario* scenario);
    
    // There are better ways to do this but this is simple
    QString getError();
    
    void dropEvent(QDropEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);    
    void mouseMoveEvent(QMouseEvent *event);
    
private slots:
    void showContextMenu(QPoint pos);

    void execAction();
};

#endif // SCENARIOCREATIONCANVAS_H
