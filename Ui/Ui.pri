HEADERS += \
    $$PWD/mainwindow.h \
    $$PWD/gamewindow.h \
    $$PWD/mygraphicsview.h \
    $$PWD/frameoutputdevice.h \
    $$PWD/screenshotview.h \
    $$PWD/calibrationview.h \
    $$PWD/targetitem.h \
    $$PWD/targetcreationview.h \
    $$PWD/gameoutputdevice.h \
    $$PWD/dragitem.h \
    $$PWD/resizeitem.h \
    $$PWD/levelcreationview.h \
    $$PWD/scenariocreationview.h \
    $$PWD/scenariocreationcanvas.h \
    $$PWD/extendedgraphicsscene.h \
    $$PWD/movablepixmapitem.h \
    $$PWD/scenariosceneconnector.h \
    $$PWD/scenarioscenepixmap.h \
    $$PWD/scenariooperatorview.h \
    $$PWD/examoperatorview.h \
    $$PWD/squarebutton.h \
    $$PWD/scalinglabel.h \
    $$PWD/scenariocreationobjecttypes.h \
    $$PWD/extendedlistwidget.h

SOURCES += \
    $$PWD/mainwindow.cpp \
    $$PWD/gamewindow.cpp \
    $$PWD/screenshotview.cpp \
    $$PWD/calibrationview.cpp \
    $$PWD/targetitem.cpp \
    $$PWD/targetcreationview.cpp \
    $$PWD/resizeitem.cpp \
    $$PWD/levelcreationview.cpp \
    $$PWD/scenariocreationview.cpp \
    $$PWD/scenariocreationcanvas.cpp \
    $$PWD/extendedgraphicsscene.cpp \
    $$PWD/movablepixmapitem.cpp \
    $$PWD/scenariosceneconnector.cpp \
    $$PWD/scenarioscenepixmap.cpp \
    $$PWD/scenariooperatorview.cpp \
    $$PWD/examoperatorview.cpp

FORMS += \
    $$PWD/mainwindow.ui \
    $$PWD/gamewindow.ui \
    $$PWD/screenshotview.ui \
    $$PWD/calibrationview.ui \
    $$PWD/targetcreationview.ui \
    $$PWD/targetpartdialog.ui \
    $$PWD/levelcreationview.ui \
    $$PWD/scenariocreationview.ui \
    $$PWD/scenariooperatorview.ui \
    $$PWD/examoperatorview.ui

DISTFILES += \
    $$PWD/stylesheet.qss

RESOURCES += \
    $$PWD/stylesheet.qss


