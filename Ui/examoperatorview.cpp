#include "examoperatorview.h"
#include "ui_examoperatorview.h"

ExamOperatorView::ExamOperatorView(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::ExamOperatorView)
{
    _ui->setupUi(this);
}
ExamOperatorView::~ExamOperatorView()
{
    delete _ui;
}

void ExamOperatorView::set(ImageModel* model)
{
    _ui->shotView->set(model);
}
