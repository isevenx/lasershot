#include "scenariooperatorview.h"
#include "ui_scenariooperatorview.h"

#include <QTimer>
#include <QFile>
#include <QJsonDocument>
#include <QMessageBox>
#include <QDebug>

#include "Misc/videothumbnailgenerator.h"

ScenarioOperatorView::ScenarioOperatorView(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::ScenarioOperatorView),
    _scenario(nullptr),
    _currentScene(nullptr)
{
    _ui->setupUi(this);
    _ui->branchList->setIconSize({ 200, 160 });
    connect(_ui->branchList, &ExtendedListWidget::keyPressed, this, &ScenarioOperatorView::keyPressEvent, Qt::DirectConnection);
    
    _scenario = new Scenario();
}
ScenarioOperatorView::~ScenarioOperatorView()
{
    delete _ui;
    
    if(_scenario) {
        delete _scenario;
    }
}

void ScenarioOperatorView::set(ImageModel* model)
{
    _ui->shotView->set(model);
}
bool ScenarioOperatorView::setScenario(QString path)
{
    QString filepath = path + "/" + SCENARIO_FILENAME;
    
    QFile file(filepath);
    if(!file.open(QFile::ReadOnly)) {
        QMessageBox::warning(this, "Error", "Failed to load scenario: could not open file");
        return false;
    }
    
    QJsonDocument jdoc = QJsonDocument::fromJson(file.readAll());
    Scenario* loaded = Scenario::fromJson(jdoc.object());
        
    if(!loaded) {
        return false;
    }
    
    if(_scenario) {
        delete _scenario;
    }
    
    _scenario = loaded;
    _scenario->path = path;
    for(Scene& s : _scenario->scenes) {
        getThumbnail(path.append(s.path));
    }
    return true;    
}
void ScenarioOperatorView::start()
{
    _currentScene = (*_scenario)[0];
    refreshBranchList();
    switchVideo(_scenario->path + "/" + _currentScene->path);
    
    setFocus();
}


void ScenarioOperatorView::nextVideo(VKey k)
{
    if(k) {
        Scene* next = _currentScene->next(k);
        if(next) {
            _currentScene = next;
            switchVideo(_scenario->path + "/" + _currentScene->path);
            refreshBranchList();
        }
    }
    else {
        Scene* next = _currentScene->nextDefault();
        if(next) {
            _currentScene = next;
            switchVideo(_scenario->path + "/" + _currentScene->path);
            refreshBranchList();
        }   
    }
}
void ScenarioOperatorView::refreshBranchList()
{
    qDebug() << "started";
    _ui->branchList->clear();
    auto&& branches = _currentScene->connections();
    for(auto it = branches.begin(); it != branches.end(); it++) {
        QImage thumbnail = getThumbnail(_scenario->path + "/" + it.value()->path );
        QString str;
        str.append(it.value()->path);
        str.append(" -- ");
        str.append((char)it.key());
        _ui->branchList->addItem(new QListWidgetItem(QIcon(QPixmap::fromImage(thumbnail)), str));//QString("%1 -- %2").arg(it.value()->path, (char)it.key())));
    }
    qDebug() << "ended";
}


void ScenarioOperatorView::switchVideo(QString link)
{
    emit videoSwitchRequested(link, true);
}

#include <QKeyEvent>
void ScenarioOperatorView::keyPressEvent(QKeyEvent* event)
{
    // Somehow Qt calls this from multiple threads 
    // for multiple fast keypresses...

    static std::atomic_flag started;
    if(!started.test_and_set()) {
        nextVideo(event->key());
        started.clear();
    }
}
void ScenarioOperatorView::on_branchList_doubleClicked(const QModelIndex &index)
{
    auto&& branches = _currentScene->connections();
    
    auto it = branches.begin();
    for(int i = 0; i < branches.size() && i < index.row(); i++, it++);
    _currentScene = it.value();
    
    switchVideo(_scenario->path + "/" + _currentScene->path);
    refreshBranchList();
}
