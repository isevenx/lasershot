#include "calibrationview.h"
#include "ui_calibrationview.h"
#include "Mode/Calibration/calibrationmode.h"
using namespace MODE;

CalibrationView::CalibrationView(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::CalibrationView),
    _view(nullptr),
    _scene(new QGraphicsScene(this)),
    _item(new QGraphicsPixmapItem),
    _cameraSettings(nullptr)
{
    _ui->setupUi(this);

    _view = _ui->graphicsView;
    connect(_ui->horizontalSlider, &QSlider::valueChanged, this, &CalibrationView::setShutter, Qt::QueuedConnection);

    _view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setFrameStyle(QFrame::NoFrame);
    _view->setInteractive(false);

    _scene->setBackgroundBrush(Qt::black);
    _scene->setItemIndexMethod(QGraphicsScene::NoIndex);

    _scene->addItem(_item); // Takes ownership!! item will be deleted by scene itself (qt, qt...)
    _view->setScene(_scene);
    _view->show();
}

CalibrationView::~CalibrationView()
{
    delete _ui;
    delete _scene;
}

void CalibrationView::setCameraSettings(CameraSettings* cameraSettings)
{
    _cameraSettings = cameraSettings;
}

void CalibrationView::setShutter(int value)
{
    if(_cameraSettings) _cameraSettings->setShutter(value);
}

void CalibrationView::showImage(QImage image)
{
    _item->setPixmap(QPixmap::fromImage(image));
    _view->fitInView(_item, Qt::KeepAspectRatio);
}

void CalibrationView::resizeEvent(QResizeEvent*)
{
    _view->fitInView(_item, Qt::KeepAspectRatio);
}

void CalibrationView::handleNewMode(Mode mode)
{
    if(mode == CALIBRATION::CAMERA_FOCUS || mode == CALIBRATION::DISPLAY_COORDINATES){
        disconnect(_ui->horizontalSlider, &QSlider::valueChanged, this, &CalibrationView::setShutter);
        _ui->horizontalSlider->setValue(_cameraSettings->getShutter());
        connect(_ui->horizontalSlider, &QSlider::valueChanged, this, &CalibrationView::setShutter, Qt::QueuedConnection);
        _ui->horizontalSlider->setEnabled(true);
    }
    else _ui->horizontalSlider->setEnabled(false);
}
