#ifndef SCENARIOCREATIONVIEW_H
#define SCENARIOCREATIONVIEW_H

#include <QWidget>
#include <QGraphicsScene>
#include <QTimer>
#include <GameItem/scenario.h>


namespace Ui {
class ScenarioCreationView;
}


struct Scenario;
class ScenarioCreationView : public QWidget
{
    Q_OBJECT

private:
    Ui::ScenarioCreationView *_ui;
    
    QVector<QPair<QPixmap, QString>> _availableVideos; // [thumbnail; filepath]
    QString _path;
    
public:
    explicit ScenarioCreationView(QWidget *parent = 0);
    ~ScenarioCreationView();
    
private:
    void autoDetectFiles();
    
    bool scenarioExists();
    void initScenario();
    void loadScenario();
    void refreshVideoFileList();
    
private slots:
    void autoRefreshFilesList();
    
    void on_newButton_clicked();
    void on_loadButton_clicked();
    void on_saveButton_clicked();
};

#endif // SCENARIOCREATIONVIEW_H
