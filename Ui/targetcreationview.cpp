#include "targetcreationview.h"
#include "ui_targetcreationview.h"
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QDesktopWidget>
#include <QDialogButtonBox>
#include <QFormLayout>
#include "targetitem.h"
#include "Misc/globalsettings.h"
using namespace GlobalSettings;

typedef QPair<int,TargetItem*> ItemPointer;

TargetCreationView::TargetCreationView(QWidget *parent) :
    QWidget(parent),
    _ui(new Ui::TargetCreationView),
    _scene(new QGraphicsScene(this)),
    _item(nullptr)
{
    _ui->setupUi(this);
    
    QBrush bgBrush(QImage(ASSETS_IMAGE_PATH + "tiles.png"));
    _scene->setBackgroundBrush(bgBrush);
    _ui->graphicsView->setFrameStyle(QFrame::NoFrame);
    _ui->graphicsView->setScene(_scene);
    _ui->graphicsView->setWheelEnabled(true);
    resetSetRegion();

    connect(_ui->pushButton, &QPushButton::pressed, this, &TargetCreationView::setImage, Qt::QueuedConnection);
    connect(_ui->pushButton_2, &QPushButton::pressed, this, &TargetCreationView::addRegion, Qt::QueuedConnection);
    connect(_ui->pushButton_3, &QPushButton::pressed, this, &TargetCreationView::save, Qt::QueuedConnection);
    connect(_ui->pushButton_4, &QPushButton::pressed, this, &TargetCreationView::import, Qt::QueuedConnection);
    connect(_ui->pushButton_5, &QPushButton::pressed, this, &TargetCreationView::removeLastRegion, Qt::QueuedConnection);
}

TargetCreationView::~TargetCreationView()
{
    delete _ui;
    delete _scene;
}

void TargetCreationView::setImage()
{
    auto result = QFileDialog::getOpenFileName(this, "Choose Target", QDir::homePath(), "*.jpg *.png *.jpeg");
    QImage image(result);
    if(!image.isNull()){
        _model.clear();
        _model.setImage(image);
        _model.setDrawShots(true);
    }
    else QMessageBox::warning(this, "Error", "Failed to load image!");
}

void TargetCreationView::resetSetRegion()
{
    _ui->pushButton_2->setText("Add Region");
}

void TargetCreationView::removeLastRegion()
{
    _model.removeLastRegion();
    _model.drawRegions();
    if(_ui->pushButton_2->text() == "Finish"){
        resetSetRegion();
    }
}

void TargetCreationView::addRegion()
{
    if(_ui->pushButton_2->text() == "Add Region"){
        QDialog d(this);
        QDialogButtonBox buttonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
        connect(&buttonBox, &QDialogButtonBox::accepted, &d, &QDialog::accept);
        connect(&buttonBox, &QDialogButtonBox::rejected, &d, &QDialog::reject);
        QLineEdit score;

        QFormLayout layout;
        layout.addRow("Score:", &score);
        layout.addWidget(&buttonBox);
        d.setLayout(&layout);
        d.setWindowTitle("Region score:");

        QRect parentRect(mapToGlobal(QPoint(0,0)), size());
        d.move(QStyle::alignedRect( Qt::LeftToRight, Qt::AlignCenter, d.size(), parentRect).topLeft());
        auto result = d.exec();
        if(result == QDialog::Accepted){
            _model.addRegion({}, score.text().toDouble());
            _item->setEditable(true);
            _ui->pushButton_2->setText("Finish");
        }
    }
    else{
        _item->setEditable(false);
        resetSetRegion();
    }
}

void TargetCreationView::save()
{
    bool ok;
    auto name = QInputDialog::getText(this, tr("Save Target"), tr("Name:"),
                                          QLineEdit::Normal, QString(), &ok);
    if(ok && !name.isEmpty()){
        QDir tmp; tmp.mkpath(ASSETS_TARGET_PATH);
        _model.toFile(ASSETS_TARGET_PATH + name + ".target");
    }
}

void TargetCreationView::import()
{
    auto result = QFileDialog::getOpenFileName(this, "Choose Target", ASSETS_TARGET_PATH, "*.target");
    if(!result.isEmpty()){
        clear();
        if(_model.fromFile(result)){
            _model.drawRegions();
        }
        else QMessageBox::warning(this, "Error", "Failed to load target!");
    }
}

void TargetCreationView::clear()
{
    _scene->clear();
    _model.clear();
    resetSetRegion();

    _item = new TargetItem(&_model);
    _item->setEditable(false);
    _scene->addItem(_item);
}

void TargetCreationView::showEvent(QShowEvent* event)
{
    clear();
    QWidget::showEvent(event);
}
