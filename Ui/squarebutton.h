#ifndef SQUAREBUTTON_H
#define SQUAREBUTTON_H

#include <QPushButton>

class SquareButton : public QPushButton
{
public:
    using QPushButton::QPushButton;
        
    virtual void resizeEvent(QResizeEvent *event) 
    {
        QSize&& s = size();
        if(s.width() != s.height()) {
            setFixedHeight(s.width());
        }
        
        QFont f = font();
        f.setPixelSize(s.width()/8);
        setFont(f);

        QWidget::resizeEvent(event);
    }
};

#endif // SQUAREBUTTON_H
