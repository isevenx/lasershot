#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QMainWindow>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <QKeyEvent>
#include <QVector>
#include "mygraphicsview.h"
#include "frameoutputdevice.h"
#include "gameoutputdevice.h"
#include "targetitem.h"
#include <QGraphicsTextItem>

namespace Ui { class GameWindow; }

class GameWindow : public QMainWindow,
        public GameOutputDevice
{
    Q_OBJECT
    
public:
    explicit GameWindow(QWidget *parent = 0);
    ~GameWindow();
    void set(QImage frame);
    void set(TargetGroup* targetGroup);
    QImage getScreenShot();
    
signals:
    void mousePressed(QPoint point);
    void keyPressed(int key);
    
private slots:
    void draw(QImage frame);
    void keyPressEvent(QKeyEvent* event);
    void resizeEvent(QResizeEvent*);
    void createItems(TargetGroup* targetGroup);
    void createItem(TargetModel* model);
    void removeGroup();
    void deleteGroup();
    void checkTargetGroup();
    QImage getScreenShotPrivate();

    void hideItems();
    void clearItems();
    void showItems();

private:
    Ui::GameWindow* _ui;
    MyGraphicsView* _view;
    QGraphicsScene* _scene;
    QGraphicsPixmapItem* _bgItem;
    Qt::WindowStates _previousState;

    QVector<TargetItem*> _targetItems;
    TargetGroup* _targetGroup;

    QGraphicsTextItem* _textItem;
    QPixmap _background;

    Qt::ConnectionType connType();
};

#endif // GAMEWINDOW_H
