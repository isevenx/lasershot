#ifndef SCENECREATIONVIEW_H
#define SCENECREATIONVIEW_H

#include <QWidget>
#include <QGraphicsScene>
#include <QListWidgetItem>
#include "targetitem.h"
#include "dragitem.h"
#include "GameItem//targetgroup.h"
#include "GameItem/levelmodel.h"

namespace Ui { class LevelCreationView; }

class LevelCreationView : public QWidget
{
    Q_OBJECT

public:
    explicit LevelCreationView(QWidget *parent = 0);
    ~LevelCreationView();

private slots:
    void showEvent(QShowEvent*);
    void resizeEvent(QResizeEvent*);
    void addItem(DragItem*, QPointF pos);
    void changeDragItems(QString name);
    void saveLevel();
    void prevLayer();
    void nextLayer();
    void addLayer();
    void lastLayer();
    void changeLayer(int id);

    TargetItem* addTargetItem(TargetModel*);
    void openLayer();

private:
    Ui::LevelCreationView* _ui;
    QGraphicsScene* _scene;

    QString _defaultBg;
    TargetModel _bgModel;
    TargetItem* _bgItem;

    LayerModel* _layer;
    QString _curLayerName;
    LevelModel _levelModel;

    QMap<QString, QVector<TargetItem*>> _layerItems;
    QMap<QString, QList<DragItem*>> _dragItems;

    void clear();
};

#endif // SCENECREATIONVIEW_H
