#include "gamewindow.h"
#include "ui_gamewindow.h"
#include <QThread>
#include <QSettings>
#include <QTextBlockFormat>
#include <QTextCursor>

GameWindow::GameWindow(QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::GameWindow),
    _scene(new QGraphicsScene(this)),
    _bgItem(new QGraphicsPixmapItem),
    _targetGroup(nullptr),
    _textItem(nullptr)
{
    _ui->setupUi(this);
    _view = _ui->graphicsView;

    _scene->addItem(_bgItem); // Takes ownership!! item will be deleted by scene itself (qt, qt...)
    _scene->setBackgroundBrush(Qt::black);

    _view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    _view->setFrameStyle(QFrame::NoFrame);
    _view->setWheelEnabled(false);
    _view->setScene(_scene);
    connect(_view, &MyGraphicsView::mousePressed, this, &GameWindow::mousePressed, Qt::DirectConnection);
    _view->show();

    QSettings settings("settings.conf", QSettings::NativeFormat);
    restoreGeometry(settings.value("GameWindow").toByteArray());
}

GameWindow::~GameWindow()
{
    clearItems();

    delete _ui;
    delete _view;    
    delete _scene;

    QSettings settings("settings.conf", QSettings::NativeFormat);
    settings.setValue("GameWindow", saveGeometry());
}

Qt::ConnectionType GameWindow::connType()
{
    return (QThread::currentThread() == thread())? Qt::DirectConnection : Qt::BlockingQueuedConnection;
}

void GameWindow::set(QImage frame)
{
    // GraphicsScene doesnt work properly from another thread
    QMetaObject::invokeMethod(this, "draw", connType(), Q_ARG(QImage,frame));
}

void GameWindow::clearItems()
{
    for(const auto& targetItem : _targetItems){
        _scene->removeItem(targetItem);
        delete targetItem;
    }
    _targetItems.clear();
}

void GameWindow::hideItems()
{
    for(TargetItem* targetItem : _targetItems)
        targetItem->hide();
}

void GameWindow::showItems()
{
    for(TargetItem* targetItem : _targetItems)
        targetItem->show();
}

void GameWindow::set(TargetGroup* targetGroup)
{
    QMetaObject::invokeMethod(this, "createItems", connType(), Q_ARG(TargetGroup*,targetGroup));
}

void GameWindow::createItems(TargetGroup* targetGroup)
{
    clearItems();
    _targetGroup = targetGroup;
    if(targetGroup){
        for(const auto& target : *targetGroup)
            createItem(target);
        connect(targetGroup, &TargetGroup::dataChanged, this, &GameWindow::checkTargetGroup, Qt::QueuedConnection);
        connect(targetGroup, &TargetGroup::deleted, this, &GameWindow::removeGroup, Qt::DirectConnection);
    }
}

void GameWindow::createItem(TargetModel* model)
{
    auto item = new TargetItem(model);
    item->setParentItem(_bgItem);
    _targetItems.append(item);
}

void GameWindow::removeGroup()
{
    QMetaObject::invokeMethod(this, "deleteGroup", connType());
}

void GameWindow::deleteGroup()
{
    clearItems();
    _targetGroup = nullptr;
}

void GameWindow::checkTargetGroup()
{
    if(_targetGroup){
        // Check removed items
        QMutableVectorIterator<TargetItem*> it(_targetItems);
        while(it.hasNext()){
            auto item = it.next();
            if(item->isEmpty()){
                _scene->removeItem(item);
                delete item;
                it.remove();
            }
        }

        // Check added items
        for(const auto& target : *_targetGroup){
            bool found = false;
            for(const auto& item : _targetItems)
                if((found = (*item == target))) break;
            if(!found) createItem(target);
        }
    }
}



QImage GameWindow::getScreenShot()
{
    QImage result;
    QMetaObject::invokeMethod(this, "getScreenShotPrivate", connType(), Q_RETURN_ARG(QImage, result));
    return result;
}

QImage GameWindow::getScreenShotPrivate()
{
    return _view->grab().toImage();
}


void GameWindow::draw(QImage frame)
{
    _background = QPixmap::fromImage(frame);
    _bgItem->setPixmap(_background);
    _view->fitInView(_bgItem);
}

void GameWindow::keyPressEvent(QKeyEvent* event)
{
    if(event->key() == Qt::Key_F11){
        if(!isFullScreen()){
            _previousState = windowState();
            setWindowState(Qt::WindowFullScreen);
        }
        else setWindowState(_previousState);
    }
    else {
        emit keyPressed(event->key());
    }
}

void GameWindow::resizeEvent(QResizeEvent* event)
{
    _view->fitInView(_bgItem);
    QWidget::resizeEvent(event);
}
