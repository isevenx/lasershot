#ifndef SCREENSHOTVIEW_H
#define SCREENSHOTVIEW_H

#include <QWidget>
#include <QListWidgetItem>
#include <QVector>
#include <QPair>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include "Database/imagemodel.h"

namespace Ui { class ScreenShotView; }

class ScreenShotView : public QWidget
{
    Q_OBJECT

public:
    explicit ScreenShotView(QWidget *parent = 0);
    ~ScreenShotView();
    void set(ImageModel* model);
    
public slots:
    void addScreenShot(QImage image);
    
private slots:
    void checkModel();
    void showImage(QListWidgetItem* item);

private:
    Ui::ScreenShotView* _ui;
    QGraphicsScene* _scene;
    QGraphicsPixmapItem* _bgItem;
    ImageModel* _model;
    QVector<QPair<QListWidgetItem*,QImage>> _items;
};

#endif // SCREENSHOTVIEW_H
