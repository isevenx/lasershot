#ifndef RESIZEITEM_H
#define RESIZEITEM_H

#include <QGraphicsRectItem>
#include <QBrush>

class ResizeItem : public QGraphicsRectItem
{
public:
    ResizeItem(QGraphicsItem& parent);
    void reset();
protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant& value);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* event);
private:
    float _scaleX;
    float _scaleY;
};

#endif // RESIZEITEM_H
