#ifndef MYGRAPHICSVIEW_H
#define MYGRAPHICSVIEW_H

#include <QGraphicsView>
#include <QTimeLine>
#include <QWheelEvent>
#include <QPointF>

class MyGraphicsView : public QGraphicsView
{
    Q_OBJECT
signals:
    void mousePressed(QPoint pos);
    
public:
    using QGraphicsView::QGraphicsView;

    void myFitInView(const QRectF& rect)
    {
        QRectF viewRect = frameRect();

        double ratio = std::min(viewRect.width() / rect.width(), viewRect.height() / rect.height());
//        double scaleY = viewRect.height() / rect.height();

        QTransform trans;
        trans.scale(ratio, ratio);
        setTransform(trans, false);

        centerOn(QPoint(rect.width() / 2, rect.height() / 2));
    }
    
    void setWheelEnabled(bool enabled)
    {
        _wheelEnabled = enabled;
    }
    
private slots:
    void wheelEvent(QWheelEvent* event)
    {
        if(_wheelEnabled){
             int numDegrees = event->delta() / 8;
             int numSteps = numDegrees / 15; // see QWheelEvent documentation
             _numScheduledScalings += numSteps;
             if (_numScheduledScalings * numSteps < 0) // if user moved the wheel in another direction, we reset previously scheduled scalings
             _numScheduledScalings = numSteps;

             QTimeLine *anim = new QTimeLine(350, this);
             anim->setUpdateInterval(20);

             connect(anim, &QTimeLine::valueChanged, this, &MyGraphicsView::scalingTime);
             connect(anim, &QTimeLine::finished, this, &MyGraphicsView::animFinished);
             anim->start();
        }
    }
    
    void scalingTime(qreal)
    {
         qreal factor = 1.0+ qreal(_numScheduledScalings) / 300.0;
         scale(factor, factor);
    }

    void animFinished()
    {
         if (_numScheduledScalings > 0)
            _numScheduledScalings--;
         else
            _numScheduledScalings++;
         sender()->~QObject();
    }

    void mousePressEvent(QMouseEvent* e)
    {
        emit mousePressed(mapToScene(e->pos()).toPoint());
        QGraphicsView::mousePressEvent(e);
    }

private:
    int _numScheduledScalings = 0;
    bool _wheelEnabled = false;
};

#endif // MYGRAPHICSVIEW_H
