#ifndef SCENARIOSCENECONNECTOR_H
#define SCENARIOSCENECONNECTOR_H

#include <QObject>
#include <QGraphicsLineItem>
#include <QGraphicsEllipseItem>
#include <QLineEdit>
#include <QPointer>

#include "scenariocreationobjecttypes.h"

class ScenarioScenePixmap;
class ScenarioSceneConnector : public QObject, public QGraphicsLineItem, public QGraphicsEllipseItem
{
    Q_OBJECT
    
private:
    friend class ScenarioScenePixmap;
    static const int _thisType = ScenarioObjectType::CONNECTOR;
    
    // Will need this to keep connnectors linked to pixmaps
    ScenarioScenePixmap* _endPts[2];
    QGraphicsTextItem* _keyLabel;
    QPointer<QLineEdit> _keyInput;
    
    bool _recursive;
    bool _twoDirectional;
    bool _default;
    
public:
    ScenarioSceneConnector(QGraphicsItem* parent = nullptr);
    virtual ~ScenarioSceneConnector();
    
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);

    void updateEnds();
    void snapToPixmaps();
    bool lockToPixmaps();
    void makeDefault();
    void makeNonDefault();
    
    inline bool isRecursive() { return _recursive; }
    inline bool isTwoDirectional() { return _twoDirectional; }
    inline bool isDefault() { return _default; }
    inline int type() const { return _thisType; }
    
private:
    // Used for creating the QLineEdit for keybind entry, and for updateLabelPos
    QPointF calcLabelPos();
    void updateLabelPos();
    
public slots:
    void setLabel(QString const& label);
};

#endif // SCENARIOSCENECONNECTOR_H
