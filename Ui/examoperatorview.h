#ifndef EXAMOPERATORVIEW_H
#define EXAMOPERATORVIEW_H

#include <QWidget>

namespace Ui {
class ExamOperatorView;
}

class ImageModel;
class ExamOperatorView : public QWidget
{
    Q_OBJECT
    
public:
    explicit ExamOperatorView(QWidget *parent = 0);
    ~ExamOperatorView();
    
    void set(ImageModel* model);

private:
    Ui::ExamOperatorView *_ui;
};

#endif // EXAMOPERATORVIEW_H
