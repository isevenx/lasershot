#ifndef DROPITEM_H
#define DROPITEM_H

#include <QListWidgetItem>

class DragItem : public QListWidgetItem
{
public:
    explicit DragItem(const QIcon& icon, const QString& text = ""):
        QListWidgetItem(icon, text, nullptr, QListWidgetItem::UserType)
    {
        _name = text;
        setTextAlignment(Qt::AlignHCenter | Qt::AlignBottom);
    }

    void setName(QString name)
    {
        _name = name;
    }

    void setDataPtr(void* data)
    {
        _data = data;
    }

    QString getName()
    {
        return _name;
    }

    DragItem* clone()
    {
        auto result = new DragItem(icon(), text());
        result->setName(_name);
        result->setDataPtr(_data);
        return result;
    }

private:
    QString _name;
    void* _data;
};

#endif // DROPITEM_H
