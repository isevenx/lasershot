#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Mode/mode.h"
#include "Camera/camerasettings.h"
#include "Database/imagemodel.h"
#include "Mode/drawmode.h"

namespace Ui { class MainWindow; }

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void set(CameraSettings*);
    void set(ImageModel* model);

signals:
    void aboutToClose();
    void freeLinkRequested(QString url);
    void scenarioLinkRequested(QString url, bool start);
    void modeChangeRequested(Mode mode);
    void hitDrawModeRequested(HitDrawMode hitDrawMode);
    void levelSelected(QString fileName);
    void examSelected(qint64 time, qint64 ammo, QString targetPath, float targetScale);
    void examShowRequested();
    void examHideRequested();
    void resetRequested();
    void videoPauseRequested();
    void videoPlayRequested();
    
public slots:
    void handleNewMode(Mode mode);
    void showImage(QImage image);
    void notifyCameraError();
    void notifyCameraRestart();
    void reset();
    void handleVideoEnded();
    
    void receiveKeyPress(int key);

private slots:
    void closeEvent(QCloseEvent*);
    
    void playFreeShooting();
    void playLevel();
    void playScenario();
    void playExam();
    
    void startLiveFocus();
    void startCalibration();
    void startMouseTest();
    void startDelayTest();
    void returnToMain();
    void startTargetCreation();
    void startLevelCreation();
    void startScenarioCreation();
    
//    void setLink(QString link, bool start);
    void chooseBackground();
    void changeVideoState();
//    void resetLink();
    
    void sendHitDrawMode(int index);
    void chooseLevel();
    void chooseScenario();
    void updateExamParams();

    void refresh(int switchedTabIndex = 0);
    void refreshTargetList();
    void refreshLevelList();
    void refreshScenarioList();
    
    void updateTargetPreview();
    void incrementScale();
    void decrementScale();
    void toggleTargetPreview();
    
    void saveUI();
    void loadUI();
    
private:    
    void keyPressEvent(QKeyEvent *event);
    
    bool _UISettingsLoaded;
    
    Mode _mode;
    
    Ui::MainWindow* _ui;
    QString _targetPath;
    
    float _targetScale;
    bool _showTargetPreview;
};

#endif // MAINWINDOW_H
