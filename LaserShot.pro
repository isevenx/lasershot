QT += core gui widgets multimedia

TARGET = LaserShot
TEMPLATE = app
CONFIG += c++14

QMAKE_CXXFLAGS_RELEASE -= -O2
QMAKE_CXXFLAGS_RELEASE += -O3

HEADERS += \
    modeswitcher.h \
    gameengine.h

SOURCES += \
    main.cpp \
    modeswitcher.cpp \
    gameengine.cpp

include(Camera/Camera.pri)
include(Mode/Mode.pri)
include(Misc/Misc.pri)
include(Ui/Ui.pri)
include(GameItem/GameItem.pri)
include(Database/Database.pri)

LIBS += -lopencv_core -lopencv_imgproc -lopencv_aruco -lflycapture -lopencv_highgui
