#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <QObject>
#include <QMediaPlayer>
#include <QVector>
#include "Mode/mode.h"
#include "Mode/drawmode.h"
#include "Misc/videoframereceiver.h"
#include "Misc/youtubelinkhandler.h"
#include "Misc/filedownloader.h"
#include "Ui/frameoutputdevice.h"
#include "Ui/gameoutputdevice.h"
#include "Database/shotmodel.h"
#include "GameItem/targetmodel.h"
#include "GameItem/targetgroup.h"
#include "GameItem/levelmodel.h"

enum Background { VIDEO, IMAGE };

class GameEngine : public QAbstractVideoSurface
{
    Q_OBJECT
public:
    explicit GameEngine(QMediaPlayer* player, GameOutputDevice* gameDevice);
    ~GameEngine();
    
    void set(ShotModel* model);
    QList<QVideoFrame::PixelFormat> supportedPixelFormats(
            QAbstractVideoBuffer::HandleType handleType = QAbstractVideoBuffer::NoHandle) const;

    // This function works in QMediaPlayer(GUI) thread!
    bool present(const QVideoFrame& frame);

signals:
    void modeChangeRequested(Mode mode);
    void terminated(QString objectName);
    void videoEnded();
    
public slots:
    void stop();
    void terminate();
    
    void reset();
    void handleNewMode(Mode mode);
    
    void processLaserPoints(QVector<QPoint> points, qint64 timestamp);
    void changeFreeLink(QString url);
    void changeScenarioLink(QString url, bool start);
    void changeHitDrawMode(HitDrawMode hitDrawMode);
    void changeLevel(QString fileName);
    void setExam(qint64 time, qint64 ammo, QString targetPath, float targetScale);
    void showExamTarget();
    void hideExamTarget();
    void playVideo();
    void pauseVideo();
    
private slots:
    void showFrame(QImage frame);
//    void setVideoLink(QString url);
//    void setBackgroundImage();

    void playerErrorHandle(QMediaPlayer::Error error);
    
    void handleMediaStatusChanged(QMediaPlayer::MediaStatus status);
    
    void createTimeEvent();
    
private:
    void clearTargets();
    void loadTargets();
    void resetTargets();
    void nextLayer();
    
    GameOutputDevice* _gameDevice;
    ShotModel* _shotModel;
    Mode _mode;
    QMediaPlayer* _player;
    QImage _defaultBackground;
    QImage _latestFrame;
    bool _playingVideo;
    qint64 _latestShotTime;
    QTimer _timer;
    static bool isImage(QString url);

    // Free shooting mode
    YouTubeLinkHandler _ytHandler;
    FileDownloader _fileDownloader;
    HitDrawMode _hitDrawMode;
    QString _freeUrl;

    // Target shooting mode
    LevelModel _levelModel;
    LayerModel* _layer;
    QString _levelPath;
    bool _resetGame;
    
    // Exam mode
    LayerModel* _previewLayer;
    qint64 _examTime;
    qint64 _examAmmo;
    QString _examTargetPath;
    float _examTargetScale;

    // Scenario mode
    QString _scenarioUrl;


};

#endif // GAMEENGINE_H
