#ifndef TARGETGROUP_H
#define TARGETGROUP_H

#include "targetmodel.h"

class TargetGroup : public DataModel
{
    Q_OBJECT
public:
    TargetGroup();
    ~TargetGroup();

    QVector<TargetModel*>::iterator begin();
    QVector<TargetModel*>::iterator end();
    QVector<TargetModel*> getTargets();

    // Takes ownership!!
    void add(TargetModel*);
    void remove(TargetModel*);
    void clear();

    virtual QJsonValue toJson();
    virtual bool fromJson(const QJsonValue&);

protected:
    QVector<TargetModel*> _targets;
};

#endif // TARGETGROUP_H
