#include "levelmodel.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFile>

LevelModel::LevelModel() { }
LevelModel::~LevelModel() { clear(); }

void LevelModel::add(LayerModel* model)
{
    _layers.append(model);
}

QVector<LayerModel*> LevelModel::getLayers()
{
    return _layers;
}

LayerModel* LevelModel::getNextLayer()
{
    if(_layerCounter < _layers.size()){
        return _layers[_layerCounter++];
    }
    else return nullptr;
}

void LevelModel::clear()
{
    for(auto layer : _layers)
        delete layer;
    _layers.clear();

    _filePath.clear();
    _layerCounter = 0;
}

bool LevelModel::fromFile(QString filePath)
{
    clear();

    bool ok = false;
    QFile file(filePath);
    if(file.open(QFile::ReadOnly)){
        QJsonArray layers = QJsonDocument::fromJson(file.readAll()).array();
        for(auto var : layers){
            QJsonObject obj = var.toObject();
            auto layer = new LayerModel;
            if(layer->fromJson(obj)) add(layer);
            else delete layer;
        }
        file.close();
        _filePath = filePath;
        ok = true;
    }
    return ok;
}

bool LevelModel::toFile(QString filePath)
{
    _filePath.clear();
    bool ok = false;
    QFile file(filePath);
    if(file.open(QFile::WriteOnly)){
        QJsonArray layers;
        for(auto layer : _layers)
            layers.append(layer->toJson());
        file.write(QJsonDocument(layers).toJson());
        file.close();
        _filePath = filePath;
        ok = true;
    }
    return ok;
}
