#include "finishaction.h"
#include <QDateTime>
#include <QJsonObject>

FinishAction::FinishAction(ActionEvent eventType, FinishType type, double value):
    _eventType(eventType), _actionId(-1), _finishType(type), _value(value), _started(false), _shotCounter(0)
{
}

FinishAction::FinishAction(ActionModel* model, FinishType type)
{
    if(model){
        connect(model, &ActionModel::finished, this, &FinishAction::finishSelf, Qt::DirectConnection);
        _actionId = model->getId();
        _finishType = type;
        _value = 0;
        _eventType = OTHER_EVENT;
    }
}

FinishAction::FinishAction(const QJsonValue& val)
{
    fromJson(val);
}

ActionEvent FinishAction::getEventType()
{
    return _eventType;
}

FinishType FinishAction::FinishAction::getFinishType()
{
    return _finishType;
}

double FinishAction::getValue()
{
    return _value;
}

bool FinishAction::getActionId(const QJsonValue& val, int* id)
{
    auto obj = val.toObject();
    if(id && obj.contains("actionId")){
        if(obj["actionId"].toInt() > -1){
            *id = obj["actionId"].toInt();
            return true;
        }
    }
    return false;
}

void FinishAction::processEvent(ActionEvent event)
{
    if(_actionId != -1) return;

    if(_started && event == _eventType){
        if(event == SHOT_EVENT){
            if(++_shotCounter >= _value) finishSelf();
        }
        else if(event == TIME_EVENT){
            auto curTime = QDateTime::currentMSecsSinceEpoch();
            if(curTime - _startTime >= _value) finishSelf();
        }
    }
}

void FinishAction::finishSelf()
{    
    if(_finishType == FINISH){
        _started = false;
        emit finished();
    }
    else{
        _restarted = true;
    }
}

bool FinishAction::isRestarted()
{
    return _restarted;
}

void FinishAction::start()
{
    _restarted = false;
    _started = true;
    _startTime = QDateTime::currentMSecsSinceEpoch();
    _shotCounter = 0;
}

bool FinishAction::isFinished() 
{ 
    return !_started; 
}

QJsonValue FinishAction::toJson()
{
    QJsonObject obj;
    obj["id"] = getId();
    obj["actionId"] = _actionId;
    obj["finishType"] = _finishType;
    obj["name"] = "FinishAction";
    obj["eventType"] = _eventType;
    obj["value"] = _value;
    return obj;
}

void FinishAction::fromJson(const QJsonValue& val)
{
    auto obj = val.toObject();
    _actionId = obj["actionId"].toInt();
    _eventType = static_cast<ActionEvent>(obj.value("eventType").toInt());
    _finishType = static_cast<FinishType>(obj.value("finishType").toInt());
    _value = obj["value"].toDouble();
}

bool FinishAction::sameType(const QJsonValue& val)
{
    auto obj = val.toObject();
    return (!obj.isEmpty() && obj["name"] == "FinishAction");
}
