#ifndef ACTIONMODEL_H
#define ACTIONMODEL_H


#include <QObject>

enum ActionEvent { TIME_EVENT, SHOT_EVENT, HIT_EVENT, OTHER_EVENT };

class ActionModel : public QObject
{
    Q_OBJECT
signals:
    void finished();
public:
    virtual ~ActionModel() = 0;
    virtual void start() = 0;
    virtual void processEvent(ActionEvent) = 0;
    virtual QJsonValue toJson() = 0;
    virtual void fromJson(const QJsonValue&) = 0;
    int getId();
    void setId(int id);
private:
    int _id = 0;
};
inline ActionModel::~ActionModel(){}

#endif // ACTIONMODEL_H
