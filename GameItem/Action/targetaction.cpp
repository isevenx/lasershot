#include "targetaction.h"
#include <QDateTime>
#include <QPainter>
#include <QJsonObject>
#include <QJsonArray>

#include "Misc/jsoncast.h"
#include "Misc/globalsettings.h"

using namespace JsonCast;
using namespace GlobalSettings;


TargetAction::TargetAction(TargetModel* model):
    _model(model)
{
    if(model) _targetId = model->getId();
}

bool TargetAction::getTargetId(const QJsonValue& val, int* id)
{
    auto obj = val.toObject();
    if(id && obj.contains("targetId")){
        *id = obj["targetId"].toInt();
        return true;
    }
    return false;
}

TargetModel* TargetAction::getTarget() { return _model; }
int TargetAction::getTargetId(){ return _targetId; }




PosChange::PosChange(TargetModel* model, QVector<QPoint> positions):
    TargetAction(model), _positions(positions), _eventType(HIT_EVENT)
{
    _random = (positions.empty());
    _timeout = -1;
}

PosChange::PosChange(TargetModel* model, qint64 timeout, QVector<QPoint> positions):
    PosChange(model, positions)
{
    _timeout = timeout;
    _eventType = TIME_EVENT;
}

void PosChange::start()
{
    _posCounter = 0;
    _startTime = QDateTime::currentMSecsSinceEpoch();
}

void PosChange::processEvent(ActionEvent event)
{
    if(_eventType == event){
        if(event == HIT_EVENT){
            if(_random) _model->setPosRandom();
            else if(_positions.size() < (int)_posCounter){
                _model->setPosition(_positions[_posCounter]);
                _posCounter++;
            }
        }
    }
}

QJsonValue PosChange::toJson()
{
    QJsonObject obj;
    obj["id"] = getId();
    obj["targetId"] = _targetId;
    obj["name"] = "PosChange";
    obj["type"] = _eventType;
    QJsonArray positions;
    for(auto pos : _positions)
        positions.append(ToJson(pos));
    obj["positions"] = positions;
    obj["timeout"] = _timeout;
    return obj;
}

void PosChange::fromJson(const QJsonValue& val)
{
    auto obj = val.toObject();
    setId(obj["id"].toInt());
    _targetId = obj["targetId"].toInt();
    _eventType = static_cast<ActionEvent>(obj.value("type").toInt());
    for(auto pos : obj["positions"].toArray())
        _positions.append(ToPoint(pos).toPoint());
    _timeout = obj["timeout"].toInt();
}

bool PosChange::sameType(const QJsonValue& val)
{
    auto obj = val.toObject();
    return (!obj.isEmpty() && obj["name"] == "PosChange");
}




#include <ctime>
qint64 randomNum(qint64 from, qint64 to)
{
    static bool init = false;
    if(!init){
        std::srand(std::time(0));
        init = true;
    }
    return from + (std::rand() % (to-from+1));
}


TargetSwitch::TargetSwitch(QVector<TargetModel*> targets, QPair<qint64, qint64> hideTimeout, QPair<qint64, qint64> showTimeout):
    TargetAction(nullptr), _switchTargets(targets), _hideTimeout(hideTimeout), _showTimeout(showTimeout),
    _currentIndex(0), _startTime(0)
{

}

void TargetSwitch::start()
{
    _startTime = QDateTime::currentMSecsSinceEpoch();
    _hide = true;
    for(auto target : _switchTargets) target->hide();
}

void TargetSwitch::processEvent(ActionEvent event)
{
    if(event == TIME_EVENT){
        if(_hide){
            _switchTargets[_currentIndex]->hide();
            _currentIndex = randomNum(0, _switchTargets.size());
        }
    }
}

QJsonValue TargetSwitch::toJson()
{
    return QJsonValue();
}

void TargetSwitch::fromJson(const QJsonValue &)
{

}






void TargetButton::start() { _startTime = QDateTime::currentMSecsSinceEpoch(); }
void TargetButton::processEvent(ActionEvent event)
{
    // Trying to fix accidential hits
    if(event == HIT_EVENT && QDateTime::currentMSecsSinceEpoch() - _startTime > 300)
        emit finished();
}

QJsonValue TargetButton::toJson()
{
    QJsonObject obj;
    obj["name"] = "TargetButton";
    obj["id"] = getId();
    obj["targetId"] = _targetId;
    return obj;
}

void TargetButton::fromJson(const QJsonValue& val)
{
    auto obj = val.toObject();
    _targetId = obj["targetId"].toInt();
    setId(obj["id"].toInt());
}

bool TargetButton::sameType(const QJsonValue& val)
{
    auto obj = val.toObject();
    return (!obj.isEmpty() && obj["name"] == "TargetButton");
}






TextAction::TextAction(TargetModel* model, QString textFormat):
    TargetAction(model), _textFormat(textFormat), _elapsedTime(0), _font("Ubuntu Mono"), _fontSize(45)
{
    _font.setWeight(200); // bold text
    _font.setPixelSize(_fontSize);
    model->setIgnoreShot(true);
//    connect(_model, &TargetModel::dataChanged, this, &TextAction::forceUpdateText, Qt::QueuedConnection);
}

QString TextAction::timeStr(qint64 time)
{
    return QString::number(time/1000.,'f',2);
}

void TextAction::forceUpdateText()
{
    updateText(true);
}

void TextAction::updateText(bool force)
{
    auto text = _textFormat;
    text.replace("%score%", QString::number(_totalScore.score));
    text.replace("%hits%", QString::number(_totalScore.hitCount));
    text.replace("%shots%", QString::number(_totalScore.shotCount));
    // Updated only via set function
    text.replace("%time%", timeStr(_totalScore.time));
    // Updated via time event
    text.replace("%t%", timeStr(_elapsedTime));
    text.replace("%rt%", timeStr(_totalScore.time - _elapsedTime));
    text.replace("%ammo%", QString::number(_totalScore.ammo - _totalScore.shotCount));

    if(force || text != _cachedText){
        QImage result(_model->getSize(), QImage::Format_ARGB32);
        result.fill(Qt::transparent);
        {
            QPainter painter(&result);

            painter.setBrush(Qt::white);
            QPainterPath path;
            QPen pen;
            pen.setWidth(2);
            pen.setColor(Qt::black);
            painter.setPen(pen);
            auto list = text.split('\n');
            auto y = _fontSize;
            for(auto line : list){
                path.addText(0, y, _font, line);
                y += _fontSize;
            }
            painter.setRenderHint(QPainter::Antialiasing, true);
            painter.drawPath(path);
        }
        _model->setImage(result);
        _cachedText = text;
    }
}

void TextAction::start()
{
    _startTime = QDateTime::currentMSecsSinceEpoch();
}

void TextAction::processEvent(ActionEvent event)
{
    if(event == TIME_EVENT)
        _elapsedTime = QDateTime::currentMSecsSinceEpoch() - _startTime;
    else if(event == SHOT_EVENT)
        _totalScore.shotCount++;
    else if(event == HIT_EVENT)
        _totalScore.hitCount++;

    updateText();
}

void TextAction::set(TotalScore score)
{
    _totalScore = score;
    updateText();
}

QJsonValue TextAction::toJson()
{
    QJsonObject obj;
    obj["id"] = getId();
    obj["targetId"] = _targetId;
    obj["name"] = "TextAction";
    obj["format"] = _textFormat;
    return obj;
}

void TextAction::fromJson(const QJsonValue& val)
{
    auto obj = val.toObject();
    setId(obj["id"].toInt());
    _targetId = obj["targetId"].toInt();
    _textFormat = obj["format"].toString();
}

bool TextAction::sameType(const QJsonValue& val)
{
    auto obj = val.toObject();
    return (!obj.isEmpty() && obj["name"] == "TextAction");
}
