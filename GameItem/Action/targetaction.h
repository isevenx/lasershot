#ifndef TARGETACTION_H
#define TARGETACTION_H

#include "actionmodel.h"
#include "GameItem/targetmodel.h"
#include <QMap>
#include <QFont>
#include "Database/totalscore.h"

class TargetAction : public ActionModel
{
public:
    TargetAction(TargetModel* model);
    TargetModel* getTarget();
    int getTargetId();
    static bool getTargetId(const QJsonValue&, int* id);

protected:
    TargetModel* _model;
    int _targetId;
};


class PosChange : public TargetAction
{
public:
    // after event happens, iterator is moved
    // if positions given empty -> every time random
    explicit PosChange(TargetModel*, QVector<QPoint> positions = QVector<QPoint>());
    explicit PosChange(TargetModel *, qint64 timeout, QVector<QPoint> positions = QVector<QPoint>());

    void start();
    void processEvent(ActionEvent);
    QJsonValue toJson();
    void fromJson(const QJsonValue&);
    static bool sameType(const QJsonValue&);

private:
    bool _random;
    size_t _posCounter;
    qint64 _timeout;
    qint64 _startTime;
    QVector<QPoint> _positions;
    ActionEvent _eventType;
};

class TargetSwitch : public TargetAction
{
    explicit TargetSwitch(QVector<TargetModel*>, QPair<qint64,qint64> hideTimeout, QPair<qint64,qint64> showTimeout);

    void start();
    void processEvent(ActionEvent);
    QJsonValue toJson();
    void fromJson(const QJsonValue&);
//    static bool sameType(const QJsonValue&);

private:
    QVector<TargetModel*> _switchTargets;
    QPair<qint64,qint64> _hideTimeout;
    QPair<qint64,qint64> _showTimeout;

    int _currentIndex;
    qint64 _startTime;
    qint64 _nextTimeout;
    bool _hide;
};

class TargetButton : public TargetAction
{
    Q_OBJECT
public:
    using TargetAction::TargetAction;
    void start();
    void processEvent(ActionEvent);
    QJsonValue toJson();
    void fromJson(const QJsonValue&);
    static bool sameType(const QJsonValue&);
private:
    qint64 _startTime;
};

class TextAction : public TargetAction
{
    Q_OBJECT
public:
    TextAction(TargetModel*, QString textFormat);

    void start();
    void processEvent(ActionEvent);
    void set(TotalScore);
    QJsonValue toJson();
    void fromJson(const QJsonValue&);
    static bool sameType(const QJsonValue&);

private slots:
    void updateText(bool force = false);
    void forceUpdateText();

private:
    QString _textFormat;
    QString _cachedText;
    TotalScore _totalScore;
    qint64 _startTime;
    qint64 _elapsedTime;
    
    QFont _font;
    int _fontSize; // pixels

    QString timeStr(qint64 time);
};



#endif // TARGETACTION_H
