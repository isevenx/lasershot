#ifndef GENERALACTION_H
#define GENERALACTION_H

#include "actionmodel.h"

enum FinishType { RESTART, FINISH };

class FinishAction : public ActionModel
{
    Q_OBJECT
public:
    FinishAction(ActionEvent, FinishType, double);
    FinishAction(const QJsonValue&);
    FinishAction(ActionModel*, FinishType);

    void start();
    void processEvent(ActionEvent);
    bool isFinished();
    bool isRestarted();
    ActionEvent getEventType();
    FinishType getFinishType();
    int getActionId();
    double getValue();

    QJsonValue toJson();
    void fromJson(const QJsonValue&);
    static bool sameType(const QJsonValue&);

    static bool getActionId(const QJsonValue&, int* id);

private slots:
    void finishSelf();

private:
    ActionEvent _eventType;
    int _actionId;
    FinishType _finishType;

    double _value;
    bool _started;
    bool _restarted;
    qint64 _startTime;
    qint64 _shotCounter;
};

#endif // GENERALACTION_H
