#ifndef TEXTACTION_H
#define TEXTACTION_H

#include "actionmodel.h"
#include "GameItem/targetmodel.h"
#include "Database/totalscore.h"

class TextAction : public ActionModel
{
public:
    TextAction(TargetModel*, QString format);

    void processEvent(ActionEvent);
    void start();
    ActionEvent getType();
    void update(TotalScore);

private:
    TargetModel* _model;
};

#endif // TEXTACTION_H
