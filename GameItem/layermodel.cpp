#include "layermodel.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QDateTime>
#include <QSoundEffect>
#include "Misc/globalsettings.h"
using namespace GlobalSettings;

LayerModel::LayerModel()
{
    clear();
}

LayerModel::~LayerModel()
{
    clear();
}

void LayerModel::clear()
{
    TargetGroup::clear();

    clearLayerActions();
    clearTargetActions();

    for(auto action : _textActions)
        delete action;
    _textActions.clear();

    _finished = true;
    _restarted = false;
    _startTime = 0;
    _missPenalty = 0;
    _missEnabled = false;
    _totalScore.clear();

    _startSound.clear();
    _endSound.clear();
}

void LayerModel::clearLayerActions()
{
    for(auto action : _layerActions)
        delete action;
    _layerActions.clear();
}

void LayerModel::clearTargetActions()
{
    for(auto action : _targetActions)
        delete action;
    _targetActions.clear();
}

void LayerModel::clearTargetActions(TargetModel* model)
{
    QMutableVectorIterator<TargetAction*> it(_targetActions);
    while(it.hasNext()){
        it.next();
        if(it.value()->getTarget() == model)
            it.remove();
    }
}

void LayerModel::clearLayerActions(FinishType type)
{
    QMutableVectorIterator<FinishAction*> it(_layerActions);
    while(it.hasNext()){
        it.next();
        if(it.value()->getFinishType() == type)
            it.remove();
    }
}

bool LayerModel::isEmpty()
{
    return (_layerActions.isEmpty() && _targetActions.isEmpty() && _textActions.isEmpty());
}

void LayerModel::setBackground(QString url)
{
    _backgroundUrl = url;
}

void LayerModel::add(FinishAction* action)
{
    action->setId(_layerActions.size());
    _layerActions.append(action);
}

void LayerModel::add(TextAction* action)
{
    action->setId(_textActions.size());
    action->set(_totalScore);
    _textActions.append(action);
}

void LayerModel::add(TargetAction* action)
{
    action->setId(_targetActions.size());
    _targetActions.append(action);
}

QString LayerModel::getBackground()
{
    return _backgroundUrl;
}

void LayerModel::set(TotalScore score)
{
    _totalScore = score;
    for(TextAction* action : _textActions)
        action->set(score);
}

TotalScore LayerModel::getScore()
{
    return _totalScore;
}

void LayerModel::setSound(QString onStart, QString onEnd)
{
    _startSound = onStart;
    _endSound = onEnd;
}

void LayerModel::setMissPenalty(double missPenalty)
{
    _missEnabled = true;
    _missPenalty = missPenalty;
}

void LayerModel::start()
{
    _totalScore.clear();
    _finished = false;
    _restarted = false;

    for(auto action : _layerActions)
        action->start();
    for(auto action : _targetActions)
        action->start();
    for(auto action : _textActions)
        action->start();

    _startTime = QDateTime::currentMSecsSinceEpoch();

    if(!_startSound.isEmpty()){
        QSoundEffect* sound = new QSoundEffect(this);
        sound->setSource(QUrl::fromLocalFile(_startSound));
        sound->play();
        QObject::connect(sound, &QSoundEffect::playingChanged, sound, &QSoundEffect::deleteLater, Qt::QueuedConnection);
    }
}

bool LayerModel::isFinished()
{
    return _finished;
}

bool LayerModel::isRestarted()
{
    return _restarted;
}

void LayerModel::finishSelf()
{
    if(!_endSound.isEmpty()){
        QSoundEffect* sound = new QSoundEffect(this);
        sound->setSource(QUrl::fromLocalFile(_endSound));
        sound->play();
        QObject::connect(sound, &QSoundEffect::playingChanged, sound, &QSoundEffect::deleteLater, Qt::QueuedConnection);
    }
    if(_missEnabled){
        for(auto action : _layerActions){
            if(action->getEventType() == SHOT_EVENT){
                auto value = std::round(action->getValue());
                _totalScore.score -= (value-_totalScore.shotCount)*_missPenalty;
                break;
            }
        }
    }
    _totalScore.time = QDateTime::currentMSecsSinceEpoch() - _startTime;
}

void LayerModel::createTimeEvent()
{
    for(auto action : _layerActions){
        action->processEvent(TIME_EVENT);
        _finished = action->isFinished() || _finished;
        _restarted = action->isRestarted() || _restarted;
    }

    for(auto action : _targetActions)
        action->processEvent(TIME_EVENT);

    for(auto action : _textActions)
        action->processEvent(TIME_EVENT);

    if(_finished) finishSelf();
}

ShotResult LayerModel::shootSelf(QPoint point)
{
    ShotResult result;

    for(auto it = _targets.end()-1; it >= _targets.begin(); it--){
        auto target = *it;
        if(target->canBeShot(point)){
            auto shot = target->shootSelf(point);

            if(shot.hitCount > 0){
                for(auto action : _targetActions){
                    if(action->getTarget() == target)
                        action->processEvent(HIT_EVENT);
                }
                if(!target->ignoresShots()){
                    result += shot;
                    break;
                }
            }
        }
    }
    if(result.hitCount <= 0) {
        if(_missEnabled){
            result.missed = true;
            result.score = -_missPenalty;
            _totalScore += result;
        }
    }
    else _totalScore += result; // increases shot count as well

    for(auto action : _layerActions){
        action->processEvent(SHOT_EVENT);
        _finished = action->isFinished() || _finished;
        _restarted = action->isRestarted() || _restarted;
    }

    for(auto text : _textActions) {
        _totalScore.time = -1;
        _totalScore.ammo = -1;
        text->set(_totalScore);
    }
    if(_finished) finishSelf();

    return result;
}

QJsonValue LayerModel::toJson()
{
    QJsonObject result;
    QString tmpUrl = _backgroundUrl;
    tmpUrl.remove(ASSETS_IMAGE_PATH);
    result.insert("background", tmpUrl);
    if(_missEnabled) result.insert("missPenalty", _missPenalty);

    result.insert("targets", TargetGroup::toJson());

    QJsonArray targetActions;
    for(auto action : _targetActions)
        targetActions.append(action->toJson());
    for(auto action : _textActions)
        targetActions.append(action->toJson());
    result.insert("targetActions", targetActions);

    QJsonArray layerActions;
    for(auto action : _layerActions){
        layerActions.append(action->toJson());
    }
    result.insert("layerActions", layerActions);
    return result;
}

bool LayerModel::fromJson(const QJsonValue& json)
{
    auto obj = json.toObject();
    if(obj.isEmpty()) return false;

    auto backgroundPath = obj["background"].toString();
    _backgroundUrl = (QFileInfo(backgroundPath).exists()) ? backgroundPath : ASSETS_IMAGE_PATH + backgroundPath;

    if(obj.contains("missPenalty")){
        _missEnabled = true;
        _missPenalty = obj["missPenalty"].toDouble();
    }

    TargetGroup::fromJson(obj["targets"].toArray());

    for(auto actionVal : obj["targetActions"].toArray()){
        int targetId = -1;
        TargetModel* model = nullptr;
        if(TargetAction::getTargetId(actionVal, &targetId)){
            for(auto target : _targets){
                if(target->getId() == targetId){
                    model = target;
                    break;
                }
            }
            if(model){
                if(PosChange::sameType(actionVal)){
                    auto posChange = new PosChange(model);
                    posChange->fromJson(actionVal);
                    add(posChange);
                }
                else if(TargetButton::sameType(actionVal)){
                    auto targetButton = new TargetButton(model);
                    targetButton->fromJson(actionVal);
                    add(targetButton);
                }
                else if(TextAction::sameType(actionVal)){
                    auto textAction = new TextAction(model, "");
                    textAction->fromJson(actionVal);
                    add(textAction);
                }
            }
        }
    }

    QJsonArray layerActions = obj["layerActions"].toArray();
    for(auto val : layerActions){
        int actionId = -1;
        if(FinishAction::getActionId(val, &actionId)){
            ActionModel* model = nullptr;
            for(auto targetAction : _targetActions){
                if(targetAction->getId() == actionId){
                    model = targetAction;
                    break;
                }
            }
            if(!model){
                for(auto textAction : _textActions){
                    if(textAction->getId() == actionId){
                        model = textAction;
                        break;
                    }
                }
            }
            if(model){
                auto finishAction = new FinishAction(model, FINISH);
                finishAction->fromJson(val);
                add(finishAction);
            }
        }
        else if(FinishAction::sameType(val))
            add(new FinishAction(val));
    }

    return true;
}


