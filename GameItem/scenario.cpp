#include "scenario.h"

#include <QJsonArray>
#include <QVariant>

qint64 Scene::s_nextId = 1;

Scene::Scene(QString videoPath, QPointF editorPos) :
    _id(s_nextId++),
    defaultTransition(0, nullptr),
    path(videoPath),
    pos(editorPos)
{}

void Scene::connect(VKey k, Scene* scene)
{
    if(scene) {
        _transitions.insert(k, {scene->_id, scene});
    }
}
void Scene::disconnect(VKey k)
{
    auto it = _transitions.find(k);
    if(it != _transitions.end()) {
        _transitions.erase(it);
    }
}
void Scene::clearConnections()
{
    _transitions.clear();
}
QMap<VKey, Scene*> Scene::connections()
{
    QMap<VKey, Scene*> connections;
    for(auto it = _transitions.begin(); it != _transitions.end(); it++) {
        connections.insert(it.key(), it.value().second);
    }
    
    return connections;
}

Scene* Scene::next(VKey k) 
{
    if(_transitions.find(k) == _transitions.end()) {
        return nullptr;
    }
    
    return _transitions.value(k).second;
}
bool Scene::isValid()
{
    return _id > 0;
}

QJsonObject Scene::toJson()
{
    QJsonObject jobj;
    jobj.insert("id", _id);
    jobj.insert("path", path);
    jobj.insert("posX", pos.x());
    jobj.insert("posY", pos.y());
    
    QJsonArray jarr;
    for(auto it = _transitions.begin(); it != _transitions.end(); it++) {
        QJsonObject transition;
        transition.insert(QString::number(it.key()), it.value().first);
        jarr.append(transition);
    }
    QJsonObject defTransition;
    defTransition.insert("default", defaultTransition.first);
    jarr.append(defTransition);
    
    jobj.insert("transitions", jarr);
    return jobj;
}

Scene Scene::fromJson(QJsonObject jobj) 
{
    bool ok = true;
    bool good;
    Scene scene;
    scene.path = jobj.value("path").toString();
    scene._id = jobj.value("id").toVariant().toLongLong(&good); ok = ok && good;
    qreal posX = jobj.value("posX").toVariant().toReal(&good);  ok = ok && good;
    qreal posY = jobj.value("posY").toVariant().toReal(&good);  ok = ok && good;
    scene.pos = QPointF(posX, posY);
    if(!ok) { scene._id = -1; return scene; }
    if(scene._id >= s_nextId) {
        s_nextId = scene._id + 1;
    }
    
    
    QJsonArray jarr = jobj.value("transitions").toArray();
    for(QJsonValue const& t : jarr) {
        QJsonObject transition = t.toObject();
        QString key = transition.keys().first();
        if(key != "default") {
            VKey k = transition.keys().first().toLongLong(&good);                             ok = ok && good;
            qint64 id = transition.value(QString::number(k)).toVariant().toLongLong(&good);   ok = ok && good;
            if(!ok) { scene._id = -1; return scene; }
            
            scene._transitions.insert(k, {id, nullptr});
        }
        else {
            qint64 id = transition.value(key).toVariant().toLongLong(&good); ok = ok && good;
            if(!ok) { id = 0; }
            scene.defaultTransition.first = id;
        }
    }
    
    
    return scene;
}



QJsonObject Scenario::toJson()
{
    QJsonArray jscenes;
    for(Scene& s : scenes) {
        jscenes.append(s.toJson());
    }
    
    QJsonObject jobj;
    jobj.insert("scenes", jscenes);
    
    return jobj;
}
Scenario* Scenario::fromJson(QJsonObject jobj) 
{
    Scenario* scenario = new Scenario();
    // Parse JSON
    QJsonArray jscenes = jobj.value("scenes").toArray();
    if(!jscenes.size()) {
        delete scenario;        
        return nullptr;
    }
    
    for(QJsonValue const& jsc : jscenes) {
        QJsonObject s = jsc.toObject();
        Scene scene = Scene::fromJson(s);
        if(!scene.isValid()) {
            delete scenario;
            return nullptr;
        }
        scenario->scenes.push_back(scene);
    }
    
    if(!scenario->connectScenes()) {
        delete scenario;
        return nullptr;
    }
      
    return scenario;
}

bool Scenario::connectScenes()
{
    for(Scene& s : scenes) {
        // Iterate through all transitions
        for(auto t = s._transitions.begin(); t != s._transitions.end(); t++) {
            // Find scene with matching id for every transition
            bool found = false;
            for(Scene& s2 : scenes) {
                if(s2._id == t.value().first) {
                    s.connect(t.key(), &s2);
                    found = true;
                    break;
                }
            }
            
            // If no scene with specified id exists, quit
            if(!found) {
                return false;
            }
        }
        
        // Default transition
        for(Scene& s2 : scenes) {
            if(s2._id == s.defaultTransition.first) {
                s.defaultTransition.second = &s2;
                break;
            }
        }
    }
    
    return true;
}

Scene* Scenario::operator[](int i) 
{
    return &scenes[i];
}
