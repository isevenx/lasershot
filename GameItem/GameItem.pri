HEADERS += \
    $$PWD/targetmodel.h \
    $$PWD/targetgroup.h \
    $$PWD/levelmodel.h \
    $$PWD/layermodel.h \
    $$PWD/scenario.h

SOURCES += \
    $$PWD/levelmodel.cpp \
    $$PWD/layermodel.cpp \
    $$PWD/targetmodel.cpp \
    $$PWD/targetgroup.cpp \
    $$PWD/scenario.cpp

include(Action/Action.pri)
