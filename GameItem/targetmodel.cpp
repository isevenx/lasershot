#include "targetmodel.h"
#include <QPainter>
#include <QTransform>
#include <QRegion>
#include <QFile>
#include "Misc/jsoncast.h"
#include "Misc/imagealg.h"
using namespace JsonCast;

TargetModel::TargetModel()
{
    clear();
}

TargetModel::~TargetModel()
{
    emit deleted();
    clear();
}

void TargetModel::clear()
{
    _filePath.clear();
    _image = QImage();
    _layer = QImage();
    _size = QSize();
    _regions.clear();
    _scaleX = 1;
    _scaleY = 1;
    _position = QPoint(0,0);
    _space = QSize(1920, 1080);
    _drawShots = false;
    _ignoreShot = false;
    _id = -1;
    _hide = false;
    emit dataChanged();
}

QString TargetModel::getFilePath()
{
    return _filePath;
}

QImage TargetModel::getImage()
{
    QImage result;
    if(_hide || _size.isEmpty() || (_size.width() <= 0 || _size.height() <= 0)) return result;

    if(_image.isNull()){
        result = _layer.scaled(_size, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    }
    else {
        result = _image.copy();
        {
            QPainter painter(&result);
            painter.setCompositionMode(QPainter::CompositionMode_SourceOver);
            painter.drawImage(0, 0, _layer);
        }
        if(_size != _image.size())
            result = result.scaled(_size, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
    }
    return result;
}

void TargetModel::hide()
{
    _hide = true;
    emit dataChanged();
}

void TargetModel::show()
{
    _hide = false;
    emit dataChanged();
}

int TargetModel::getId()
{
    return _id;
}

void TargetModel::setId(int id)
{
    _id = id;
}

QSize TargetModel::getSize()
{
    return _size;
}

QSize TargetModel::getOriginalSize()
{
    return _originalSize;
}

bool TargetModel::hasImage()
{
    return !_image.isNull();
}

QPointF TargetModel::getPosition()
{
    return _position;
}

QSize TargetModel::getSpace()
{
    return _space;
}

void TargetModel::setImage(QImage image)
{
    _image = image.convertToFormat(QImage::Format_ARGB32);
    if(!image.isNull()){
        _layer = QImage(_image.size(), QImage::Format_ARGB32);
        _layer.fill(Qt::transparent);
        _originalSize = _image.size();
        if(_size.isEmpty()) _size = _image.size();
    }
    emit dataChanged();
}

void TargetModel::setPosition(QPointF position)
{
    _position = position;
    emit dataChanged();
}

void TargetModel::setSize(QSize size)
{
    if(_originalSize.isEmpty()) _originalSize = size;
    _scaleX = size.width()/float(_originalSize.width());
    _scaleY = size.height()/float(_originalSize.height());
    _size = size;
    if(_image.isNull()){
        _originalSize = _size;
        if(_layer.isNull()){
            _layer = QImage(_size, QImage::Format_ARGB32);
            _layer.fill(Qt::transparent);
        }
    }
    emit dataChanged();
}

void TargetModel::setSpace(QSize space)
{
    _space = space;
}

void TargetModel::setScale(float scaleX, float scaleY)
{
    QSize tmp = _originalSize;
    if(tmp.isEmpty()) return;

    tmp.rwidth() *= scaleX;
    tmp.rheight() *= scaleY;
    if(tmp.width() <= 0.1 || tmp.height() <= 0.1) return;
    setSize(tmp);

    emit dataChanged();
}

void TargetModel::setDrawShots(bool enabled)
{
    _drawShots = enabled;
}

void TargetModel::addRegion(QPolygonF region, double score)
{
    _regions.append({region, score});
}

void TargetModel::addLastRegion(QPoint point)
{
    if(!_regions.isEmpty())
        _regions.back().first.append(point);
}

void TargetModel::removeLastRegion()
{
    if(!_regions.isEmpty())
        _regions.removeLast();
}

void TargetModel::drawRegions()
{
    if(!_layer.isNull()){
        _layer.fill(Qt::transparent);
        for(auto region : _regions){
            QPainter painter(&_layer);
            painter.setBrush(QColor(0,0,0,30));
            painter.setPen(QPen(Qt::red, 2));
            painter.drawPolygon(region.first);
            painter.drawEllipse(region.first.last(), 3, 3);
        }
        emit dataChanged();
    }
}

void TargetModel::setIgnoreShot(bool ignore)
{
    _ignoreShot = ignore;
}

bool TargetModel::ignoresShots()
{
    return _ignoreShot;
}

bool TargetModel::canBeShot(QPointF point)
{
    return (!_ignoreShot && point.x() >= _position.x() && point.x() <= _position.x()+_size.width() &&
            point.y() >= _position.y() && point.y() <= _position.y()+_size.height());
}

ShotResult TargetModel::shootSelf(QPointF point)
{
    ShotResult result;

    QTransform t;
    // Scale to target image
    t.scale(1/_scaleX, 1/_scaleY);
    // Shift position
    t.translate(-_position.x(), -_position.y());
    t.translate(point.x(), point.y());
    auto circle = ImageAlg::makeCircle(t.map(QPointF(0,0)), 7, 12);

    for(auto it = _regions.end()-1; it >= _regions.begin(); --it){
        if(it->first.intersected(circle).size()){
            result.hitCount = 1;
            result.score = it->second;
            break;
        }
    }

    if(result.hitCount && _drawShots){
        clearLayer();
        QPainter painter(&_layer);
        painter.setBrush(Qt::black);
        painter.setPen(QPen(Qt::white, 5));
        painter.setTransform(t);
        painter.drawEllipse(QPointF(0,0), 7, 7);
        emit dataChanged();
    }

    return result;
}

void TargetModel::setPosRandom()
{
    if(!_space.isEmpty()){
        QPointF tmp;
        tmp.setX(rand() % (_space.width() - _size.width()));
        tmp.setY(rand() % (_space.height() - _size.height()));
        setPosition(tmp);
    }
}

void TargetModel::clearLayer()
{
    _layer.fill(Qt::transparent);
    emit dataChanged();
}

QJsonValue TargetModel::toJson()
{
    QJsonObject obj;
    obj.insert("image", ToJson(_image));
    QJsonArray array;
    for(const auto& region : _regions){
        QJsonObject obj;
        obj["score"] = region.second;
        obj["region"] = ToJson(region.first);
        array.append(obj);
    }
    obj.insert("regions", array);
    obj.insert("position", ToJson(_position));
    obj.insert("size", ToJson(_size));
    obj.insert("drawShots", _drawShots);
    obj.insert("ignoreShot", _ignoreShot);
    return obj;
}

bool TargetModel::fromJson(const QJsonValue& json)
{
    clear();

    QJsonObject obj = json.toObject();
    if(obj.isEmpty()) return false;
    else{
        setImage(ToImage(obj["image"]));
        QJsonArray array = obj["regions"].toArray();
        for(const auto& val : array){
            auto obj = val.toObject();
            addRegion(ToPolygon(obj["region"]), obj["score"].toDouble());
        }
        setSize(ToSize(obj["size"]));
        setPosition(ToPoint(obj["position"]));
        setDrawShots(obj["drawShots"].toBool());
        setIgnoreShot(obj["ignoreShot"].toBool());
        return (!_image.isNull() || !_size.isEmpty());
    }
}

bool TargetModel::toFile(QString filePath)
{
    _filePath.clear();

    QFile file(filePath);
    if(file.open(QFile::WriteOnly)){
        file.write(QJsonDocument(toJson().toObject()).toJson());
        file.close();
        _filePath = filePath;
        return true;
    }
    return false;
}

bool TargetModel::fromFile(QString filePath)
{
    _filePath.clear();

    QFile file(filePath);
    bool ok = false;
    if(file.open(QFile::ReadOnly)){
        ok = fromJson(QJsonDocument::fromJson(file.readAll()).object());
        file.close();
        _filePath = filePath;
    }
    return ok;
}
