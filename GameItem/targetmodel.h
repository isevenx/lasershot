#ifndef TARGETMODEL_H
#define TARGETMODEL_H

#include "Database/datamodel.h"
#include "Database/shotresult.h"
#include "Action/actionmodel.h"
#include <QPoint>
#include <QImage>
#include <QTimer>
#include <QJsonValue>

class TargetModel : public DataModel
{
    Q_OBJECT
public:
    TargetModel();
    ~TargetModel();

    void clear();
    void clearLayer();
    void drawRegions();
    void setImage(QImage image);
    void setSize(QSize size);
    void setScale(float scaleX, float scaleY);
    void setPosition(QPointF position);
    void setSpace(QSize space);
    void setIgnoreShot(bool ignore);
    void setDrawShots(bool enabled);

    void hide();
    void show();

    void addRegion(QPolygonF region, double score);
    void addLastRegion(QPoint point);
    void removeLastRegion();

    QJsonValue toJson();
    int getId();
    void setId(int id);
    bool fromJson(const QJsonValue&);
    bool toFile(QString filePath);
    bool fromFile(QString filePath);

    QString getFilePath();
    QImage getImage();
    QSize getSize();
    QSize getOriginalSize();
    QSize getSpace();
    bool hasImage();
    QPointF getPosition();
    bool ignoresShots();

    bool canBeShot(QPointF point);
    ShotResult shootSelf(QPointF point);
    void setPosRandom();

private:
    int _id;
    QString _filePath;
    QImage _image;
    QString _imageText;
    QSize _originalSize;
    QPointF _position;
    QSize _size;
    float _scaleX, _scaleY;
    QSize _space;
    QVector<QPair<QPolygonF,double>> _regions;

    QImage _layer;
    bool _drawShots;
    bool _ignoreShot;
    bool _hide;
};

#endif // TARGETMODEL_H
