#include "targetgroup.h"
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>
#include "Misc/jsoncast.h"
#include "Misc/globalsettings.h"
using namespace JsonCast;
using namespace GlobalSettings;

TargetGroup::TargetGroup()
{

}

TargetGroup::~TargetGroup()
{
    emit deleted();
    clear();
}

void TargetGroup::add(TargetModel* model)
{
    model->setId(_targets.size());
    _targets.append(model);
    emit dataChanged();
}

void TargetGroup::remove(TargetModel* model)
{
    QMutableVectorIterator<TargetModel*> it(_targets);
    while(it.hasNext()){
        auto target = it.next();
        if(target == model){
            it.remove();
            delete target;
            emit dataChanged();
            break;
        }
    }
}

void TargetGroup::clear()
{
    for(auto& target : _targets)
        delete target;
    _targets.clear();
    emit dataChanged();
}

QVector<TargetModel*>::iterator TargetGroup::begin()
{
    return _targets.begin();
}

QVector<TargetModel*>::iterator TargetGroup::end()
{
    return _targets.end();
}

QVector<TargetModel*> TargetGroup::getTargets()
{
    return _targets;
}

QJsonValue TargetGroup::toJson()
{
    QJsonArray targets;
    for(auto target : _targets){
        QString filePath = target->getFilePath();
        filePath.remove(ASSETS_TARGET_PATH);
        QJsonObject obj;
        if(filePath.isEmpty()) obj.insert("target", target->toJson());
        else obj.insert("filePath", filePath);

        obj.insert("size", ToJson(target->getSize()));
        obj.insert("position", ToJson(target->getPosition()));
        obj.insert("id", target->getId());
        targets.append(obj);
    }
    return targets;
}

bool TargetGroup::fromJson(const QJsonValue& json)
{
    clear();

    QJsonArray targets = json.toArray();
    if(targets.isEmpty()) return false;
    else{
        for(const auto& val : targets){
            auto obj = val.toObject();
            auto target = new TargetModel;
            auto filePath = obj["filePath"].toString();
            if(!filePath.isEmpty()){
                if(!target->fromFile(ASSETS_TARGET_PATH + filePath)){
                    delete target;
                    target = nullptr;
                }
            }
            else if(!target->fromJson(obj["target"])){
                delete target;
                target = nullptr;
            }

            if(target){
                target->setSize(ToSize(obj["size"]));
                target->setPosition(ToPoint(obj["position"]));
                target->setId(obj["id"].toInt());
                add(target);
            }
        }
    }
    return (_targets.size());
}
