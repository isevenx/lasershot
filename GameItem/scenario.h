#ifndef SCENARIO_H
#define SCENARIO_H

#include <QPair>
#include <QMap>
#include <QVector>
#include <QPointF>

#include <QJsonObject>

// @ make this a parameter for file creation
#define SCENARIO_FILENAME "/scenario.json"

typedef int VKey;
class Scene 
{
    // @ dirty haxx
    friend class ScenarioCreationCanvas;
    friend struct Scenario;
    static qint64 s_nextId;
    
    qint64 _id;
    QMap<VKey, QPair<qint64, Scene*>> _transitions;
    
public:
    // @ Probably should use get/set but no time
    QPair<qint64, Scene*> defaultTransition;
    // Relative to Scenario path
    QString path;
    // For storing and loading Scenario editor graphics
    QPointF pos;
    
    Scene(QString videoPath = "", QPointF editorPos = QPointF());
    
    void connect(VKey k, Scene* scene);
    void disconnect(VKey k);
    void clearConnections();
    QMap<VKey, Scene*> connections();
    
    Scene* next(VKey k);
    inline Scene* nextDefault() { return defaultTransition.second; }
    bool isValid();
    
    QJsonObject toJson();
    static Scene fromJson(QJsonObject jobj) ;
};

struct Scenario 
{
    QVector<Scene> scenes;
    // Doesn't actually mean anything, just a convenience storage member
    QString path;
    
    QJsonObject toJson();
    static Scenario* fromJson(QJsonObject jobj);
    
    bool connectScenes();
    
    Scene* operator[](int i);
};

#endif // SCENARIO_H
