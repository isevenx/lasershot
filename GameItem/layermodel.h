#ifndef LAYERMODEL_H
#define LAYERMODEL_H

#include "targetgroup.h"
#include "Database/totalscore.h"
#include "Action/finishaction.h"
#include "Action/targetaction.h"

class LayerModel : public TargetGroup
{
    Q_OBJECT
public:
    LayerModel();
    ~LayerModel();

    using TargetGroup::add;
    using TargetGroup::begin;
    using TargetGroup::end;

    void add(FinishAction*);
    void add(TargetAction*);
    void add(TextAction*);

    void setBackground(QString url);
    QString getBackground();
    void set(TotalScore);
    TotalScore getScore();
    void setSound(QString onStart, QString onEnd = QString()); // path to file
    void setMissPenalty(double missPenalty);

    void start();
    ShotResult shootSelf(QPoint point);
    void createTimeEvent();
    bool isFinished();
    bool isRestarted();
    void clear();
    void clearLayerActions();
    void clearTargetActions();
    void clearTargetActions(TargetModel*);
    void clearLayerActions(FinishType);
    void finishSelf();
    bool isEmpty();

    QJsonValue toJson() override;
    bool fromJson(const QJsonValue &) override;

private:
    QString _backgroundUrl;
    QVector<FinishAction*> _layerActions;
    QVector<TargetAction*> _targetActions;
    QVector<TextAction*> _textActions;

    TotalScore _totalScore;
    qint64 _startTime;
    bool _finished;
    bool _restarted;
    double _missPenalty;
    bool _missEnabled;

    QString _startSound;
    QString _endSound;
};

#endif // LAYERMODEL_H
