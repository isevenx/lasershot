#ifndef LEVELMODEL_H
#define LEVELMODEL_H


#include "targetgroup.h"
#include "layermodel.h"

class LevelModel
{
public:
    LevelModel();
    ~LevelModel();

    void add(LayerModel*);
    QVector<LayerModel*> getLayers();
    LayerModel* getNextLayer();
    void clear();

    bool toFile(QString filePath);
    bool fromFile(QString filePath);

private:
    QVector<LayerModel*> _layers;
    qint64 _layerCounter;
    QString _filePath;
};

#endif // LEVELMODEL_H
