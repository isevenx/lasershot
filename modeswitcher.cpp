#include "modeswitcher.h"
#include <QThread>
#include <QTime>
#include <QFile>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QSettings>
#include <QDebug>

#include "Mode/Calibration/calibrationmode.h"
#include "Mode/Calibration/calibrod.h"
#include "Mode/Calibration/calibshutter.h"
#include "Mode/Calibration/calibration.h"
#include "Mode/Calibration/calibfocus.h"
#include "Mode/Test/testmode.h"
#include "Mode/Test/delaytest.h"
#include "Mode/Test/mousetest.h"
#include "Mode/Shooting/laserdetection.h"
#include "Misc/imagealg.h"
#include "Misc/globalsettings.h"
using namespace GlobalSettings;
using namespace MODE;

ModeSwitcher::ModeSwitcher(Camera* camera, CameraSettings* camSettings, FrameOutputDevice* outDevice, QObject *parent) :
    QObject(parent),
    _camera(camera),
    _settings(camSettings),
    _outDevice(outDevice)
{
    QSettings settings("settings.conf", QSettings::NativeFormat);

    _shootingShutter = settings.value("ShootingShutter", -1).toDouble();

    auto json = QJsonDocument::fromBinaryData(settings.value("RodCalib").toByteArray()).object();
    _lambda = Mat(json["rows"].toInt(0), json["cols"].toInt(0), CV_64F);
    QJsonArray data = json["data"].toArray();
    auto it = data.begin();
    for(int y = 0; y < _lambda.cols; y++){
        for(int x = 0; x < _lambda.rows; x++){
            _lambda.at<double>(y,x) = (*it).toDouble();
            it++;
        }
    }
    if(!_lambda.empty()) _projectorRegion = ImageAlg::getPerspRegion(_lambda, cv::Size(RENDER_BUFFER_WIDTH, RENDER_BUFFER_HEIGHT));
}

ModeSwitcher::~ModeSwitcher()
{
    stop();

    QSettings settings("settings.conf", QSettings::NativeFormat);
    settings.setValue("ShootingShutter", _shootingShutter);

    if(!_lambda.empty()){
        QJsonObject json;
        json["rows"] = _lambda.rows;
        json["cols"] = _lambda.cols;
        QJsonArray data;
        for(int y = 0; y < _lambda.cols; y++){
            for(int x = 0; x < _lambda.rows; x++)
                data.append(_lambda.at<double>(y,x));
        }
        json["data"] = data;
    settings.setValue("RodCalib", QJsonDocument(json).toBinaryData());
    }
}

void ModeSwitcher::stop()
{
    if(_runnable){
        _runnable->disconnect();
        QMetaObject::invokeMethod(_runnable, "terminate", Qt::BlockingQueuedConnection);
        _runnableThread.quit();
        if(!_runnableThread.wait(500))
            qWarning() << "Runnable did not finish properly!";
        delete _runnable.data();
    }
}

void ModeSwitcher::terminate()
{
    stop();
    emit terminated("ModeSwitcher");
    disconnect();
}

void ModeSwitcher::changeMode(Mode mode)
{
    stop();

    _mode = mode;
    if(_mode){
        if(_mode <= MODE::CALIBRATION) startCalibration();
        else if(_mode <= MODE::SHOOTING) startShooting();
        else if(_mode <= MODE::TEST) startTest();
    }

    emit modeChanged(_mode);
}

void ModeSwitcher::startCalibration()
{
    if(_mode == MODE::CALIBRATION){
        _modeQueue.clear();
        _modeQueue.append(CALIBRATION::SHOOTING_SHUTTER);
        changeMode(CALIBRATION::DISPLAY_COORDINATES);
    }
    else if(_mode == CALIBRATION::CAMERA_FOCUS){
        _settings->setFps(25);
        auto runnable = new CalibFocus(_outDevice);
        runnable->moveToThread(&_runnableThread);
        connect(runnable, &CalibFocus::newDebugFrame, this, &ModeSwitcher::sendDebugFrame, Qt::QueuedConnection);
        _runnable = runnable;
        _runnableThread.start();
    }
    else if(_mode == CALIBRATION::DISPLAY_COORDINATES){
        _settings->setFps(10);
        _settings->setShutter(55);
        auto runnable = new CalibRod(_outDevice);
        runnable->moveToThread(&_runnableThread);
        connect(runnable, &CalibRod::newDebugFrame, this, &ModeSwitcher::sendDebugFrame, Qt::QueuedConnection);
        connect(runnable, &CalibRod::lambdaCalculated, this, &ModeSwitcher::saveLambda, Qt::QueuedConnection);
        connect(runnable, &CalibRod::finished, this, &ModeSwitcher::finishCalibration, Qt::QueuedConnection);
        _runnable = runnable;
        _runnableThread.start();
    }
    else if(_mode == CALIBRATION::SHOOTING_SHUTTER){
        _settings->setFps(100);
        auto runnable = new CalibShutter(_settings, _outDevice);
        runnable->setRegion(_projectorRegion);
        runnable->moveToThread(&_runnableThread);
        connect(runnable, &CalibShutter::newDebugFrame, this, &ModeSwitcher::sendDebugFrame, Qt::QueuedConnection);
        connect(runnable, &CalibShutter::shutterCalibrated, this, &ModeSwitcher::saveShootingShutter, Qt::QueuedConnection);
        connect(runnable, &CalibShutter::finished, this, &ModeSwitcher::finishCalibration, Qt::QueuedConnection);
        _runnable = runnable;
        _runnableThread.start();
    }
}

void ModeSwitcher::startShooting()
{
//    if(_shootingShutter >= 0 && !_lambda.empty()){
        _settings->setFps(100);
        _settings->setShutter(_shootingShutter);
        auto runnable = new LaserDetection(_lambda);
        runnable->setRegion(_projectorRegion);
        connect(runnable, &LaserDetection::laserDetected, this, &ModeSwitcher::sendLaserPoints, Qt::QueuedConnection);
        connect(runnable, &LaserDetection::newDebugFrame, this, &ModeSwitcher::sendDebugFrame, Qt::QueuedConnection);
        runnable->moveToThread(&_runnableThread);
        _runnable = runnable;
        _runnableThread.start();
//    }
//    else{
//        qWarning() << "Shutter & lambda not calibrated!";
//        _mode = MODE::IDLE;
//    }
}

void ModeSwitcher::startTest()
{
    if(_mode == TEST::MOUSE_DELAY){
        auto runnable = new MouseTest(_outDevice);
        runnable->moveToThread(&_runnableThread);
        _runnable = runnable;
        _runnableThread.start();
    }
    else if(_mode == TEST::LASER_DELAY){
//        if(_shootingShutter >= 0 && !_lambda.empty()){
            _settings->setFps(100);
            _settings->setShutter(_shootingShutter);
            auto runnable = new DelayTest(_outDevice, _lambda);
            connect(runnable, &DelayTest::newDebugFrame, this, &ModeSwitcher::sendDebugFrame, Qt::QueuedConnection);
            runnable->moveToThread(&_runnableThread);
            _runnable = runnable;
            _runnableThread.start();
//        }
//        else{
//            qWarning() << "Shutter & lambda not calibrated!";
//            _mode = MODE::IDLE;
//        }
    }
}

void ModeSwitcher::processFrame()
{
    if(_runnable && _runnable->isReady())
        QMetaObject::invokeMethod(_runnable, "processFrame", Qt::QueuedConnection, Q_ARG(const cv::Mat, _camera->getFrame()));
}

void ModeSwitcher::createShot(QPoint position)
{
    if(_mode <= MODE::SHOOTING)
        sendLaserPoints({position});
}

void ModeSwitcher::sendDebugFrame(cv::Mat frame)
{
    if(!frame.empty()){
        QImage img((const uchar*)frame.data, frame.cols, frame.rows, frame.step, QImage::Format_RGB888);
        emit newDebugFrame(img.copy());
    }
}

void ModeSwitcher::saveShootingShutter(float shutter)
{
    _shootingShutter = shutter;
}

void ModeSwitcher::saveLambda(cv::Mat lambda)
{
    _lambda = lambda;
    _projectorRegion = ImageAlg::getPerspRegion(lambda, cv::Size(RENDER_BUFFER_WIDTH, RENDER_BUFFER_HEIGHT));
}

void ModeSwitcher::finishCalibration()
{
    if(!_modeQueue.isEmpty()){
        changeMode(_modeQueue.first());
        _modeQueue.pop_front();
    }
    else changeMode(MODE::IDLE);
}

void ModeSwitcher::sendLaserPoints(QVector<QPoint> points)
{
    emit newLaserPoints(points, QDateTime::currentMSecsSinceEpoch());
}
